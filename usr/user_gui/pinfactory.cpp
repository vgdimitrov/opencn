#include "pinfactory.h"

PinFactory* PinFactory::instance = nullptr;

PinFactory* PinFactory::getInstance()
{
    if (instance == nullptr) {
        instance = new PinFactory();
    }

    return instance;
}

Pin* PinFactory::createPin(const char *name, hal_type_t type, hal_pin_dir_t dir)
{
    hal_pin_t* halPin = pin_new(name, type, dir);

    if (halPin == nullptr) {
        return nullptr;
    }

    return new Pin(halPin, true);
}

Pin* PinFactory::findPinByName(const char* name)
{
    hal_pin_t* halPin = pin_find_by_name(name);

    if (halPin == nullptr) {
        return nullptr;
    }

    return new Pin(halPin, false);
}

void PinFactory::destroyPin(Pin* pin)
{
    if (pin->isLocal()) {
        pin_delete(pin->getName());
    }

    delete pin;
}

PinFactory::PinFactory()
{
    af_init();
}

PinFactory::~PinFactory()
{
    af_exit();
}
