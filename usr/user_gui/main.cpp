#include "mainwindow.h"
#include <QApplication>
#include <af.h>

// ================= DUMMY IMPLEMENTATION =================
int feedopt_read_gcode(int)
{
    return 1;
}

PushStatus feedopt_push_opt(CurvStruct, double *, int) {return PushStatus_Success;}

void queue_push(int, CurvStruct) {}

void queue_resize(int, int, CurvStruct) {}

CurvStruct queue_get(int, int)
{
    return CurvStruct{};
}

void queue_set(int, int, CurvStruct) {}

void queue_clear(int) {}

int queue_size(int)
{
    return 0;
}
// ================= DUMMY IMPLEMENTATION =================

int main(int argc, char *argv[])
{
    //TODO: comment before deploy
    //QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling)
    //af_init();
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    int ret = a.exec();
    //af_exit();
    return ret;
}
