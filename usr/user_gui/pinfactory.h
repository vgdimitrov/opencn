#ifndef PIN_FACTORY_H
#define PIN_FACTORY_H

#include "pin.h"

class PinFactory {

public:
    static PinFactory* getInstance();

    /**
     * Create a pin according to its name, type and directory.
     * Please call destroyPin() to destroy the created pin.
     *
     * \param name Name of the pin
     * \param type Type of the pin
     * \param dir Directory of the pin
     *
     * \return Created pin object or nullptr if error
     */
    Pin* createPin(const char* name, hal_type_t type, hal_pin_dir_t dir);

    /**
     * Find a pin from its name.
     * Please call destroyPin() to destroy the created pin.
     *
     * \param name Name of the pin to find
     *
     * \return Pin object if found or nullptr
     */
    Pin* findPinByName(const char* name);
    
    /**
     * Destroy a pin.
     *
     * \param pin Pin to destroy
     */
    void destroyPin(Pin* pin);

private:
    PinFactory();
    ~PinFactory();

private:
    static PinFactory* instance;
};

#endif /* PIN_FACTORY_H */
