#include "pin.h"
#include <QDebug>

Pin::Pin(hal_pin_t *halPin, bool local) : _halPin(halPin), _local(local) { _halData = pin_get_value(_halPin); }

bool Pin::getValueBit() { return _halData->b; }

qint32 Pin::getValueS32() { return _halData->s; }

quint32 Pin::getValueU32() { return _halData->u; }

float Pin::getValueF32() { return _halData->f; }

bool Pin::isLocal() { return _local; }

const char *Pin::getName() { return _halPin->name; }

void Pin::setValue(bool bit) { _halData->b = bit; }

void Pin::setValue(qint32 s) { _halData->s = s; }

void Pin::setValue(quint32 u) { _halData->u = u; }

void Pin::setValue(float f) { _halData->f = f; }

PinDouble::PinDouble(const QString &name, bool local) : _name{name}
{
    _halPin = pin_find_by_name(name.toStdString().c_str());
    if (_halPin) {
        _data = &pin_get_value(_halPin)->f;
        if (!_data) {
            qDebug() << "pin_get_value: failed to retrieve value of pin " << name << endl;
        }
    } else {
        qDebug() << "find_pin_by_name: failed to find " << name << endl;
    }
}

PinDouble::~PinDouble() {}

void PinDouble::update()
{
    if (_data) {
        emit valueChanged(*_data);
    }
}

void PinDouble::setValue(double d)
{
//    qDebug() << _name << ": value = " << d << endl;
    if (_data) *_data = d;
}

double PinDouble::getValue()
{

    static bool error = false;
    if (_data) {
        return *_data;
    } else {
        if (!error) {
            qDebug() << "Unable to get value. Default 0.0 used. " << endl;
            error = true;
        }
        return 0.0;
    }
}

PinBool::PinBool(const QString &name, bool local) : _name{name}
{
    _halPin = pin_find_by_name(name.toStdString().c_str());
    if (_halPin) {
        _data = &pin_get_value(_halPin)->b;
        if (!_data) {
            qDebug() << "pin_get_value: failed to retrieve value of pin " << name << endl;
        }
    } else {
        qDebug() << "find_pin_by_name: failed to find " << name << endl;
    }
}

PinBool::~PinBool() {}

void PinBool::update()
{
    if (_data) {
        emit valueChanged(*_data);
    }
}

void PinBool::setValue(bool b)
{
    // qDebug() << _name << ": value = " << b << endl;
    if (_data) *_data = b;
}

void PinBool::setTrue() { setValue(true); }

void PinBool::setFalse() { setValue(false); }

bool PinBool::getValue()
{

    static bool error = false;

    if (_data) {
        return *_data;
    } else {
        if (!error) {
            qDebug() << "Unable to get value. Default false used. " << endl;
            error = true;
        }
        return false;
    }
}

PinInt32::PinInt32(const QString &name, bool local) : _name{name}
{
    _halPin = pin_find_by_name(name.toStdString().c_str());
    if (_halPin) {
        _data = &pin_get_value(_halPin)->s;
        if (!_data) {
            qDebug() << "pin_get_value: failed to retrieve value of pin " << name << endl;
        }
    } else {
        qDebug() << "find_pin_by_name: failed to find " << name << endl;
    }
}

PinInt32::~PinInt32() {}

void PinInt32::update()
{
    if (_data) {
        emit valueChanged(*_data);
    }
}

void PinInt32::setValue(int v)
{
//    qDebug() << _name << ": value = " << v << endl;
    if (_data) *_data = v;
}

int PinInt32::getValue()
{

    static bool error = false;

    if (_data) {
        return *_data;
    } else {
        if (!error) {
            qDebug() << "Unable to get value. Default false used. " << endl;
            error = true;
        }
        return false;
    }
}
