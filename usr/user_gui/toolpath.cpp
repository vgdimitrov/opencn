#include "toolpath.h"

#include <GL/glu.h>
#include <QDebug>
#include <fcntl.h>

const unsigned int ToolPath::updateIntervalMs = 1000/10;
const unsigned int ToolPath::maxSampleSize = 256;
const unsigned int ToolPath::maxPoints = 4096*4;
const unsigned int ToolPath::sampleRate = 10;

ToolPath::ToolPath(QObject *parent)
    : QObject(parent), _channel(nullptr), _points(nullptr), _pointCount(0), _writeIdx(0), _readIdx(0),
      _overlapped(false)
{
    _updateTimer = new QTimer(this);

    connect(_updateTimer, &QTimer::timeout, this, &ToolPath::update);

    _points = new QVector3D[maxPoints];
}

ToolPath::~ToolPath()
{
    stop();
    delete[] _points;
}

bool ToolPath::start()
{
    int ret;

    if (_channel) {
        qDebug() << "Toolpath channel already started.";
        return false;
    }

    ret = open_sample_channel(&_channel, O_RDONLY);
    if (ret) {
        qDebug() << "open_sample_channel() failed " << ret;
        _channel = nullptr;
        return false;
    }

    if (!sample_channel_set_freq(sampleRate)) {
        qDebug() << "Unable to set toolpath sample rate";
    }

    if (!sample_channel_enable(true)) {
        qDebug() << "Unable to start sample channel";
        close_channel(_channel);
        _channel = nullptr;
        return false;
    }

    _updateTimer->start(updateIntervalMs);

    return true;
}

void ToolPath::stop()
{
    if (sample_channel_enabled()) {
        sample_channel_enable(false);
    }

    _updateTimer->stop();
    if (_channel != nullptr) {
        close_channel(_channel);
        _channel = nullptr;
    }
}

void ToolPath::reset()
{
    _writeIdx = 0;
    _readIdx = 0;
    _overlapped = false;
}

QVector3D ToolPath::getLastPosition()
{
    unsigned int idx = _writeIdx == 0 ? maxPoints - 1 : _writeIdx - 1;
    return _points[idx];
}

void ToolPath::draw()
{
    if (_readIdx == _writeIdx) {
        // Empty
        return;
    }

    glPushMatrix();
    glTranslatef(0.0f, 0.00001f, 0.0f);
    glEnable(GL_COLOR_MATERIAL);
    glColor3f(1.0f, 0.0f, 0.0f);

    glLineWidth(2.0f);
    glBegin(GL_LINE_STRIP);

    unsigned int i = _readIdx;
    do {
        glVertex3f(_points[i].x(), -_points[i].y(), _points[i].z());

        i++;
        if (i >= maxPoints) {
            i = 0;
        }
    } while (i != _writeIdx);

    glEnd();

    glDisable(GL_COLOR_MATERIAL);
    glPopMatrix();
}

void ToolPath::update()
{
    int nbSample = 0;
    char sample[maxSampleSize];

    ssize_t s;
    do {
        s = read_sample(_channel, sample, sizeof(sample));
        if ((s < 0) || (s == sizeof(sample))) {
            qDebug() << "read_sample() failed " << s;
            return;
        }

        if (s != 0) {
            QString line = QString(sample);
            QStringList list = line.split(" ", QString::SkipEmptyParts);
            if (list.size() < 3) {
                qDebug() << "Error format point: " << sample;
            } else {

                _points[_writeIdx].setX(list[0].toFloat() / 1000.0f);
                _points[_writeIdx].setY(list[2].toFloat() / 1000.0f);
                _points[_writeIdx].setZ(-list[1].toFloat() / 1000.0f);

                nbSample++;

                _writeIdx++;

                if (_writeIdx == maxPoints) {
                    _writeIdx = 0;
                    _overlapped = true;
                }

                if (_overlapped) {
                    _readIdx = _writeIdx + 1;
                    _readIdx = _readIdx < maxPoints ? _readIdx : 0;
                }
            }
        }
    } while (s > 0);

    if (nbSample > 0) {
        emit isUpdated();
    }
}
