add_executable(testing-gui
    mainwindow.ui
    mainwindow.cpp
    ../main.cpp
    ../pin.cpp
    ../pinfactory.cpp
)

target_link_libraries(testing-gui Qt5::Widgets opencn)

target_include_directories(testing-gui
    PRIVATE ${PROJECT_SOURCE_DIR}
    ${opencn-usr_SOURCE_DIR}/../matlab/src)
