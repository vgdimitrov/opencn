#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QTimer>
#include <QDebug>

#include "pinfactory.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    _timer = new QTimer(this);

    connect(_timer, &QTimer::timeout, this, &MainWindow::update);

    _timer->start(100);

    _inputPin = PinFactory::getInstance()->createPin("lcec.0.1.din-0", HAL_BIT, HAL_OUT);
    if (_inputPin == nullptr)
        qDebug() << "Create pin failed";

    _outputPin = PinFactory::getInstance()->createPin("lcec.0.2.dout-0", HAL_BIT, HAL_IN);
    if (_outputPin == nullptr)
        qDebug() << "Create pin failed";

    connect(ui->pushButton, &QPushButton::clicked, this, &MainWindow::buttonTriggered);

    /* Update GUI */
    buttonTriggered(ui->pushButton->isChecked());
    update();
}

MainWindow::~MainWindow()
{
    if (_inputPin != nullptr)

        PinFactory::getInstance()->destroyPin(_inputPin);

    if (_outputPin != nullptr)
        PinFactory::getInstance()->destroyPin(_outputPin);

    delete ui;
}

void MainWindow::update()
{
    if (_inputPin == nullptr)
        return;

    if (_inputPin->getValueBit() == true)
        ui->checkBox->setChecked(true);
    else
        ui->checkBox->setChecked(false);
}

void MainWindow::buttonTriggered(bool checked)
{
    if (_outputPin == nullptr)
        return;

    _outputPin->setValue(checked);
}
