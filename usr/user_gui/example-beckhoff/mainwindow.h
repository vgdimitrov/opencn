#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "pin.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void update();
    void buttonTriggered(bool checked);
private:
    Ui::MainWindow *ui;
    QTimer* _timer;
    Pin* _inputPin;
    Pin* _outputPin;
};

#endif // MAINWINDOW_H
