/*
 * Copyright (C) 2019 Kevin Joly <kevin.jolyr@heig-vd.ch>
 * Copyright (C) 2019 Daniel Rossier <daniel.rossier@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <poll.h>

#include <af.h>

#define AFLIB_PATH_FIFO_NAME "/tmp/aflib-path-fifo"
#define AFLIB_SEGMENT_FIFO_NAME "/tmp/aflib-segment-fifo"

struct _communicationChannel {
	int pipe_fd;
	char pipe_name[HAL_NAME_LEN];
	void (*close_channel)(communicationChannel_t *channel);
};

/* Internal use */
static void __close_channel(communicationChannel_t *channel);

/********************
 * Public functions *
 ********************/

/*
 * open_channel can be called several times by any process.
 * The special fifo dir entry is created if it does not exist yet.
 */
int open_channel(const char *file_name, communicationChannel_t **channel) {
	int ret;

	*channel = malloc(sizeof(struct _communicationChannel));
	strcpy((*channel)->pipe_name, file_name);

	ret = mkfifo(file_name, 0666);
	if (ret && (errno != EEXIST))
		return -1;

	(*channel)->pipe_fd = open(file_name, O_RDWR);
	if ((*channel)->pipe_fd == -1) {
		free(*channel);
		return -1;
	}

	(*channel)->close_channel = __close_channel;

	return 0;
}

int open_GCodeFilePath_channel(communicationChannel_t **channel) {
	return open_channel(AFLIB_PATH_FIFO_NAME, channel);
}

int open_struct_channel(communicationChannel_t **channel) {
	return open_channel(AFLIB_SEGMENT_FIFO_NAME, channel);
}

static void __close_channel(communicationChannel_t *channel) {
	int ret;

	close(channel->pipe_fd);

	/*
	 * We do not remove the dir entry related to this pipe since it may
	 * be re-used by another process.
	 */
	free(channel);
}
void close_channel(communicationChannel_t *channel) {
	channel->close_channel(channel);
}

ssize_t writeGCodeFilePath(communicationChannel_t *channel, const char *path, size_t len) {
	ssize_t ret;

	ret = write(channel->pipe_fd, path, len);

	return ret;
}

ssize_t readGCodeFilePath(communicationChannel_t *channel, char *path, size_t maxlen) {
	ssize_t ret;

	ret = read(channel->pipe_fd, path, maxlen);

	return ret;
}

ssize_t readStruct(communicationChannel_t *channel, void *structData, size_t structSize, unsigned int timeoutms) {
	ssize_t ret;
	fd_set set;
	struct timeval timeout;
	int retsel;

	FD_ZERO(&set);
	FD_SET(channel->pipe_fd, &set);

	timeout.tv_sec = timeoutms/1000;
	timeoutms -= timeout.tv_sec * 1000;
	timeout.tv_usec = timeoutms * 1000;

	if ((timeout.tv_sec == 0) && (timeout.tv_usec == 0)){
		retsel = select(channel->pipe_fd + 1, &set, NULL, NULL, NULL);
	} else {
		retsel = select(channel->pipe_fd + 1, &set, NULL, NULL, &timeout);
	}

	if (retsel < 1) {
		return -1;
	}

	ret = read(channel->pipe_fd, structData, structSize);

	return ret;
}

ssize_t writeStruct(communicationChannel_t *channel, const void *structData, size_t structSize) {
	ssize_t ret;

	ret = write(channel->pipe_fd, structData, structSize);

	return ret;
}


