#ifndef C_SPLINE_H
#define C_SPLINE_H

#include <stddef.h>
#ifndef __KERNEL__
#include <gsl/gsl_bspline.h>
#include <stdint.h>
#else
#include <linux/types.h>
#include <opencn/gsl/gsl_bspline.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

typedef struct bspline {
    gsl_bspline_workspace *ws;
    gsl_matrix *dB;
} bspline_t;

void c_bspline_create(unsigned long *handle, double x0, double x1, size_t degree, size_t nbreak);
void c_bspline_base_eval(const unsigned long *handle, size_t N, const double *xvec, double *BasisVal, double *BasisValD,
                         double *BasisValDD, double *BasisIntegr);

void c_bspline_destroy(const unsigned long *handle);
void c_bspline_eval(const unsigned long *handle, const double *c, double x, double X[3]);
void c_bspline_eval_vec(const unsigned long *handle, const double *c, size_t N, double *xvec, double X[][3]);
size_t c_bspline_ncoeff(const unsigned long *handle);

#ifdef __cplusplus
}
#endif

#endif
/* C_SPLINE_H */
