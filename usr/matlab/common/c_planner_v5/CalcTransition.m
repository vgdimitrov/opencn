function [CurvStruct1_C, CurvStruct_T, CurvStruct2_C]  = CalcTransition(CurvStruct1, CurvStruct2, CutOff)
    DebugLog('========== CalcTransition ==========\n')
    DebugLog('CutOff = %.3f\n', CutOff)
    PrintCurvStruct(CurvStruct1);
    PrintCurvStruct(CurvStruct2);
    CurvStruct1_C      = CutCurvStruct(CurvStruct1, 0, CutOff);
    CurvStruct2_C      = CutCurvStruct(CurvStruct2, CutOff, 0);
    DebugLog('========== AFTER CUTTING \n')
    PrintCurvStruct(CurvStruct1_C)
    PrintCurvStruct(CurvStruct2_C)
    [r0D0, r0D1, r0D2] = EvalCurvStruct(CurvStruct1_C, 1); % end
    [r1D0, r1D1, r1D2] = EvalCurvStruct(CurvStruct2_C, 0); % begin
    rD0 = [r0D0, r1D0];
    rD1 = [r0D1, r1D1];
    p5                 = G2_Hermite_Interpolation(r0D0, r0D1, r0D2, r1D0, r1D1, r1D2);
    CurvStruct_T       = ConstrTransP5Struct(p5, CurvStruct1.FeedRate);
end