function value = QueueGet(id, k)
global g_QueueData
value = g_QueueData{id}(k);
end