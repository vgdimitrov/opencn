function CurvStructCut = CutHelix(CurvStruct, d0, d1)
%
theta  = CurvStruct.theta;
[L, R] = LengthHelix(CurvStruct);
%
u0      = d0/L;
u1      = 1 - d1/L;
%
P0prim = EvalHelix(CurvStruct, u0);
P1prim = EvalHelix(CurvStruct, u1);
%
theta  = theta - (d0+d1)/R;
%
CurvStructCut       = CurvStruct;
CurvStructCut.P0    = P0prim;
CurvStructCut.P1    = P1prim;
CurvStructCut.theta = theta;
% PITCH NOT YET RECALCULATED  !!!





