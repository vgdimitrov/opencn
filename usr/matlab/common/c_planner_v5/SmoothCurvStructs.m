function SmoothCurvStructs(QueueIn, QueueOut, CutOff)
%
Ncrv = int32(QueueIn.Size);
if Ncrv == 0
    return;
end
%
%% calculate the number of transition curves
% Ntrans = 0;
% %
% for k = 1:Ncrv-1
%     if (CurvStructs(k).ZSpdMode == ZSpdMode.NN) || (CurvStructs(k).ZSpdMode == ZSpdMode.ZN)
%         Ntrans = Ntrans + 1;
%     end
% end

Ntrans = int32(0);
%
for k = 1:Ncrv-1
    if (QueueIn.Get(k).ZSpdMode == ZSpdMode.NN) || (QueueIn.Get(k).ZSpdMode == ZSpdMode.ZN)
        Ntrans = Ntrans + 1;
    end
end

%% preallocation of smoothed curve array
% QueueOut.Resize(Ncrv + Ntrans, QueueIn.Get(1)); % CurvStructs1 = repmat(CurvStructs(1), 1, Ncrv+Ntrans);
CurvStruct1  = QueueIn.Get(1); % CurvStruct1 = CurvStructs(1);

k0 = 1;
if Ncrv > 1
    % Satisfy CurvStruct2_C is not fully defined on some execution paths
    % @HACK HANDLE THIS BETTER
    CurvStruct2_C = CurvStruct1;
    %
    kk = int32(1);
    %
    for k = 1:Ncrv-1
        if (CurvStruct1.ZSpdMode ~= ZSpdMode.NZ) && (CurvStruct1.ZSpdMode ~= ZSpdMode.ZZ)
            %         [CurvStruct1_C, CurvStruct_T, CurvStruct2_C]  = ...
            %             CalcTransition(CurvStruct1, CurvStructs(k+1), CutOff);
            
            if LengthCurv(QueueIn.Get(k+1)) > 3*CutOff
                [~, r0D1] = EvalCurvStruct(CurvStruct1, 1);
                [~, r1D1] = EvalCurvStruct(QueueIn.Get(k+1), 0);
                if CurvStruct1.Type == CurveType.Line && QueueIn.Get(k+1).Type == CurveType.Line ...
                        && norm(cross(r0D1, r1D1)) < 1e-6
                    QueueOut.Push(CurvStruct1);
                    CurvStruct1 = QueueIn.Get(k+1);
                    kk = kk + 1;
                else

                   [CurvStruct1_C, CurvStruct_T, CurvStruct2_C]  = ...
                    CalcTransition(CurvStruct1, QueueIn.Get(k+1), CutOff); 

                    QueueOut.Push(CurvStruct1_C); % CurvStructs1(kk)   = CurvStruct1_C;
                    QueueOut.Push(CurvStruct_T); % CurvStructs1(kk+1) = CurvStruct_T;
                    kk = kk + 2;
                    CurvStruct1        = CurvStruct2_C;
                end
                k0 = k+1;
            elseif LengthCurv(QueueIn.Get(k+1)) > 2*CutOff
                [~, r0D1] = EvalCurvStruct(CurvStruct1, 1);
                [~, r1D1] = EvalCurvStruct(QueueIn.Get(k+1), 0);
                if CurvStruct1.Type == CurveType.Line && QueueIn.Get(k+1).Type == CurveType.Line ...
                        && norm(cross(r0D1, r1D1)) < 1e-6
                    QueueOut.Push(CurvStruct1);
                    CurvStruct1 = QueueIn.Get(k+1);
                    kk = kk + 1;
                else
                    % If the the segment length is between 2 and 3 cutoffs, use a
                    % half cutoff
                   [CurvStruct1_C, CurvStruct_T, CurvStruct2_C]  = ...
                    CalcTransition(CurvStruct1, QueueIn.Get(k+1), CutOff/2); 

                    QueueOut.Push(CurvStruct1_C); % CurvStructs1(kk)   = CurvStruct1_C;
                    QueueOut.Push(CurvStruct_T); % CurvStructs1(kk+1) = CurvStruct_T;
                    kk = kk + 2;
                    CurvStruct1        = CurvStruct2_C;
                end
                k0 = k+1;
            else
                DebugLog('!!! SKIPPING CURVE !!!\n');
                PrintCurvStruct(QueueIn.Get(k+1));
            end
        else
            QueueOut.Push(QueueIn.Get(k)); % CurvStructs1(kk)   = CurvStructs(k);
            kk = kk + 1;
            CurvStruct1 = QueueIn.Get(k+1); % CurvStruct1 = CurvStructs(k+1);
        end
    end

    %
    % if (CurvStructs(end-1).ZSpdMode ~= ZSpdMode.NZ) && (CurvStructs(end-1).ZSpdMode ~= ZSpdMode.ZZ)
    %     CurvStructs1(end) = CurvStruct2_C;
    % else
    %     CurvStructs1(end) = CurvStructs(end);
    % end
    
    if (QueueIn.RGet(1).ZSpdMode ~= ZSpdMode.NZ) && (QueueIn.RGet(1).ZSpdMode ~= ZSpdMode.ZZ)
%         QueueOut.RSet(0, CurvStruct2_C);
        QueueOut.Push(CurvStruct2_C);
    else
%         QueueOut.RSet(0, QueueIn.RGet(0));
        QueueOut.Push(QueueIn.RGet(0));
    end
else % if Ncrv > 1
    QueueOut.Push(QueueIn.Get(1));
end


