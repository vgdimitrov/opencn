function CurvStructCut = CutLine(CurvStruct, d0, d1)
P0            = CurvStruct.P0;
P1            = CurvStruct.P1;
l             = MyNorm(P1 - P0);
P0_prim       = (d0/l)  * P1 + (1-d0/l) * P0;
P1_prim       = (1-d1/l)* P1 + (d1/l)   * P0;
CurvStructCut = ConstrLineStruct(P0_prim, P1_prim, CurvStruct.FeedRate, CurvStruct.ZSpdMode);
