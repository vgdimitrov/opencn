function value = QueueSize(id)
global g_QueueData
value = length(g_QueueData{id});
end