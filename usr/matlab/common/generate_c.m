clear; clc;
cfg = coder.config('lib', 'ecoder', true);
cfg.IndentSize = 4;
cfg.BuildConfiguration = 'Faster Runs';
% cfg.ColumnLimit = 100;
cfg.GenCodeOnly = true;
cfg.FilePartitionMethod = 'MapMFileTOCFile';
cfg.PreserveArrayDimensions = true;
cfg.GenerateReport = false;
cfg.GenerateCodeMetricsReport = false;
cfg.SupportNonFinite = false;
cfg.TargetLang = 'C';
cfg.TargetLangStandard = 'C89/C90 (ANSI)';
% cfg.CodeReplacementLibrary = 'Intel AVX (Linux)';
% cfg.Hardware = 'Intel->x86-64 (Linux 64)';
% cfg.HardwareImplementation.TargetHWDeviceType = 'Intel->x86-64 (Linux 64)';
cfg.HardwareImplementation.ProdHWDeviceType = 'Intel->x86-64 (Linux 64)';

% cfg.CustomInclude = './c_functions/';
% cfg.CustomSource = './c_functions/functions.c';
% cfg.CustomLibrary = '/usr/lib/libgsl.so /usr/lib/libgslcblas.so';
cfg.CustomInclude = '../src';
cfg.CustomHeaderCode = '#include "c_simplex.hpp"';
cfg.EnableVariableSizing = true;
cfg.DynamicMemoryAllocation = 'Threshold';
cfg.SILPILCheckConstantInputs = false;
cfg.SILPILSyncGlobalData = false;
cfg.SaturateOnIntegerOverflow = false;
% cfg.StackUsageMax = 8096*1024;

cfg.HighlightPotentialDataTypeIssues = true;
cfg.HighlightPotentialRowMajorIssues = true;



addpath('c_planner_v5')

global g_FeedoptConfig
InitConfig();

Nmax = FeedoptLimits.MaxNCoeff;
Ndiscr_max = FeedoptLimits.MaxNDiscr;

cfg.DynamicMemoryAllocationThreshold = FeedoptLimits.MaxNCoeff*FeedoptLimits.MaxNDiscr*16 + 1;


%% BuildSmoothedPolygon3D
vmax   = 1.5;              % max feedrate in [m/s]
amax   = 4.5*ones(3, 1);   % max acceleration per axis [m/s^2]
jmax   = 100*ones(3, 1);   % max jerk per axis [m/s^3]

% Points = [-1,    0,    0,    -1,   -1;
%            0,    0,    1,     0,    0;
%            0,    0,    0.5, 0.5,    0];
       
Points = [0 1 1 0 0; 0 0 1 1 0; 0 0 0 0 0];
CutOff      = 0.2;
CurvStructs0 = ConstrPolygon3D(Points, vmax);
% SmoothCurvStructs(CurvStructs0, CutOff);
v_0    =  0.1;
at_0   =  0.0;
v_1    =  0.1;
at_1   =  0.0;

%% generate BSpline basis functions and their derivatives evaluated on a grid
N      = 25;    % number of discretization points on u axis
Ndiscr = 100;    % number of constraint discretization
u_vec  = linspace(0, 1, Ndiscr);
%
d       = 4;   % degree of spline
s_end   = 1;   % normalized path coordinate
%
% Bl      = BSplineBasis([0, s_end], d, N);

Bl.handle = uint64(0);
Bl.n = int32(20);

%
N_Hor = 3; % number of curve pieces in the receding horizon


ARGS_FP =  {coder.typeof(CurvStructs0, [1, Nmax], [0, 1]), amax, jmax, v_0, at_0, v_1, at_1, ...
    coder.typeof(0.0, [Ndiscr_max, Nmax], [1, 1]), ...
    coder.typeof(0.0, [Ndiscr_max, Nmax], [1, 1]),...
    coder.typeof(0.0, [Ndiscr_max, Nmax], [1, 1]),...
    coder.typeof(0.0, [Nmax, 1], [1 0]), Bl, coder.typeof(u_vec, [1, Ndiscr_max], [0, 1]), N_Hor};

P0 = Points(:, 1);
P1 = Points(:, 2);
FeedRate = 1.2;
evec = P0;
theta = 0.2;
pitch = 0.3;
dt = 1e-2;

CurvStructTypeName = coder.cstructname(CurvStructs0(1), 'CurvStruct');
FeedoptConfigTypeName = coder.cstructname(g_FeedoptConfig, 'FeedoptConfigStruct');

OptStruct = struct('Coeff', zeros(g_FeedoptConfig.MaxNCoeff, 1), 'CurvStruct', CurvStructs0(1));
OptStructName = coder.cstructname(OptStruct, 'OptStruct');

QueueGCode = Queue(QueueId.GCode);
QueueSmooth = Queue(QueueId.Smooth);
QueueSplit = Queue(QueueId.Split);
QueueOpt = Queue(QueueId.Opt);

% Qstruct = struct(QueueOpt);
% QueueTypeName = coder.cstructname(Qstruct, 'QueueHandle');

% %     'RecedingHorizon', '-args', ARGS_RH,...
% FEEDOPT USERSPACE
codegen('-config', cfg,'-d', '../generated', ...
    'FeedoptTypes', '-args', {CurvStructTypeName, FeedoptConfigTypeName, QueueId.GCode, PushStatus.Success},...
    'c_alloc_matrix', '-args', {2,3},...
    'c_linspace', '-args', {2,3,int32(4)},...
    'sinspace', '-args', {2,3,int32(20)},...
    'ConstrLineStruct', '-args', {P0, P1, FeedRate, ZSpdMode.NN},...
    'ConstrHelixStruct', '-args', {P0, P1, evec, theta, pitch, FeedRate, ZSpdMode.NN},...
    'EvalCurvStruct', '-args', {CurvStructTypeName, coder.typeof(u_vec, [1, Ndiscr_max], [0, 1])},...
    'Calc_u_v4', '-args', {Bl, coder.typeof(0, [Nmax, 1], [1, 0]), dt},...
    'c_roots_', '-args', coder.typeof(0, [1, 12], [0, 1]),...
    'FeedoptPlan', '-args', {Fopt.Opt},...
    'PrintCurvStruct', '-args', QueueGCode.ValueType,...
    'InitConfig',...
    'CalcTransition', '-args', {CurvStructs0(1), CurvStructs0(2), 0.2},...
    'FeedoptDefaultConfig',...
    'BenchmarkFeedratePlanning', '-args', {coder.typeof(CurvStructs0(1), [1, 10], [0,1]), int32(100)}, ...
    'ResampleTick', '-args', {OptStructName, OptStructName, Bl, 0.0, 1.0});


cfg.EnableMemcpy = false;           % Disabme memcopy
cfg.InitFltsAndDblsToZero = false;  % Disable memset

% FEEDOPT KERNEL REALTIME
codegen('-config', cfg,'-d', '../../../linux-4.14-amp/opencn/matlab/generated', ...
    'FeedoptTypes', '-args', {CurvStructTypeName, FeedoptConfigTypeName, QueueId.GCode, PushStatus.Success},...
    'c_linspace', '-args', {2,3,int32(4)},...
    'sinspace', '-args', {2,3,int32(20)},...
    'EvalCurvStruct', '-args', {CurvStructTypeName, coder.typeof(u_vec, [1, Ndiscr_max], [0, 1])},...
    'PrintCurvStruct', '-args', QueueGCode.ValueType,...
    'InitConfig',...
    'FeedoptDefaultConfig',...
    'ResampleTick', '-args', {OptStructName, OptStructName, Bl, 0.0, 1.0});





%     'BenchmarkFeedratePlanning', '-args', {coder.typeof(CurvStructs0(1), [1, 10], [0,1]), int32(100)});
        %     'c_simplex', '-args', {f, A, b, Aeq, beq});
%     'SmoothCurvStructs', '-args', {QueueGCode, QueueSmooth, CutOff},...


