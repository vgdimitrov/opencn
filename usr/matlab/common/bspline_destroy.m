function bspline_destroy(Bl) %#codegen
    if coder.target('rtw') || coder.target('mex')
        coder.updateBuildInfo('addSourceFiles','c_spline.c');
        coder.updateBuildInfo('addLinkFlags', '-lgsl');
        coder.cinclude('c_spline.h');
        coder.ceval('c_bspline_destroy', coder.rref(Bl.handle));
    else
        bspline_destroy_mex(Bl);
    end
end