/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: ResampleTick.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

#ifndef RESAMPLETICK_H
#define RESAMPLETICK_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Function Declarations */
extern void ResampleTick(const OptStruct *CurOptStruct, const OptStruct
    *NextOptStruct, const struct0_T *b_Bl, double *u, double dt, boolean_T
    *pop_next, boolean_T *finished);

#endif

/*
 * File trailer for ResampleTick.h
 *
 * [EOF]
 */
