/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: BuildConstrJerk_v4.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "BuildConstrJerk_v4.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_emxutil.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "bsxfun.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"
#include "sparse.h"
#include "sparse1.h"
#include <math.h>
#include <string.h>

/* Function Definitions */

/*
 * Arguments    : const CurvStruct CurvStructs_data[]
 *                const int CurvStructs_size[2]
 *                const double Coeff_data[]
 *                const int Coeff_size[2]
 *                const double jmax[3]
 *                const emxArray_real_T *b_BasisVal
 *                const emxArray_real_T *b_BasisValD
 *                const emxArray_real_T *b_BasisValDD
 *                const double u_vec_data[]
 *                const int u_vec_size[2]
 *                coder_internal_sparse *A
 *                double b_data[]
 *                int b_size[1]
 * Return Type  : void
 */
void BuildConstrJerk_v4(const CurvStruct CurvStructs_data[], const int
                        CurvStructs_size[2], const double Coeff_data[], const
                        int Coeff_size[2], const double jmax[3], const
                        emxArray_real_T *b_BasisVal, const emxArray_real_T
                        *b_BasisValD, const emxArray_real_T *b_BasisValDD, const
                        double u_vec_data[], const int u_vec_size[2],
                        coder_internal_sparse *A, double b_data[], int b_size[1])
{
    int N;
    int M;
    int inner;
    int m;
    int q_val_size[1];
    int loop_ub;
    int i;
    double q_val_data[200];
    int k;
    int b_u_vec_size[2];
    int i1;
    int input_sizes_idx_0;
    double b_u_vec_data[200];
    double unusedU0_data[600];
    int unusedU0_size[2];
    double r1D_data[600];
    int r1D_size[2];
    double r2D_data[600];
    int r2D_size[2];
    double r3D_data[600];
    int r3D_size[2];
    int b_r3D_size[1];
    emxArray_real_T *R3;
    emxArray_real_T *varargin_2;
    emxArray_real_T *varargin_4;
    emxArray_real_T *R1;
    emxArray_real_T *R2;
    emxArray_real_T *varargin_6;
    boolean_T empty_non_axis_sizes;
    int b_input_sizes_idx_0;
    int c_input_sizes_idx_0;
    int d_input_sizes_idx_0;
    int e_input_sizes_idx_0;
    emxArray_real_T *y;
    double b;
    emxArray_real_T *b_y;
    emxArray_real_T *b_R1;
    int f_input_sizes_idx_0;
    int i2;
    int bC2_size_idx_0;
    int bC3_size_idx_0;
    double bC2_data[200];
    int bC4_size_idx_0;
    double bC3_data[200];
    double bC4_data[200];
    int c_u_vec_size[2];
    double a_tmp;
    double b_tmp;
    double a;
    double b_bC2_data[1200];
    PROF_BLOCK(BuildConstrJerk_v4);

    /*  import splines.* */
    /*  */
    N = b_BasisVal->size[1];
    M = b_BasisVal->size[0];

    /*  */
    sparse(6.0 * (double)b_BasisVal->size[0] * (double)CurvStructs_size[1],
           (double)b_BasisVal->size[1] * (double)CurvStructs_size[1], A->d,
           A->colidx, A->rowidx, &A->m, &A->n, &A->maxnz);

    /*  preallocation */
    inner = 6 * b_BasisVal->size[0] * CurvStructs_size[1];
    b_size[0] = inner;
    if (0 <= inner - 1) {
        memset(&b_data[0], 0, inner * sizeof(double));
    }

    /*  preallocation */
    /*  coder.varsize('A', [6*FeedoptLimits.MaxNDiscr*FeedoptLimits.MaxNHorz, FeedoptLimits.MaxNCoeff*FeedoptLimits.MaxNHorz], [1,1]); */
    /*  coder.varsize('b', [6*FeedoptLimits.MaxNDiscr*FeedoptLimits.MaxNHorz, 1], [1,0]); */
    /*  */
    /*  q_opt  = Function(Bl, Coeff(:, 1)); */
    /*  q_val  = q_opt.fast_eval(u_vec); */
    if ((b_BasisVal->size[1] == 1) || (Coeff_size[0] == 1)) {
        q_val_size[0] = b_BasisVal->size[0];
        loop_ub = b_BasisVal->size[0];
        for (i = 0; i < loop_ub; i++) {
            q_val_data[i] = 0.0;
            inner = b_BasisVal->size[1];
            for (i1 = 0; i1 < inner; i1++) {
                q_val_data[i] += b_BasisVal->data[i + b_BasisVal->size[0] * i1] *
                    Coeff_data[i1];
            }
        }
    } else {
        m = b_BasisVal->size[0] - 1;
        inner = b_BasisVal->size[1];
        q_val_size[0] = b_BasisVal->size[0];
        if (0 <= m) {
            memset(&q_val_data[0], 0, (m + 1) * sizeof(double));
        }

        for (k = 0; k < inner; k++) {
            for (input_sizes_idx_0 = 0; input_sizes_idx_0 <= m;
                    input_sizes_idx_0++) {
                q_val_data[input_sizes_idx_0] += Coeff_data[k] *
                    b_BasisVal->data[input_sizes_idx_0 + b_BasisVal->size[0] * k];
            }
        }
    }

    b_u_vec_size[0] = 1;
    b_u_vec_size[1] = u_vec_size[1];
    loop_ub = u_vec_size[1] - 1;
    if (0 <= loop_ub) {
        memcpy(&b_u_vec_data[0], &u_vec_data[0], (loop_ub + 1) * sizeof(double));
    }

    e_EvalCurvStruct(CurvStructs_data[0].Type, CurvStructs_data[0].P0,
                     CurvStructs_data[0].P1, CurvStructs_data[0].evec,
                     CurvStructs_data[0].theta, CurvStructs_data[0].pitch,
                     *(double (*)[6][3])&CurvStructs_data[0].CoeffP5[0][0],
                     b_u_vec_data, b_u_vec_size, unusedU0_data, unusedU0_size,
                     r1D_data, r1D_size, r2D_data, r2D_size, r3D_data, r3D_size);

    /*  */
    i = q_val_size[0];
    for (k = 0; k < i; k++) {
        q_val_data[k] = sqrt(q_val_data[k]);
    }

    loop_ub = r3D_size[1];
    b_r3D_size[0] = r3D_size[1];
    for (i = 0; i < loop_ub; i++) {
        b_u_vec_data[i] = r3D_data[3 * i];
    }

    emxInit_real_T(&R3, 2);
    b_bsxfun(b_BasisVal, b_u_vec_data, b_r3D_size, R3);
    loop_ub = r2D_size[1];
    b_r3D_size[0] = r2D_size[1];
    for (i = 0; i < loop_ub; i++) {
        b_u_vec_data[i] = r2D_data[3 * i];
    }

    emxInit_real_T(&varargin_2, 2);
    b_bsxfun(b_BasisValD, b_u_vec_data, b_r3D_size, varargin_2);
    loop_ub = r1D_size[1];
    b_r3D_size[0] = r1D_size[1];
    for (i = 0; i < loop_ub; i++) {
        b_u_vec_data[i] = r1D_data[3 * i];
    }

    emxInit_real_T(&varargin_4, 2);
    b_bsxfun(b_BasisValDD, b_u_vec_data, b_r3D_size, varargin_4);
    loop_ub = R3->size[1];
    for (i = 0; i < loop_ub; i++) {
        inner = R3->size[0];
        for (i1 = 0; i1 < inner; i1++) {
            R3->data[i1 + R3->size[0] * i] = (R3->data[i1 + R3->size[0] * i] +
                1.5 * varargin_2->data[i1 + varargin_2->size[0] * i]) + 0.5 *
                varargin_4->data[i1 + varargin_4->size[0] * i];
        }
    }

    emxInit_real_T(&R1, 2);
    b_bsxfun(R3, q_val_data, q_val_size, R1);
    loop_ub = r3D_size[1];
    b_r3D_size[0] = r3D_size[1];
    for (i = 0; i < loop_ub; i++) {
        b_u_vec_data[i] = r3D_data[3 * i + 1];
    }

    b_bsxfun(b_BasisVal, b_u_vec_data, b_r3D_size, R3);
    loop_ub = r2D_size[1];
    b_r3D_size[0] = r2D_size[1];
    for (i = 0; i < loop_ub; i++) {
        b_u_vec_data[i] = r2D_data[3 * i + 1];
    }

    b_bsxfun(b_BasisValD, b_u_vec_data, b_r3D_size, varargin_2);
    loop_ub = r1D_size[1];
    b_r3D_size[0] = r1D_size[1];
    for (i = 0; i < loop_ub; i++) {
        b_u_vec_data[i] = r1D_data[3 * i + 1];
    }

    b_bsxfun(b_BasisValDD, b_u_vec_data, b_r3D_size, varargin_4);
    loop_ub = R3->size[1];
    for (i = 0; i < loop_ub; i++) {
        inner = R3->size[0];
        for (i1 = 0; i1 < inner; i1++) {
            R3->data[i1 + R3->size[0] * i] = (R3->data[i1 + R3->size[0] * i] +
                1.5 * varargin_2->data[i1 + varargin_2->size[0] * i]) + 0.5 *
                varargin_4->data[i1 + varargin_4->size[0] * i];
        }
    }

    emxInit_real_T(&R2, 2);
    b_bsxfun(R3, q_val_data, q_val_size, R2);
    loop_ub = r3D_size[1];
    b_r3D_size[0] = r3D_size[1];
    for (i = 0; i < loop_ub; i++) {
        b_u_vec_data[i] = r3D_data[3 * i + 2];
    }

    b_bsxfun(b_BasisVal, b_u_vec_data, b_r3D_size, R3);
    loop_ub = r2D_size[1];
    b_r3D_size[0] = r2D_size[1];
    for (i = 0; i < loop_ub; i++) {
        b_u_vec_data[i] = r2D_data[3 * i + 2];
    }

    b_bsxfun(b_BasisValD, b_u_vec_data, b_r3D_size, varargin_2);
    loop_ub = r1D_size[1];
    b_r3D_size[0] = r1D_size[1];
    for (i = 0; i < loop_ub; i++) {
        b_u_vec_data[i] = r1D_data[3 * i + 2];
    }

    b_bsxfun(b_BasisValDD, b_u_vec_data, b_r3D_size, varargin_4);
    i = varargin_2->size[0] * varargin_2->size[1];
    varargin_2->size[0] = R3->size[0];
    varargin_2->size[1] = R3->size[1];
    emxEnsureCapacity_real_T(varargin_2, i);
    loop_ub = R3->size[1];
    for (i = 0; i < loop_ub; i++) {
        inner = R3->size[0];
        for (i1 = 0; i1 < inner; i1++) {
            varargin_2->data[i1 + varargin_2->size[0] * i] = (R3->data[i1 +
                R3->size[0] * i] + 1.5 * varargin_2->data[i1 + varargin_2->size
                [0] * i]) + 0.5 * varargin_4->data[i1 + varargin_4->size[0] * i];
        }
    }

    b_bsxfun(varargin_2, q_val_data, q_val_size, R3);

    /*  R2 = (BasisVal .* r3D(2, :)' + ... */
    /*        1.5*BasisValD .* r2D(2, :)' + ... */
    /*        0.5*BasisValDD.*r1D(2, :)') .* sqrt(q_val); */
    /*  R3 = (BasisVal .* r3D(3, :)' + ... */
    /*        1.5*BasisValD .* r2D(3, :)' + ... */
    /*        0.5*BasisValDD.*r1D(3, :)') .* sqrt(q_val); */
    /*  */
    i = varargin_2->size[0] * varargin_2->size[1];
    varargin_2->size[0] = R1->size[0];
    varargin_2->size[1] = R1->size[1];
    emxEnsureCapacity_real_T(varargin_2, i);
    loop_ub = R1->size[1];
    for (i = 0; i < loop_ub; i++) {
        inner = R1->size[0];
        for (i1 = 0; i1 < inner; i1++) {
            varargin_2->data[i1 + varargin_2->size[0] * i] = -R1->data[i1 +
                R1->size[0] * i];
        }
    }

    i = varargin_4->size[0] * varargin_4->size[1];
    varargin_4->size[0] = R2->size[0];
    varargin_4->size[1] = R2->size[1];
    emxEnsureCapacity_real_T(varargin_4, i);
    loop_ub = R2->size[1];
    for (i = 0; i < loop_ub; i++) {
        inner = R2->size[0];
        for (i1 = 0; i1 < inner; i1++) {
            varargin_4->data[i1 + varargin_4->size[0] * i] = -R2->data[i1 +
                R2->size[0] * i];
        }
    }

    emxInit_real_T(&varargin_6, 2);
    i = varargin_6->size[0] * varargin_6->size[1];
    varargin_6->size[0] = R3->size[0];
    varargin_6->size[1] = R3->size[1];
    emxEnsureCapacity_real_T(varargin_6, i);
    loop_ub = R3->size[1];
    for (i = 0; i < loop_ub; i++) {
        inner = R3->size[0];
        for (i1 = 0; i1 < inner; i1++) {
            varargin_6->data[i1 + varargin_6->size[0] * i] = -R3->data[i1 +
                R3->size[0] * i];
        }
    }

    if ((R1->size[0] != 0) && (R1->size[1] != 0)) {
        m = R1->size[1];
    } else if ((varargin_2->size[0] != 0) && (varargin_2->size[1] != 0)) {
        m = varargin_2->size[1];
    } else if ((R2->size[0] != 0) && (R2->size[1] != 0)) {
        m = R2->size[1];
    } else if ((varargin_4->size[0] != 0) && (varargin_4->size[1] != 0)) {
        m = varargin_4->size[1];
    } else if ((R3->size[0] != 0) && (R3->size[1] != 0)) {
        m = R3->size[1];
    } else if ((varargin_6->size[0] != 0) && (varargin_6->size[1] != 0)) {
        m = varargin_6->size[1];
    } else {
        m = R1->size[1];
        if (m <= 0) {
            m = 0;
        }

        if (varargin_2->size[1] > m) {
            m = varargin_2->size[1];
        }

        if (R2->size[1] > m) {
            m = R2->size[1];
        }

        if (varargin_4->size[1] > m) {
            m = varargin_4->size[1];
        }

        if (R3->size[1] > m) {
            m = R3->size[1];
        }

        if (varargin_6->size[1] > m) {
            m = varargin_6->size[1];
        }
    }

    empty_non_axis_sizes = (m == 0);
    if (empty_non_axis_sizes || ((R1->size[0] != 0) && (R1->size[1] != 0))) {
        input_sizes_idx_0 = R1->size[0];
    } else {
        input_sizes_idx_0 = 0;
    }

    if (empty_non_axis_sizes || ((varargin_2->size[0] != 0) && (varargin_2->
            size[1] != 0))) {
        b_input_sizes_idx_0 = varargin_2->size[0];
    } else {
        b_input_sizes_idx_0 = 0;
    }

    if (empty_non_axis_sizes || ((R2->size[0] != 0) && (R2->size[1] != 0))) {
        c_input_sizes_idx_0 = R2->size[0];
    } else {
        c_input_sizes_idx_0 = 0;
    }

    if (empty_non_axis_sizes || ((varargin_4->size[0] != 0) && (varargin_4->
            size[1] != 0))) {
        d_input_sizes_idx_0 = varargin_4->size[0];
    } else {
        d_input_sizes_idx_0 = 0;
    }

    if (empty_non_axis_sizes || ((R3->size[0] != 0) && (R3->size[1] != 0))) {
        e_input_sizes_idx_0 = R3->size[0];
    } else {
        e_input_sizes_idx_0 = 0;
    }

    if (empty_non_axis_sizes || ((varargin_6->size[0] != 0) && (varargin_6->
            size[1] != 0))) {
        inner = varargin_6->size[0];
    } else {
        inner = 0;
    }

    emxInit_real_T(&y, 2);
    b = 6.0 * (double)b_BasisVal->size[0];
    if (b < 1.0) {
        y->size[0] = 1;
        y->size[1] = 0;
    } else {
        i = y->size[0] * y->size[1];
        y->size[0] = 1;
        loop_ub = (int)(b - 1.0);
        y->size[1] = loop_ub + 1;
        emxEnsureCapacity_real_T(y, i);
        for (i = 0; i <= loop_ub; i++) {
            y->data[i] = (double)i + 1.0;
        }
    }

    emxInit_real_T(&b_y, 2);
    if (b_BasisVal->size[1] < 1) {
        b_y->size[0] = 1;
        b_y->size[1] = 0;
    } else {
        i = b_y->size[0] * b_y->size[1];
        b_y->size[0] = 1;
        b_y->size[1] = b_BasisVal->size[1];
        emxEnsureCapacity_real_T(b_y, i);
        loop_ub = b_BasisVal->size[1] - 1;
        for (i = 0; i <= loop_ub; i++) {
            b_y->data[i] = (double)i + 1.0;
        }
    }

    emxInit_real_T(&b_R1, 2);
    f_input_sizes_idx_0 = input_sizes_idx_0;
    input_sizes_idx_0 = b_input_sizes_idx_0;
    b_input_sizes_idx_0 = c_input_sizes_idx_0;
    i = f_input_sizes_idx_0 + input_sizes_idx_0;
    i1 = b_R1->size[0] * b_R1->size[1];
    b_R1->size[0] = (((i + b_input_sizes_idx_0) + d_input_sizes_idx_0) +
                     e_input_sizes_idx_0) + inner;
    b_R1->size[1] = m;
    emxEnsureCapacity_real_T(b_R1, i1);
    for (i1 = 0; i1 < m; i1++) {
        for (i2 = 0; i2 < f_input_sizes_idx_0; i2++) {
            b_R1->data[i2 + b_R1->size[0] * i1] = R1->data[i2 +
                f_input_sizes_idx_0 * i1];
        }
    }

    for (i1 = 0; i1 < m; i1++) {
        for (i2 = 0; i2 < input_sizes_idx_0; i2++) {
            b_R1->data[(i2 + f_input_sizes_idx_0) + b_R1->size[0] * i1] =
                varargin_2->data[i2 + input_sizes_idx_0 * i1];
        }
    }

    for (i1 = 0; i1 < m; i1++) {
        for (i2 = 0; i2 < b_input_sizes_idx_0; i2++) {
            b_R1->data[((i2 + f_input_sizes_idx_0) + input_sizes_idx_0) +
                b_R1->size[0] * i1] = R2->data[i2 + b_input_sizes_idx_0 * i1];
        }
    }

    for (i1 = 0; i1 < m; i1++) {
        for (i2 = 0; i2 < d_input_sizes_idx_0; i2++) {
            b_R1->data[(((i2 + f_input_sizes_idx_0) + input_sizes_idx_0) +
                        b_input_sizes_idx_0) + b_R1->size[0] * i1] =
                varargin_4->data[i2 + d_input_sizes_idx_0 * i1];
        }
    }

    for (i1 = 0; i1 < m; i1++) {
        for (i2 = 0; i2 < e_input_sizes_idx_0; i2++) {
            b_R1->data[((((i2 + f_input_sizes_idx_0) + input_sizes_idx_0) +
                         b_input_sizes_idx_0) + d_input_sizes_idx_0) +
                b_R1->size[0] * i1] = R3->data[i2 + e_input_sizes_idx_0 * i1];
        }
    }

    for (i1 = 0; i1 < m; i1++) {
        for (i2 = 0; i2 < inner; i2++) {
            b_R1->data[((((i2 + i) + b_input_sizes_idx_0) + d_input_sizes_idx_0)
                        + e_input_sizes_idx_0) + b_R1->size[0] * i1] =
                varargin_6->data[i2 + inner * i1];
        }
    }

    sparse_parenAssign(A, b_R1, y->data, y->size, b_y);

    /*  */
    bC2_size_idx_0 = b_BasisVal->size[0];
    loop_ub = b_BasisVal->size[0];
    for (i = 0; i < loop_ub; i++) {
        bC2_data[i] = jmax[0];
    }

    bC3_size_idx_0 = b_BasisVal->size[0];
    loop_ub = b_BasisVal->size[0];
    for (i = 0; i < loop_ub; i++) {
        bC3_data[i] = jmax[1];
    }

    bC4_size_idx_0 = b_BasisVal->size[0];
    loop_ub = b_BasisVal->size[0];
    for (i = 0; i < loop_ub; i++) {
        bC4_data[i] = jmax[2];
    }

    /*  */
    if (0 <= bC2_size_idx_0 - 1) {
        memcpy(&b_data[0], &bC2_data[0], bC2_size_idx_0 * sizeof(double));
    }

    for (i = 0; i < bC2_size_idx_0; i++) {
        b_data[i + bC2_size_idx_0] = bC2_data[i];
    }

    for (i = 0; i < bC3_size_idx_0; i++) {
        b_data[(i + bC2_size_idx_0) + bC2_size_idx_0] = bC3_data[i];
    }

    for (i = 0; i < bC3_size_idx_0; i++) {
        b_data[((i + bC2_size_idx_0) + bC2_size_idx_0) + bC3_size_idx_0] =
            bC3_data[i];
    }

    for (i = 0; i < bC4_size_idx_0; i++) {
        b_data[(((i + bC2_size_idx_0) + bC2_size_idx_0) + bC3_size_idx_0) +
            bC3_size_idx_0] = bC4_data[i];
    }

    for (i = 0; i < bC4_size_idx_0; i++) {
        b_data[((((i + bC2_size_idx_0) + bC2_size_idx_0) + bC3_size_idx_0) +
                bC3_size_idx_0) + bC4_size_idx_0] = bC4_data[i];
    }

    /*  */
    i = CurvStructs_size[1];
    for (k = 0; k <= i - 2; k++) {
        c_u_vec_size[0] = 1;
        c_u_vec_size[1] = u_vec_size[1];
        loop_ub = u_vec_size[1] - 1;
        if (0 <= loop_ub) {
            memcpy(&b_u_vec_data[0], &u_vec_data[0], (loop_ub + 1) * sizeof
                   (double));
        }

        e_EvalCurvStruct(CurvStructs_data[k + 1].Type, CurvStructs_data[k + 1].
                         P0, CurvStructs_data[k + 1].P1, CurvStructs_data[k + 1]
                         .evec, CurvStructs_data[k + 1].theta,
                         CurvStructs_data[k + 1].pitch, *(double (*)[6][3])&
                         CurvStructs_data[k + 1].CoeffP5[0][0], b_u_vec_data,
                         c_u_vec_size, unusedU0_data, unusedU0_size, r1D_data,
                         r1D_size, r2D_data, r2D_size, r3D_data, r3D_size);

        /*  */
        /*      q_opt  = Function(Bl, Coeff(:, k+1)); */
        /*      q_val  = q_opt.fast_eval(u_vec); */
        i1 = b_BasisVal->size[1];
        if ((b_BasisVal->size[1] == 1) || (Coeff_size[0] == 1)) {
            loop_ub = b_BasisVal->size[0];
            q_val_size[0] = b_BasisVal->size[0];
            for (i1 = 0; i1 < loop_ub; i1++) {
                q_val_data[i1] = 0.0;
                inner = b_BasisVal->size[1];
                for (i2 = 0; i2 < inner; i2++) {
                    q_val_data[i1] += b_BasisVal->data[i1 + b_BasisVal->size[0] *
                        i2] * Coeff_data[i2 + Coeff_size[0] * (k + 1)];
                }
            }
        } else {
            m = b_BasisVal->size[0] - 1;
            q_val_size[0] = b_BasisVal->size[0];
            if (0 <= m) {
                memset(&q_val_data[0], 0, (m + 1) * sizeof(double));
            }

            for (inner = 0; inner < i1; inner++) {
                for (input_sizes_idx_0 = 0; input_sizes_idx_0 <= m;
                        input_sizes_idx_0++) {
                    q_val_data[input_sizes_idx_0] += Coeff_data[inner +
                        Coeff_size[0] * (k + 1)] * b_BasisVal->
                        data[input_sizes_idx_0 + b_BasisVal->size[0] * inner];
                }
            }
        }

        /*  */
        i1 = q_val_size[0];
        for (inner = 0; inner < i1; inner++) {
            q_val_data[inner] = sqrt(q_val_data[inner]);
        }

        loop_ub = r3D_size[1];
        b_r3D_size[0] = r3D_size[1];
        for (i1 = 0; i1 < loop_ub; i1++) {
            b_u_vec_data[i1] = r3D_data[3 * i1];
        }

        b_bsxfun(b_BasisVal, b_u_vec_data, b_r3D_size, R3);
        loop_ub = r2D_size[1];
        b_r3D_size[0] = r2D_size[1];
        for (i1 = 0; i1 < loop_ub; i1++) {
            b_u_vec_data[i1] = r2D_data[3 * i1];
        }

        b_bsxfun(b_BasisValD, b_u_vec_data, b_r3D_size, varargin_2);
        loop_ub = r1D_size[1];
        b_r3D_size[0] = r1D_size[1];
        for (i1 = 0; i1 < loop_ub; i1++) {
            b_u_vec_data[i1] = r1D_data[3 * i1];
        }

        b_bsxfun(b_BasisValDD, b_u_vec_data, b_r3D_size, varargin_4);
        loop_ub = R3->size[1];
        for (i1 = 0; i1 < loop_ub; i1++) {
            inner = R3->size[0];
            for (i2 = 0; i2 < inner; i2++) {
                R3->data[i2 + R3->size[0] * i1] = (R3->data[i2 + R3->size[0] *
                    i1] + 1.5 * varargin_2->data[i2 + varargin_2->size[0] * i1])
                    + 0.5 * varargin_4->data[i2 + varargin_4->size[0] * i1];
            }
        }

        b_bsxfun(R3, q_val_data, q_val_size, R1);
        loop_ub = r3D_size[1];
        b_r3D_size[0] = r3D_size[1];
        for (i1 = 0; i1 < loop_ub; i1++) {
            b_u_vec_data[i1] = r3D_data[3 * i1 + 1];
        }

        b_bsxfun(b_BasisVal, b_u_vec_data, b_r3D_size, R3);
        loop_ub = r2D_size[1];
        b_r3D_size[0] = r2D_size[1];
        for (i1 = 0; i1 < loop_ub; i1++) {
            b_u_vec_data[i1] = r2D_data[3 * i1 + 1];
        }

        b_bsxfun(b_BasisValD, b_u_vec_data, b_r3D_size, varargin_2);
        loop_ub = r1D_size[1];
        b_r3D_size[0] = r1D_size[1];
        for (i1 = 0; i1 < loop_ub; i1++) {
            b_u_vec_data[i1] = r1D_data[3 * i1 + 1];
        }

        b_bsxfun(b_BasisValDD, b_u_vec_data, b_r3D_size, varargin_4);
        loop_ub = R3->size[1];
        for (i1 = 0; i1 < loop_ub; i1++) {
            inner = R3->size[0];
            for (i2 = 0; i2 < inner; i2++) {
                R3->data[i2 + R3->size[0] * i1] = (R3->data[i2 + R3->size[0] *
                    i1] + 1.5 * varargin_2->data[i2 + varargin_2->size[0] * i1])
                    + 0.5 * varargin_4->data[i2 + varargin_4->size[0] * i1];
            }
        }

        b_bsxfun(R3, q_val_data, q_val_size, R2);
        loop_ub = r3D_size[1];
        b_r3D_size[0] = r3D_size[1];
        for (i1 = 0; i1 < loop_ub; i1++) {
            b_u_vec_data[i1] = r3D_data[3 * i1 + 2];
        }

        b_bsxfun(b_BasisVal, b_u_vec_data, b_r3D_size, R3);
        loop_ub = r2D_size[1];
        b_r3D_size[0] = r2D_size[1];
        for (i1 = 0; i1 < loop_ub; i1++) {
            b_u_vec_data[i1] = r2D_data[3 * i1 + 2];
        }

        b_bsxfun(b_BasisValD, b_u_vec_data, b_r3D_size, varargin_2);
        loop_ub = r1D_size[1];
        b_r3D_size[0] = r1D_size[1];
        for (i1 = 0; i1 < loop_ub; i1++) {
            b_u_vec_data[i1] = r1D_data[3 * i1 + 2];
        }

        b_bsxfun(b_BasisValDD, b_u_vec_data, b_r3D_size, varargin_4);
        i1 = varargin_2->size[0] * varargin_2->size[1];
        varargin_2->size[0] = R3->size[0];
        varargin_2->size[1] = R3->size[1];
        emxEnsureCapacity_real_T(varargin_2, i1);
        loop_ub = R3->size[1];
        for (i1 = 0; i1 < loop_ub; i1++) {
            inner = R3->size[0];
            for (i2 = 0; i2 < inner; i2++) {
                varargin_2->data[i2 + varargin_2->size[0] * i1] = (R3->data[i2 +
                    R3->size[0] * i1] + 1.5 * varargin_2->data[i2 +
                    varargin_2->size[0] * i1]) + 0.5 * varargin_4->data[i2 +
                    varargin_4->size[0] * i1];
            }
        }

        b_bsxfun(varargin_2, q_val_data, q_val_size, R3);

        /*      R2 = (BasisVal .* r3D(2, :)' + ... */
        /*            1.5*BasisValD .* r2D(2, :)' + ... */
        /*            0.5*BasisValDD.*r1D(2, :)') .* sqrt(q_val); */
        /*      R3 = (BasisVal .* r3D(3, :)' + ... */
        /*            1.5*BasisValD .* r2D(3, :)' + ... */
        /*            0.5*BasisValDD.*r1D(3, :)') .* sqrt(q_val); */
        /*  */
        i1 = varargin_2->size[0] * varargin_2->size[1];
        varargin_2->size[0] = R1->size[0];
        varargin_2->size[1] = R1->size[1];
        emxEnsureCapacity_real_T(varargin_2, i1);
        loop_ub = R1->size[1];
        for (i1 = 0; i1 < loop_ub; i1++) {
            inner = R1->size[0];
            for (i2 = 0; i2 < inner; i2++) {
                varargin_2->data[i2 + varargin_2->size[0] * i1] = -R1->data[i2 +
                    R1->size[0] * i1];
            }
        }

        i1 = varargin_4->size[0] * varargin_4->size[1];
        varargin_4->size[0] = R2->size[0];
        varargin_4->size[1] = R2->size[1];
        emxEnsureCapacity_real_T(varargin_4, i1);
        loop_ub = R2->size[1];
        for (i1 = 0; i1 < loop_ub; i1++) {
            inner = R2->size[0];
            for (i2 = 0; i2 < inner; i2++) {
                varargin_4->data[i2 + varargin_4->size[0] * i1] = -R2->data[i2 +
                    R2->size[0] * i1];
            }
        }

        i1 = varargin_6->size[0] * varargin_6->size[1];
        varargin_6->size[0] = R3->size[0];
        varargin_6->size[1] = R3->size[1];
        emxEnsureCapacity_real_T(varargin_6, i1);
        loop_ub = R3->size[1];
        for (i1 = 0; i1 < loop_ub; i1++) {
            inner = R3->size[0];
            for (i2 = 0; i2 < inner; i2++) {
                varargin_6->data[i2 + varargin_6->size[0] * i1] = -R3->data[i2 +
                    R3->size[0] * i1];
            }
        }

        if ((R1->size[0] != 0) && (R1->size[1] != 0)) {
            m = R1->size[1];
        } else if ((varargin_2->size[0] != 0) && (varargin_2->size[1] != 0)) {
            m = varargin_2->size[1];
        } else if ((R2->size[0] != 0) && (R2->size[1] != 0)) {
            m = R2->size[1];
        } else if ((varargin_4->size[0] != 0) && (varargin_4->size[1] != 0)) {
            m = varargin_4->size[1];
        } else if ((R3->size[0] != 0) && (R3->size[1] != 0)) {
            m = R3->size[1];
        } else if ((varargin_6->size[0] != 0) && (varargin_6->size[1] != 0)) {
            m = varargin_6->size[1];
        } else {
            m = R1->size[1];
            if (m <= 0) {
                m = 0;
            }

            if (varargin_2->size[1] > m) {
                m = varargin_2->size[1];
            }

            if (R2->size[1] > m) {
                m = R2->size[1];
            }

            if (varargin_4->size[1] > m) {
                m = varargin_4->size[1];
            }

            if (R3->size[1] > m) {
                m = R3->size[1];
            }

            if (varargin_6->size[1] > m) {
                m = varargin_6->size[1];
            }
        }

        empty_non_axis_sizes = (m == 0);
        if (empty_non_axis_sizes || ((R1->size[0] != 0) && (R1->size[1] != 0)))
        {
            input_sizes_idx_0 = R1->size[0];
        } else {
            input_sizes_idx_0 = 0;
        }

        if (empty_non_axis_sizes || ((varargin_2->size[0] != 0) &&
                (varargin_2->size[1] != 0))) {
            b_input_sizes_idx_0 = varargin_2->size[0];
        } else {
            b_input_sizes_idx_0 = 0;
        }

        if (empty_non_axis_sizes || ((R2->size[0] != 0) && (R2->size[1] != 0)))
        {
            c_input_sizes_idx_0 = R2->size[0];
        } else {
            c_input_sizes_idx_0 = 0;
        }

        if (empty_non_axis_sizes || ((varargin_4->size[0] != 0) &&
                (varargin_4->size[1] != 0))) {
            d_input_sizes_idx_0 = varargin_4->size[0];
        } else {
            d_input_sizes_idx_0 = 0;
        }

        if (empty_non_axis_sizes || ((R3->size[0] != 0) && (R3->size[1] != 0)))
        {
            e_input_sizes_idx_0 = R3->size[0];
        } else {
            e_input_sizes_idx_0 = 0;
        }

        if (empty_non_axis_sizes || ((varargin_6->size[0] != 0) &&
                (varargin_6->size[1] != 0))) {
            f_input_sizes_idx_0 = varargin_6->size[0];
        } else {
            f_input_sizes_idx_0 = 0;
        }

        a_tmp = ((double)k + 1.0) * 6.0 * (double)M + 1.0;
        b_tmp = (((double)k + 1.0) + 1.0) * 6.0 * (double)M;
        if (b_tmp < a_tmp) {
            y->size[0] = 1;
            y->size[1] = 0;
        } else {
            i1 = y->size[0] * y->size[1];
            y->size[0] = 1;
            loop_ub = (int)(b_tmp - a_tmp);
            y->size[1] = loop_ub + 1;
            emxEnsureCapacity_real_T(y, i1);
            for (i1 = 0; i1 <= loop_ub; i1++) {
                y->data[i1] = a_tmp + (double)i1;
            }
        }

        a = ((double)k + 1.0) * (double)N + 1.0;
        b = (((double)k + 1.0) + 1.0) * (double)N;
        if (b < a) {
            b_y->size[0] = 1;
            b_y->size[1] = 0;
        } else {
            i1 = b_y->size[0] * b_y->size[1];
            b_y->size[0] = 1;
            loop_ub = (int)(b - a);
            b_y->size[1] = loop_ub + 1;
            emxEnsureCapacity_real_T(b_y, i1);
            for (i1 = 0; i1 <= loop_ub; i1++) {
                b_y->data[i1] = a + (double)i1;
            }
        }

        i1 = b_R1->size[0] * b_R1->size[1];
        b_R1->size[0] = ((((input_sizes_idx_0 + b_input_sizes_idx_0) +
                           c_input_sizes_idx_0) + d_input_sizes_idx_0) +
                         e_input_sizes_idx_0) + f_input_sizes_idx_0;
        b_R1->size[1] = m;
        emxEnsureCapacity_real_T(b_R1, i1);
        for (i1 = 0; i1 < m; i1++) {
            for (i2 = 0; i2 < input_sizes_idx_0; i2++) {
                b_R1->data[i2 + b_R1->size[0] * i1] = R1->data[i2 +
                    input_sizes_idx_0 * i1];
            }
        }

        for (i1 = 0; i1 < m; i1++) {
            for (i2 = 0; i2 < b_input_sizes_idx_0; i2++) {
                b_R1->data[(i2 + input_sizes_idx_0) + b_R1->size[0] * i1] =
                    varargin_2->data[i2 + b_input_sizes_idx_0 * i1];
            }
        }

        for (i1 = 0; i1 < m; i1++) {
            for (i2 = 0; i2 < c_input_sizes_idx_0; i2++) {
                b_R1->data[((i2 + input_sizes_idx_0) + b_input_sizes_idx_0) +
                    b_R1->size[0] * i1] = R2->data[i2 + c_input_sizes_idx_0 * i1];
            }
        }

        for (i1 = 0; i1 < m; i1++) {
            for (i2 = 0; i2 < d_input_sizes_idx_0; i2++) {
                b_R1->data[(((i2 + input_sizes_idx_0) + b_input_sizes_idx_0) +
                            c_input_sizes_idx_0) + b_R1->size[0] * i1] =
                    varargin_4->data[i2 + d_input_sizes_idx_0 * i1];
            }
        }

        for (i1 = 0; i1 < m; i1++) {
            for (i2 = 0; i2 < e_input_sizes_idx_0; i2++) {
                b_R1->data[((((i2 + input_sizes_idx_0) + b_input_sizes_idx_0) +
                             c_input_sizes_idx_0) + d_input_sizes_idx_0) +
                    b_R1->size[0] * i1] = R3->data[i2 + e_input_sizes_idx_0 * i1];
            }
        }

        for (i1 = 0; i1 < m; i1++) {
            for (i2 = 0; i2 < f_input_sizes_idx_0; i2++) {
                b_R1->data[(((((i2 + input_sizes_idx_0) + b_input_sizes_idx_0) +
                              c_input_sizes_idx_0) + d_input_sizes_idx_0) +
                            e_input_sizes_idx_0) + b_R1->size[0] * i1] =
                    varargin_6->data[i2 + f_input_sizes_idx_0 * i1];
            }
        }

        sparse_parenAssign(A, b_R1, y->data, y->size, b_y);

        /*  */
        if (a_tmp > b_tmp) {
            i1 = -1;
            i2 = 0;
        } else {
            i1 = (int)a_tmp - 2;
            i2 = (int)b_tmp;
        }

        for (inner = 0; inner < bC2_size_idx_0; inner++) {
            b_bC2_data[inner] = bC2_data[inner];
            b_bC2_data[inner + bC2_size_idx_0] = bC2_data[inner];
        }

        for (inner = 0; inner < bC3_size_idx_0; inner++) {
            b_bC2_data[(inner + bC2_size_idx_0) + bC2_size_idx_0] =
                bC3_data[inner];
        }

        for (inner = 0; inner < bC3_size_idx_0; inner++) {
            b_bC2_data[((inner + bC2_size_idx_0) + bC2_size_idx_0) +
                bC3_size_idx_0] = bC3_data[inner];
        }

        for (inner = 0; inner < bC4_size_idx_0; inner++) {
            b_bC2_data[(((inner + bC2_size_idx_0) + bC2_size_idx_0) +
                        bC3_size_idx_0) + bC3_size_idx_0] = bC4_data[inner];
        }

        for (inner = 0; inner < bC4_size_idx_0; inner++) {
            b_bC2_data[((((inner + bC2_size_idx_0) + bC2_size_idx_0) +
                         bC3_size_idx_0) + bC3_size_idx_0) + bC4_size_idx_0] =
                bC4_data[inner];
        }

        inner = (i2 - i1) - 1;
        for (i2 = 0; i2 < inner; i2++) {
            b_data[(i1 + i2) + 1] = b_bC2_data[i2];
        }

        /*  */
    }

    emxFree_real_T(&b_R1);
    emxFree_real_T(&b_y);
    emxFree_real_T(&y);
    emxFree_real_T(&varargin_6);
    emxFree_real_T(&varargin_4);
    emxFree_real_T(&varargin_2);
    emxFree_real_T(&R3);
    emxFree_real_T(&R2);
    emxFree_real_T(&R1);
}

/*
 * File trailer for BuildConstrJerk_v4.c
 *
 * [EOF]
 */
