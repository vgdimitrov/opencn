/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: EvalLine.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "EvalLine.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"

/* Function Definitions */

/*
 * parametrization of a straight line between P0 and P1
 * Arguments    : const double CurvStruct_P0[3]
 *                const double CurvStruct_P1[3]
 *                const double u_vec_data[]
 *                const int u_vec_size[2]
 *                double r0D_data[]
 *                int r0D_size[2]
 *                double r1D_data[]
 *                int r1D_size[2]
 *                double r2D_data[]
 *                int r2D_size[2]
 *                double r3D_data[]
 *                int r3D_size[2]
 * Return Type  : void
 */
void EvalLine(const double CurvStruct_P0[3], const double CurvStruct_P1[3],
              const double u_vec_data[], const int u_vec_size[2], double
              r0D_data[], int r0D_size[2], double r1D_data[], int r1D_size[2],
              double r2D_data[], int r2D_size[2], double r3D_data[], int
              r3D_size[2])
{
    int loop_ub;
    int i;
    double a_idx_0;
    double a_idx_1;
    double a_idx_2;

    /*  */
    r0D_size[0] = 3;
    r0D_size[1] = u_vec_size[1];
    loop_ub = u_vec_size[1];
    for (i = 0; i < loop_ub; i++) {
        r0D_data[3 * i] = CurvStruct_P1[0] * u_vec_data[i] + CurvStruct_P0[0] *
            (1.0 - u_vec_data[i]);
    }

    loop_ub = u_vec_size[1];
    for (i = 0; i < loop_ub; i++) {
        r0D_data[3 * i + 1] = CurvStruct_P1[1] * u_vec_data[i] + CurvStruct_P0[1]
            * (1.0 - u_vec_data[i]);
    }

    loop_ub = u_vec_size[1];
    for (i = 0; i < loop_ub; i++) {
        r0D_data[3 * i + 2] = CurvStruct_P1[2] * u_vec_data[i] + CurvStruct_P0[2]
            * (1.0 - u_vec_data[i]);
    }

    /*  */
    a_idx_0 = CurvStruct_P1[0] - CurvStruct_P0[0];
    a_idx_1 = CurvStruct_P1[1] - CurvStruct_P0[1];
    a_idx_2 = CurvStruct_P1[2] - CurvStruct_P0[2];
    r1D_size[0] = 3;
    r1D_size[1] = (unsigned char)u_vec_size[1];
    if ((unsigned char)u_vec_size[1] != 0) {
        i = u_vec_size[1] - 1;
        for (loop_ub = 0; loop_ub <= i; loop_ub++) {
            r1D_data[3 * loop_ub] = a_idx_0;
            r1D_data[3 * loop_ub + 1] = a_idx_1;
            r1D_data[3 * loop_ub + 2] = a_idx_2;
        }
    }

    /*  */
    r2D_size[0] = 3;
    r2D_size[1] = u_vec_size[1];
    loop_ub = u_vec_size[1];
    for (i = 0; i < loop_ub; i++) {
        r2D_data[3 * i] = 0.0;
        r2D_data[3 * i + 1] = 0.0;
        r2D_data[3 * i + 2] = 0.0;
    }

    r3D_size[0] = 3;
    r3D_size[1] = u_vec_size[1];
    loop_ub = u_vec_size[1];
    for (i = 0; i < loop_ub; i++) {
        r3D_data[3 * i] = 0.0;
        r3D_data[3 * i + 1] = 0.0;
        r3D_data[3 * i + 2] = 0.0;
    }
}

/*
 * File trailer for EvalLine.c
 *
 * [EOF]
 */
