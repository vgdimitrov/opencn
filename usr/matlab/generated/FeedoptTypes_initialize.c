/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: FeedoptTypes_initialize.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "FeedoptTypes_initialize.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_data.h"
#include "FeedratePlanning_v4.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"
#include "timeKeeper.h"

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void FeedoptTypes_initialize(void)
{
    int i;
    savedTime_not_empty_init();
    g_FeedoptConfig.NDiscr = 20;
    g_FeedoptConfig.NBreak = 10;
    g_FeedoptConfig.NHorz = 3;
    g_FeedoptConfig.MaxNHorz = 10;
    g_FeedoptConfig.MaxNDiscr = 200;
    g_FeedoptConfig.MaxNCoeff = 120;
    g_FeedoptConfig.vmax = 15.0;
    g_FeedoptConfig.amax[0] = 20000.0;
    g_FeedoptConfig.jmax[0] = 1.5E+6;
    g_FeedoptConfig.amax[1] = 20000.0;
    g_FeedoptConfig.jmax[1] = 1.5E+6;
    g_FeedoptConfig.amax[2] = 20000.0;
    g_FeedoptConfig.jmax[2] = 1.5E+6;
    g_FeedoptConfig.SplineDegree = 4;
    g_FeedoptConfig.CutOff = 0.1;
    g_FeedoptConfig.LSplit = 2.0;
    g_FeedoptConfig.v_0 = 0.1;
    g_FeedoptConfig.at_0 = 0.0;
    g_FeedoptConfig.v_1 = 0.1;
    g_FeedoptConfig.at_1 = 0.0;
    for (i = 0; i < 1024; i++) {
        g_FeedoptConfig.source[i] = ' ';
    }

    g_FeedoptConfig.DebugPrint = false;
    Bl_not_empty_init();
    FeedoptPlan_init();
    FeedratePlanning_v4_init();
    isInitialized_FeedoptTypes = true;
}

/*
 * File trailer for FeedoptTypes_initialize.c
 *
 * [EOF]
 */
