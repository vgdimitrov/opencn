/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: sum.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "sum.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"

/* Function Definitions */

/*
 * Arguments    : const double x_data[]
 *                const int x_size[2]
 *                double y_data[]
 *                int y_size[2]
 * Return Type  : void
 */
void sum(const double x_data[], const int x_size[2], double y_data[], int
         y_size[2])
{
    int i;
    int k;
    int y_tmp;
    if (x_size[1] == 0) {
        y_size[0] = 1;
        y_size[1] = 0;
    } else {
        y_size[0] = 1;
        y_size[1] = (unsigned char)x_size[1];
        i = x_size[1];
        for (k = 0; k < i; k++) {
            y_data[k] = x_data[3 * k];
            y_tmp = (unsigned char)(k + 1) - 1;
            y_data[y_tmp] += x_data[3 * k + 1];
            y_data[y_tmp] += x_data[3 * k + 2];
        }
    }
}

/*
 * File trailer for sum.c
 *
 * [EOF]
 */
