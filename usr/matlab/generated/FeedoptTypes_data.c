/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: FeedoptTypes_data.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "FeedoptTypes_data.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"

/* Variable Definitions */
FeedoptConfigStruct g_FeedoptConfig;
const char cv[30] = { 'U', 'n', 'k', 'n', 'o', 'w', 'n', ' ', 'C', 'u', 'r', 'v',
    'e', ' ', 'T', 'y', 'p', 'e', ' ', 'f', 'o', 'r', ' ', 'E', 'v', 'a', 'l',
    '.', '\\', 'n' };

const char cv1[16] = { 'e', ' ', 'c', 'r', 'o', 's', 's', ' ', 'P', '0', 'P',
    '1', ' ', '=', ' ', '0' };

const char cv2[13] = { 'e', '\'', ' ', '*', ' ', 'P', '0', 'P', '1', ' ', '=',
    ' ', '0' };

const char cv3[29] = { 'B', 'A', 'D', ' ', 'C', 'U', 'R', 'V', 'E', ' ', 'T',
    'Y', 'P', 'E', ' ', 'I', 'N', ' ', 'L', 'E', 'N', 'G', 'T', 'H', ' ', 'C',
    'U', 'R', 'V' };

boolean_T isInitialized_FeedoptTypes = false;

/*
 * File trailer for FeedoptTypes_data.c
 *
 * [EOF]
 */
