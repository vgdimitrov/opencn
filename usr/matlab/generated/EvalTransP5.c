/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: EvalTransP5.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "EvalTransP5.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"

/* Function Definitions */

/*
 * Arguments    : double CurvStruct_CoeffP5[6][3]
 *                const double u_vec_data[]
 *                const int u_vec_size[2]
 *                double r_0D_data[]
 *                int r_0D_size[2]
 *                double r_1D_data[]
 *                int r_1D_size[2]
 *                double r_2D_data[]
 *                int r_2D_size[2]
 *                double r_3D_data[]
 *                int r_3D_size[2]
 * Return Type  : void
 */
void EvalTransP5(double CurvStruct_CoeffP5[6][3], const double u_vec_data[],
                 const int u_vec_size[2], double r_0D_data[], int r_0D_size[2],
                 double r_1D_data[], int r_1D_size[2], double r_2D_data[], int
                 r_2D_size[2], double r_3D_data[], int r_3D_size[2])
{
    int k;
    int na;
    double p5_1D[5][3];
    double p5_2D[4][3];
    double b[3][3];
    unsigned char outsize_idx_1;
    unsigned char b_outsize_idx_1;
    int b_size_idx_1;
    int i;
    double b_data[600];
    double b_b_data[600];

    /* MYPOLYDER Differentiate polynomial. */
    /*  */
    /* u  = u(:).';  */
    for (k = 0; k < 5; k++) {
        na = 5 - k;
        p5_1D[k][0] = CurvStruct_CoeffP5[k][0] * (double)na;
        p5_1D[k][1] = CurvStruct_CoeffP5[k][1] * (double)na;
        p5_1D[k][2] = CurvStruct_CoeffP5[k][2] * (double)na;
    }

    /* MYPOLYDER Differentiate polynomial. */
    /*  */
    /* u  = u(:).';  */
    for (k = 0; k < 4; k++) {
        na = 4 - k;
        p5_2D[k][0] = p5_1D[k][0] * (double)na;
        p5_2D[k][1] = p5_1D[k][1] * (double)na;
        p5_2D[k][2] = p5_1D[k][2] * (double)na;
    }

    /* MYPOLYDER Differentiate polynomial. */
    /*  */
    /* u  = u(:).';  */
    for (k = 0; k < 3; k++) {
        na = 3 - k;
        b[k][0] = na;
        b[k][1] = na;
        b[k][2] = na;
    }

    /*  */
    /* POLYVAL Evaluate array of polynomials with same degree. */
    /*  */
    /*  */
    /*  Use Horner's method for general case where X is an array. */
    if ((unsigned char)u_vec_size[1] != 0) {
        k = u_vec_size[1] - 1;
        for (na = 0; na <= k; na++) {
            r_0D_data[3 * na] = CurvStruct_CoeffP5[0][0];
            r_0D_data[3 * na + 1] = CurvStruct_CoeffP5[0][1];
            r_0D_data[3 * na + 2] = CurvStruct_CoeffP5[0][2];
        }
    }

    outsize_idx_1 = (unsigned char)u_vec_size[1];
    b_outsize_idx_1 = (unsigned char)u_vec_size[1];
    b_size_idx_1 = outsize_idx_1;
    if (outsize_idx_1 != 0) {
        na = u_vec_size[1];
        for (k = 0; k < na; k++) {
            b_data[3 * k] = u_vec_data[k];
            b_data[3 * k + 1] = u_vec_data[k];
            b_data[3 * k + 2] = u_vec_data[k];
        }
    }

    r_0D_size[0] = 3;
    r_0D_size[1] = outsize_idx_1;
    for (i = 0; i < 5; i++) {
        if (b_outsize_idx_1 != 0) {
            k = u_vec_size[1] - 1;
            for (na = 0; na <= k; na++) {
                b_b_data[3 * na] = CurvStruct_CoeffP5[i + 1][0];
                b_b_data[3 * na + 1] = CurvStruct_CoeffP5[i + 1][1];
                b_b_data[3 * na + 2] = CurvStruct_CoeffP5[i + 1][2];
            }
        }

        for (k = 0; k < b_size_idx_1; k++) {
            r_0D_data[3 * k] = b_data[3 * k] * r_0D_data[3 * k] + b_b_data[3 * k];
            na = 3 * k + 1;
            r_0D_data[na] = b_data[na] * r_0D_data[na] + b_b_data[na];
            na = 3 * k + 2;
            r_0D_data[na] = b_data[na] * r_0D_data[na] + b_b_data[na];
        }
    }

    /* POLYVAL Evaluate array of polynomials with same degree. */
    /*  */
    /*  */
    /*  Use Horner's method for general case where X is an array. */
    if ((unsigned char)u_vec_size[1] != 0) {
        k = u_vec_size[1] - 1;
        for (na = 0; na <= k; na++) {
            r_1D_data[3 * na] = p5_1D[0][0];
            r_1D_data[3 * na + 1] = p5_1D[0][1];
            r_1D_data[3 * na + 2] = p5_1D[0][2];
        }
    }

    outsize_idx_1 = (unsigned char)u_vec_size[1];
    b_outsize_idx_1 = (unsigned char)u_vec_size[1];
    b_size_idx_1 = outsize_idx_1;
    if (outsize_idx_1 != 0) {
        na = u_vec_size[1];
        for (k = 0; k < na; k++) {
            b_data[3 * k] = u_vec_data[k];
            b_data[3 * k + 1] = u_vec_data[k];
            b_data[3 * k + 2] = u_vec_data[k];
        }
    }

    r_1D_size[0] = 3;
    r_1D_size[1] = outsize_idx_1;
    for (i = 0; i < 4; i++) {
        if (b_outsize_idx_1 != 0) {
            k = u_vec_size[1] - 1;
            for (na = 0; na <= k; na++) {
                b_b_data[3 * na] = p5_1D[i + 1][0];
                b_b_data[3 * na + 1] = p5_1D[i + 1][1];
                b_b_data[3 * na + 2] = p5_1D[i + 1][2];
            }
        }

        for (k = 0; k < b_size_idx_1; k++) {
            r_1D_data[3 * k] = b_data[3 * k] * r_1D_data[3 * k] + b_b_data[3 * k];
            na = 3 * k + 1;
            r_1D_data[na] = b_data[na] * r_1D_data[na] + b_b_data[na];
            na = 3 * k + 2;
            r_1D_data[na] = b_data[na] * r_1D_data[na] + b_b_data[na];
        }
    }

    /* POLYVAL Evaluate array of polynomials with same degree. */
    /*  */
    /*  */
    /*  Use Horner's method for general case where X is an array. */
    if ((unsigned char)u_vec_size[1] != 0) {
        k = u_vec_size[1] - 1;
        for (na = 0; na <= k; na++) {
            r_2D_data[3 * na] = p5_2D[0][0];
            r_2D_data[3 * na + 1] = p5_2D[0][1];
            r_2D_data[3 * na + 2] = p5_2D[0][2];
        }
    }

    outsize_idx_1 = (unsigned char)u_vec_size[1];
    b_outsize_idx_1 = (unsigned char)u_vec_size[1];
    b_size_idx_1 = outsize_idx_1;
    if (outsize_idx_1 != 0) {
        na = u_vec_size[1];
        for (k = 0; k < na; k++) {
            b_data[3 * k] = u_vec_data[k];
            b_data[3 * k + 1] = u_vec_data[k];
            b_data[3 * k + 2] = u_vec_data[k];
        }
    }

    r_2D_size[0] = 3;
    r_2D_size[1] = outsize_idx_1;
    for (i = 0; i < 3; i++) {
        if (b_outsize_idx_1 != 0) {
            k = u_vec_size[1] - 1;
            for (na = 0; na <= k; na++) {
                b_b_data[3 * na] = p5_2D[i + 1][0];
                b_b_data[3 * na + 1] = p5_2D[i + 1][1];
                b_b_data[3 * na + 2] = p5_2D[i + 1][2];
            }
        }

        for (k = 0; k < b_size_idx_1; k++) {
            r_2D_data[3 * k] = b_data[3 * k] * r_2D_data[3 * k] + b_b_data[3 * k];
            na = 3 * k + 1;
            r_2D_data[na] = b_data[na] * r_2D_data[na] + b_b_data[na];
            na = 3 * k + 2;
            r_2D_data[na] = b_data[na] * r_2D_data[na] + b_b_data[na];
        }

        b[i][0] *= p5_2D[i][0];
        b[i][1] *= p5_2D[i][1];
        b[i][2] *= p5_2D[i][2];
    }

    /* POLYVAL Evaluate array of polynomials with same degree. */
    /*  */
    /*  */
    /*  Use Horner's method for general case where X is an array. */
    if ((unsigned char)u_vec_size[1] != 0) {
        k = u_vec_size[1] - 1;
        for (na = 0; na <= k; na++) {
            r_3D_data[3 * na] = b[0][0];
            r_3D_data[3 * na + 1] = b[0][1];
            r_3D_data[3 * na + 2] = b[0][2];
        }
    }

    outsize_idx_1 = (unsigned char)u_vec_size[1];
    b_outsize_idx_1 = (unsigned char)u_vec_size[1];
    b_size_idx_1 = outsize_idx_1;
    if (outsize_idx_1 != 0) {
        na = u_vec_size[1];
        for (k = 0; k < na; k++) {
            b_data[3 * k] = u_vec_data[k];
            b_data[3 * k + 1] = u_vec_data[k];
            b_data[3 * k + 2] = u_vec_data[k];
        }
    }

    r_3D_size[0] = 3;
    r_3D_size[1] = outsize_idx_1;
    for (i = 0; i < 2; i++) {
        if (b_outsize_idx_1 != 0) {
            k = u_vec_size[1] - 1;
            for (na = 0; na <= k; na++) {
                b_b_data[3 * na] = b[i + 1][0];
                b_b_data[3 * na + 1] = b[i + 1][1];
                b_b_data[3 * na + 2] = b[i + 1][2];
            }
        }

        for (k = 0; k < b_size_idx_1; k++) {
            r_3D_data[3 * k] = b_data[3 * k] * r_3D_data[3 * k] + b_b_data[3 * k];
            na = 3 * k + 1;
            r_3D_data[na] = b_data[na] * r_3D_data[na] + b_b_data[na];
            na = 3 * k + 2;
            r_3D_data[na] = b_data[na] * r_3D_data[na] + b_b_data[na];
        }
    }
}

/*
 * Arguments    : double CurvStruct_CoeffP5[6][3]
 *                double b_u_vec
 *                double r_0D[3]
 *                double r_1D[3]
 *                double r_2D[3]
 *                double r_3D[3]
 * Return Type  : void
 */
void b_EvalTransP5(double CurvStruct_CoeffP5[6][3], double b_u_vec, double r_0D
                   [3], double r_1D[3], double r_2D[3], double r_3D[3])
{
    int k;
    int p5_1D_tmp;
    double p5_1D[5][3];
    double p5_2D[4][3];
    double d;
    double d1;
    double p5_3D[3][3];
    double d2;

    /* MYPOLYDER Differentiate polynomial. */
    /*  */
    /* u  = u(:).';  */
    for (k = 0; k < 5; k++) {
        p5_1D_tmp = 5 - k;
        p5_1D[k][0] = CurvStruct_CoeffP5[k][0] * (double)p5_1D_tmp;
        p5_1D[k][1] = CurvStruct_CoeffP5[k][1] * (double)p5_1D_tmp;
        p5_1D[k][2] = CurvStruct_CoeffP5[k][2] * (double)p5_1D_tmp;
    }

    /* MYPOLYDER Differentiate polynomial. */
    /*  */
    /* u  = u(:).';  */
    for (k = 0; k < 4; k++) {
        p5_1D_tmp = 4 - k;
        p5_2D[k][0] = p5_1D[k][0] * (double)p5_1D_tmp;
        p5_2D[k][1] = p5_1D[k][1] * (double)p5_1D_tmp;
        p5_2D[k][2] = p5_1D[k][2] * (double)p5_1D_tmp;
    }

    /* MYPOLYDER Differentiate polynomial. */
    /*  */
    /* u  = u(:).';  */
    /*  */
    /* POLYVAL Evaluate array of polynomials with same degree. */
    /*  */
    /*  */
    /*  Use Horner's method for general case where X is an array. */
    for (k = 0; k < 3; k++) {
        p5_1D_tmp = 3 - k;
        p5_3D[k][0] = p5_2D[k][0] * (double)p5_1D_tmp;
        p5_3D[k][1] = p5_2D[k][1] * (double)p5_1D_tmp;
        p5_3D[k][2] = p5_2D[k][2] * (double)p5_1D_tmp;
        r_0D[k] = CurvStruct_CoeffP5[0][k];
    }

    d = r_0D[0];
    d1 = r_0D[1];
    d2 = r_0D[2];
    for (p5_1D_tmp = 0; p5_1D_tmp < 5; p5_1D_tmp++) {
        d = b_u_vec * d + CurvStruct_CoeffP5[p5_1D_tmp + 1][0];
        d1 = b_u_vec * d1 + CurvStruct_CoeffP5[p5_1D_tmp + 1][1];
        d2 = b_u_vec * d2 + CurvStruct_CoeffP5[p5_1D_tmp + 1][2];
    }

    r_0D[2] = d2;
    r_0D[1] = d1;
    r_0D[0] = d;

    /* POLYVAL Evaluate array of polynomials with same degree. */
    /*  */
    /*  */
    /*  Use Horner's method for general case where X is an array. */
    d = p5_1D[0][0];
    d1 = p5_1D[0][1];
    d2 = p5_1D[0][2];
    for (p5_1D_tmp = 0; p5_1D_tmp < 4; p5_1D_tmp++) {
        d = b_u_vec * d + p5_1D[p5_1D_tmp + 1][0];
        d1 = b_u_vec * d1 + p5_1D[p5_1D_tmp + 1][1];
        d2 = b_u_vec * d2 + p5_1D[p5_1D_tmp + 1][2];
    }

    r_1D[2] = d2;
    r_1D[1] = d1;
    r_1D[0] = d;

    /* POLYVAL Evaluate array of polynomials with same degree. */
    /*  */
    /*  */
    /*  Use Horner's method for general case where X is an array. */
    /* POLYVAL Evaluate array of polynomials with same degree. */
    /*  */
    /*  */
    /*  Use Horner's method for general case where X is an array. */
    d = p5_2D[0][0];
    d1 = p5_2D[0][1];
    d2 = p5_2D[0][2];
    for (p5_1D_tmp = 0; p5_1D_tmp < 3; p5_1D_tmp++) {
        d = b_u_vec * d + p5_2D[p5_1D_tmp + 1][0];
        d1 = b_u_vec * d1 + p5_2D[p5_1D_tmp + 1][1];
        d2 = b_u_vec * d2 + p5_2D[p5_1D_tmp + 1][2];
        r_3D[p5_1D_tmp] = p5_3D[0][p5_1D_tmp];
    }

    r_2D[2] = d2;
    r_2D[1] = d1;
    r_2D[0] = d;
    d = r_3D[0];
    d1 = r_3D[1];
    d2 = r_3D[2];
    for (p5_1D_tmp = 0; p5_1D_tmp < 2; p5_1D_tmp++) {
        d = b_u_vec * d + p5_3D[p5_1D_tmp + 1][0];
        d1 = b_u_vec * d1 + p5_3D[p5_1D_tmp + 1][1];
        d2 = b_u_vec * d2 + p5_3D[p5_1D_tmp + 1][2];
    }

    r_3D[2] = d2;
    r_3D[1] = d1;
    r_3D[0] = d;
}

/*
 * File trailer for EvalTransP5.c
 *
 * [EOF]
 */
