/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: CalcFrenet.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "CalcFrenet.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"
#include <math.h>

/* Function Definitions */

/*
 * Arguments    : const double rD1[3]
 *                const double rD2[3]
 *                double t[3]
 *                double n[3]
 *                double *kappa
 * Return Type  : void
 */
void CalcFrenet(const double rD1[3], const double rD2[3], double t[3], double n
                [3], double *kappa)
{
    double n_tmp;
    double b_idx_2;
    double b_idx_1;
    double b_idx_2_tmp;
    double b_n_tmp;
    double b_idx_0;

    /*  computes the local Frenet frame (t, n, b) of a curve in R^3 */
    /*  [t, n, b, kappa] = CalcFrenet(rD1, rD2) */
    /*  where rD1 is the first derivative and rD2 the second one */
    n_tmp = sqrt((pow(rD1[0], 2.0) + pow(rD1[1], 2.0)) + pow(rD1[2], 2.0));

    /*  tangential unit vector */
    /*  */
    t[0] = rD1[0] / n_tmp;
    t[1] = rD1[1] / n_tmp;
    t[2] = rD1[2] / n_tmp;
    if (fabs(fabs((t[0] * rD2[0] + t[1] * rD2[1]) + t[2] * rD2[2]) - sqrt((pow
            (rD2[0], 2.0) + pow(rD2[1], 2.0)) + pow(rD2[2], 2.0))) >
            2.2204460492503131E-16) {
        /*  regular case */
        b_idx_2 = rD1[1] * rD2[2] - rD1[2] * rD2[1];
        b_idx_1 = rD1[2] * rD2[0] - rD1[0] * rD2[2];
        b_idx_2_tmp = rD1[0] * rD2[1] - rD1[1] * rD2[0];
        b_n_tmp = sqrt((pow(b_idx_2, 2.0) + pow(b_idx_1, 2.0)) + pow(b_idx_2_tmp,
                        2.0));
        b_idx_0 = b_idx_2 / b_n_tmp;
        b_idx_1 /= b_n_tmp;
        b_idx_2 = b_idx_2_tmp / b_n_tmp;

        /*  binormal unit vector */
        n[0] = b_idx_1 * t[2] - b_idx_2 * t[1];
        n[1] = b_idx_2 * t[0] - b_idx_0 * t[2];
        n[2] = b_idx_0 * t[1] - b_idx_1 * t[0];

        /*  normal unit vector */
        *kappa = b_n_tmp / pow(n_tmp, 3.0);

        /*  curvature */
    } else {
        /*  special case if rD2 = 0 */
        n[0] = 0.0;
        n[1] = 0.0;
        n[2] = 0.0;

        /*  normal unit vector not defined */
        *kappa = 0.0;
    }
}

/*
 * File trailer for CalcFrenet.c
 *
 * [EOF]
 */
