/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: BuildConstr_v4.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "BuildConstr_v4.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_data.h"
#include "FeedoptTypes_emxutil.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "bsxfun.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "mtimes.h"
#include "norm.h"
#include "power.h"
#include "sinspace.h"
#include "sparse.h"
#include "sparse1.h"
#include "sum.h"
#include <math.h>
#include <stdio.h>
#include <string.h>

/* Function Definitions */

/*
 * Arguments    : const CurvStruct CurvStructs_data[]
 *                const int CurvStructs_size[2]
 *                const double amax[3]
 *                double b_v_0
 *                double b_at_0
 *                double b_v_1
 *                double b_at_1
 *                const emxArray_real_T *b_BasisVal
 *                const emxArray_real_T *b_BasisValD
 *                const double u_vec_data[]
 *                const int u_vec_size[2]
 *                coder_internal_sparse *A
 *                double b_data[]
 *                int b_size[1]
 *                double Aeq_data[]
 *                int Aeq_size[2]
 *                double beq_data[]
 *                int beq_size[1]
 * Return Type  : void
 */
void BuildConstr_v4(const CurvStruct CurvStructs_data[], const int
                    CurvStructs_size[2], const double amax[3], double b_v_0,
                    double b_at_0, double b_v_1, double b_at_1, const
                    emxArray_real_T *b_BasisVal, const emxArray_real_T
                    *b_BasisValD, const double u_vec_data[], const int
                    u_vec_size[2], coder_internal_sparse *A, double b_data[],
                    int b_size[1], double Aeq_data[], int Aeq_size[2], double
                    beq_data[], int beq_size[1])
{
    int N;
    int M;
    int CurvStructs_idx_0;
    int input_sizes_idx_0;
    int b_input_sizes_idx_0;
    int i;
    int i1;
    int b_u_vec_size[2];
    int loop_ub;
    double r1D_sqnorm_data[200];
    double unusedU0_data[600];
    int unusedU0_size[2];
    double r1D_data[600];
    int r1D_size[2];
    double r2D_data[600];
    int r2D_size[2];
    int b_r2D_size[1];
    emxArray_real_T *R1;
    double b_r2D_data[200];
    emxArray_real_T *varargin_3;
    emxArray_real_T *R2;
    emxArray_real_T *R3;
    emxArray_real_T *varargin_5;
    emxArray_real_T *varargin_7;
    boolean_T empty_non_axis_sizes;
    int c_input_sizes_idx_0;
    int d_input_sizes_idx_0;
    int e_input_sizes_idx_0;
    int f_input_sizes_idx_0;
    int sizes_idx_0;
    emxArray_real_T *y;
    double b;
    emxArray_real_T *b_y;
    emxArray_real_T *c_BasisVal;
    int g_input_sizes_idx_0;
    int i2;
    int i3;
    double x;
    int bC2_size_idx_0;
    int bC3_size_idx_0;
    double bC2_data[200];
    int bC4_size_idx_0;
    double bC3_data[200];
    double bC4_data[200];
    emxArray_real_T *a;
    double b_a;
    double c_a;
    double d_a;
    double d;
    double t_1[3];
    int b_loop_ub;
    cell_wrap_3 reshapes[3];
    int c_loop_ub;
    int k;
    int d_loop_ub;
    int d_BasisVal;
    int c_u_vec_size[2];
    int e_loop_ub;
    int f_loop_ub;
    int i4;
    int e_BasisVal;
    int c_BasisValD;
    double r1Dn_data[600];
    int r1Dn_size[2];
    double r2Dn_data[600];
    int r2Dn_size[2];
    int g_loop_ub;
    int i5;
    int f_BasisVal;
    int d_BasisValD;
    int i6;
    int g_BasisVal;
    int e_BasisValD;
    int i7;
    int t_1_tmp;
    int h_loop_ub;
    int b_t_1_tmp;
    double b_tmp;
    double a_tmp;
    double b_b_tmp;
    double x_data[1400];
    if (g_FeedoptConfig.DebugPrint) {
        printf("BuildConstr_v4 with Ncrv = %d, amax = [%f, %f, %f], v_0 = %f, at_0 = %f, v_1 = %f, at_1 = %f\n",
               CurvStructs_size[1], amax[0], amax[1], amax[2], b_v_0, b_at_0,
               b_v_1, b_at_1);
        fflush(stdout);
    }

    PROF_IN(BuildConstr_v4);
    N = b_BasisVal->size[1];
    M = b_BasisVal->size[0];

    /*  */
    sparse(7.0 * (double)b_BasisVal->size[0] * (double)CurvStructs_size[1],
           (double)b_BasisVal->size[1] * (double)CurvStructs_size[1], A->d,
           A->colidx, A->rowidx, &A->m, &A->n, &A->maxnz);

    /*  preallocation */
    CurvStructs_idx_0 = 7 * b_BasisVal->size[0] * CurvStructs_size[1];
    b_size[0] = CurvStructs_idx_0;
    if (0 <= CurvStructs_idx_0 - 1) {
        memset(&b_data[0], 0, CurvStructs_idx_0 * sizeof(double));
    }

    /*  preallocation */
    input_sizes_idx_0 = (CurvStructs_size[1] + 1) << 1;
    b_input_sizes_idx_0 = b_BasisVal->size[1] * CurvStructs_size[1];
    Aeq_size[0] = input_sizes_idx_0;
    Aeq_size[1] = b_input_sizes_idx_0;
    for (i = 0; i < b_input_sizes_idx_0; i++) {
        for (i1 = 0; i1 < input_sizes_idx_0; i1++) {
            Aeq_data[i1 + Aeq_size[0] * i] = 0.0;
        }
    }

    /*  preallocation */
    beq_size[0] = input_sizes_idx_0;
    if (0 <= input_sizes_idx_0 - 1) {
        memset(&beq_data[0], 0, input_sizes_idx_0 * sizeof(double));
    }

    /*  preallocation */
    /*  coder.varsize('BasisVal', [FeedoptLimits.MaxNDiscr, FeedoptLimits.MaxNCoeff], [1, 1]); */
    /*  coder.varsize('BasisValD', [FeedoptLimits.MaxNDiscr, FeedoptLimits.MaxNCoeff], [1, 1]); */
    /*  coder.varsize('A', [7*FeedoptLimits.MaxNDiscr*FeedoptLimits.MaxNHorz, FeedoptLimits.MaxNCoeff*FeedoptLimits.MaxNHorz], [1,1]); */
    /*  */
    b_u_vec_size[0] = 1;
    b_u_vec_size[1] = u_vec_size[1];
    loop_ub = u_vec_size[1] - 1;
    if (0 <= loop_ub) {
        memcpy(&r1D_sqnorm_data[0], &u_vec_data[0], (loop_ub + 1) * sizeof
               (double));
    }

    d_EvalCurvStruct(CurvStructs_data[0].Type, CurvStructs_data[0].P0,
                     CurvStructs_data[0].P1, CurvStructs_data[0].evec,
                     CurvStructs_data[0].theta, CurvStructs_data[0].pitch,
                     *(double (*)[6][3])&CurvStructs_data[0].CoeffP5[0][0],
                     r1D_sqnorm_data, b_u_vec_size, unusedU0_data, unusedU0_size,
                     r1D_data, r1D_size, r2D_data, r2D_size);
    power(r1D_data, r1D_size, unusedU0_data, b_u_vec_size);
    sum(unusedU0_data, b_u_vec_size, r1D_sqnorm_data, unusedU0_size);

    /*  squared norm */
    /*  */
    /*  unit tangent vector @ start */
    /*  */
    /*  */
    loop_ub = r2D_size[1];
    b_r2D_size[0] = r2D_size[1];
    for (i = 0; i < loop_ub; i++) {
        b_r2D_data[i] = r2D_data[3 * i];
    }

    emxInit_real_T(&R1, 2);
    bsxfun(b_r2D_data, b_r2D_size, b_BasisVal, R1);
    loop_ub = r1D_size[1];
    b_r2D_size[0] = r1D_size[1];
    for (i = 0; i < loop_ub; i++) {
        b_r2D_data[i] = r1D_data[3 * i];
    }

    emxInit_real_T(&varargin_3, 2);
    bsxfun(b_r2D_data, b_r2D_size, b_BasisValD, varargin_3);
    loop_ub = R1->size[1];
    for (i = 0; i < loop_ub; i++) {
        input_sizes_idx_0 = R1->size[0];
        for (i1 = 0; i1 < input_sizes_idx_0; i1++) {
            R1->data[i1 + R1->size[0] * i] += 0.5 * varargin_3->data[i1 +
                varargin_3->size[0] * i];
        }
    }

    loop_ub = r2D_size[1];
    b_r2D_size[0] = r2D_size[1];
    for (i = 0; i < loop_ub; i++) {
        b_r2D_data[i] = r2D_data[3 * i + 1];
    }

    emxInit_real_T(&R2, 2);
    bsxfun(b_r2D_data, b_r2D_size, b_BasisVal, R2);
    loop_ub = r1D_size[1];
    b_r2D_size[0] = r1D_size[1];
    for (i = 0; i < loop_ub; i++) {
        b_r2D_data[i] = r1D_data[3 * i + 1];
    }

    bsxfun(b_r2D_data, b_r2D_size, b_BasisValD, varargin_3);
    loop_ub = R2->size[1];
    for (i = 0; i < loop_ub; i++) {
        input_sizes_idx_0 = R2->size[0];
        for (i1 = 0; i1 < input_sizes_idx_0; i1++) {
            R2->data[i1 + R2->size[0] * i] += 0.5 * varargin_3->data[i1 +
                varargin_3->size[0] * i];
        }
    }

    loop_ub = r2D_size[1];
    b_r2D_size[0] = r2D_size[1];
    for (i = 0; i < loop_ub; i++) {
        b_r2D_data[i] = r2D_data[3 * i + 2];
    }

    emxInit_real_T(&R3, 2);
    bsxfun(b_r2D_data, b_r2D_size, b_BasisVal, R3);
    loop_ub = r1D_size[1];
    b_r2D_size[0] = r1D_size[1];
    for (i = 0; i < loop_ub; i++) {
        b_r2D_data[i] = r1D_data[3 * i + 2];
    }

    bsxfun(b_r2D_data, b_r2D_size, b_BasisValD, varargin_3);
    loop_ub = R3->size[1];
    for (i = 0; i < loop_ub; i++) {
        input_sizes_idx_0 = R3->size[0];
        for (i1 = 0; i1 < input_sizes_idx_0; i1++) {
            R3->data[i1 + R3->size[0] * i] += 0.5 * varargin_3->data[i1 +
                varargin_3->size[0] * i];
        }
    }

    /*  */
    i = varargin_3->size[0] * varargin_3->size[1];
    varargin_3->size[0] = R1->size[0];
    varargin_3->size[1] = R1->size[1];
    emxEnsureCapacity_real_T(varargin_3, i);
    loop_ub = R1->size[1];
    for (i = 0; i < loop_ub; i++) {
        input_sizes_idx_0 = R1->size[0];
        for (i1 = 0; i1 < input_sizes_idx_0; i1++) {
            varargin_3->data[i1 + varargin_3->size[0] * i] = -R1->data[i1 +
                R1->size[0] * i];
        }
    }

    emxInit_real_T(&varargin_5, 2);
    i = varargin_5->size[0] * varargin_5->size[1];
    varargin_5->size[0] = R2->size[0];
    varargin_5->size[1] = R2->size[1];
    emxEnsureCapacity_real_T(varargin_5, i);
    loop_ub = R2->size[1];
    for (i = 0; i < loop_ub; i++) {
        input_sizes_idx_0 = R2->size[0];
        for (i1 = 0; i1 < input_sizes_idx_0; i1++) {
            varargin_5->data[i1 + varargin_5->size[0] * i] = -R2->data[i1 +
                R2->size[0] * i];
        }
    }

    emxInit_real_T(&varargin_7, 2);
    i = varargin_7->size[0] * varargin_7->size[1];
    varargin_7->size[0] = R3->size[0];
    varargin_7->size[1] = R3->size[1];
    emxEnsureCapacity_real_T(varargin_7, i);
    loop_ub = R3->size[1];
    for (i = 0; i < loop_ub; i++) {
        input_sizes_idx_0 = R3->size[0];
        for (i1 = 0; i1 < input_sizes_idx_0; i1++) {
            varargin_7->data[i1 + varargin_7->size[0] * i] = -R3->data[i1 +
                R3->size[0] * i];
        }
    }

    if ((b_BasisVal->size[0] != 0) && (b_BasisVal->size[1] != 0)) {
        CurvStructs_idx_0 = b_BasisVal->size[1];
    } else if ((R1->size[0] != 0) && (R1->size[1] != 0)) {
        CurvStructs_idx_0 = R1->size[1];
    } else if ((varargin_3->size[0] != 0) && (varargin_3->size[1] != 0)) {
        CurvStructs_idx_0 = varargin_3->size[1];
    } else if ((R2->size[0] != 0) && (R2->size[1] != 0)) {
        CurvStructs_idx_0 = R2->size[1];
    } else if ((varargin_5->size[0] != 0) && (varargin_5->size[1] != 0)) {
        CurvStructs_idx_0 = varargin_5->size[1];
    } else if ((R3->size[0] != 0) && (R3->size[1] != 0)) {
        CurvStructs_idx_0 = R3->size[1];
    } else if ((varargin_7->size[0] != 0) && (varargin_7->size[1] != 0)) {
        CurvStructs_idx_0 = varargin_7->size[1];
    } else {
        CurvStructs_idx_0 = b_BasisVal->size[1];
        if (CurvStructs_idx_0 <= 0) {
            CurvStructs_idx_0 = 0;
        }

        if (R1->size[1] > CurvStructs_idx_0) {
            CurvStructs_idx_0 = R1->size[1];
        }

        if (varargin_3->size[1] > CurvStructs_idx_0) {
            CurvStructs_idx_0 = varargin_3->size[1];
        }

        if (R2->size[1] > CurvStructs_idx_0) {
            CurvStructs_idx_0 = R2->size[1];
        }

        if (varargin_5->size[1] > CurvStructs_idx_0) {
            CurvStructs_idx_0 = varargin_5->size[1];
        }

        if (R3->size[1] > CurvStructs_idx_0) {
            CurvStructs_idx_0 = R3->size[1];
        }

        if (varargin_7->size[1] > CurvStructs_idx_0) {
            CurvStructs_idx_0 = varargin_7->size[1];
        }
    }

    empty_non_axis_sizes = (CurvStructs_idx_0 == 0);
    if (empty_non_axis_sizes || ((b_BasisVal->size[0] != 0) && (b_BasisVal->
            size[1] != 0))) {
        c_input_sizes_idx_0 = b_BasisVal->size[0];
    } else {
        c_input_sizes_idx_0 = 0;
    }

    if (empty_non_axis_sizes || ((R1->size[0] != 0) && (R1->size[1] != 0))) {
        input_sizes_idx_0 = R1->size[0];
    } else {
        input_sizes_idx_0 = 0;
    }

    if (empty_non_axis_sizes || ((varargin_3->size[0] != 0) && (varargin_3->
            size[1] != 0))) {
        b_input_sizes_idx_0 = varargin_3->size[0];
    } else {
        b_input_sizes_idx_0 = 0;
    }

    if (empty_non_axis_sizes || ((R2->size[0] != 0) && (R2->size[1] != 0))) {
        d_input_sizes_idx_0 = R2->size[0];
    } else {
        d_input_sizes_idx_0 = 0;
    }

    if (empty_non_axis_sizes || ((varargin_5->size[0] != 0) && (varargin_5->
            size[1] != 0))) {
        e_input_sizes_idx_0 = varargin_5->size[0];
    } else {
        e_input_sizes_idx_0 = 0;
    }

    if (empty_non_axis_sizes || ((R3->size[0] != 0) && (R3->size[1] != 0))) {
        f_input_sizes_idx_0 = R3->size[0];
    } else {
        f_input_sizes_idx_0 = 0;
    }

    if (empty_non_axis_sizes || ((varargin_7->size[0] != 0) && (varargin_7->
            size[1] != 0))) {
        sizes_idx_0 = varargin_7->size[0];
    } else {
        sizes_idx_0 = 0;
    }

    emxInit_real_T(&y, 2);
    b = 7.0 * (double)b_BasisVal->size[0];
    if (b < 1.0) {
        y->size[0] = 1;
        y->size[1] = 0;
    } else {
        i = y->size[0] * y->size[1];
        y->size[0] = 1;
        loop_ub = (int)(b - 1.0);
        y->size[1] = loop_ub + 1;
        emxEnsureCapacity_real_T(y, i);
        for (i = 0; i <= loop_ub; i++) {
            y->data[i] = (double)i + 1.0;
        }
    }

    emxInit_real_T(&b_y, 2);
    if (b_BasisVal->size[1] < 1) {
        b_y->size[0] = 1;
        b_y->size[1] = 0;
    } else {
        i = b_y->size[0] * b_y->size[1];
        b_y->size[0] = 1;
        b_y->size[1] = b_BasisVal->size[1];
        emxEnsureCapacity_real_T(b_y, i);
        loop_ub = b_BasisVal->size[1] - 1;
        for (i = 0; i <= loop_ub; i++) {
            b_y->data[i] = (double)i + 1.0;
        }
    }

    emxInit_real_T(&c_BasisVal, 2);
    g_input_sizes_idx_0 = c_input_sizes_idx_0;
    c_input_sizes_idx_0 = input_sizes_idx_0;
    input_sizes_idx_0 = b_input_sizes_idx_0;
    b_input_sizes_idx_0 = d_input_sizes_idx_0;
    i = g_input_sizes_idx_0 + c_input_sizes_idx_0;
    i1 = i + input_sizes_idx_0;
    i2 = c_BasisVal->size[0] * c_BasisVal->size[1];
    c_BasisVal->size[0] = (((i1 + b_input_sizes_idx_0) + e_input_sizes_idx_0) +
                           f_input_sizes_idx_0) + sizes_idx_0;
    c_BasisVal->size[1] = CurvStructs_idx_0;
    emxEnsureCapacity_real_T(c_BasisVal, i2);
    for (i2 = 0; i2 < CurvStructs_idx_0; i2++) {
        for (i3 = 0; i3 < g_input_sizes_idx_0; i3++) {
            c_BasisVal->data[i3 + c_BasisVal->size[0] * i2] = b_BasisVal->
                data[i3 + g_input_sizes_idx_0 * i2];
        }
    }

    for (i2 = 0; i2 < CurvStructs_idx_0; i2++) {
        for (i3 = 0; i3 < c_input_sizes_idx_0; i3++) {
            c_BasisVal->data[(i3 + g_input_sizes_idx_0) + c_BasisVal->size[0] *
                i2] = R1->data[i3 + c_input_sizes_idx_0 * i2];
        }
    }

    for (i2 = 0; i2 < CurvStructs_idx_0; i2++) {
        for (i3 = 0; i3 < input_sizes_idx_0; i3++) {
            c_BasisVal->data[((i3 + g_input_sizes_idx_0) + c_input_sizes_idx_0)
                + c_BasisVal->size[0] * i2] = varargin_3->data[i3 +
                input_sizes_idx_0 * i2];
        }
    }

    for (i2 = 0; i2 < CurvStructs_idx_0; i2++) {
        for (i3 = 0; i3 < b_input_sizes_idx_0; i3++) {
            c_BasisVal->data[(((i3 + g_input_sizes_idx_0) + c_input_sizes_idx_0)
                              + input_sizes_idx_0) + c_BasisVal->size[0] * i2] =
                R2->data[i3 + b_input_sizes_idx_0 * i2];
        }
    }

    for (i2 = 0; i2 < CurvStructs_idx_0; i2++) {
        for (i3 = 0; i3 < e_input_sizes_idx_0; i3++) {
            c_BasisVal->data[((((i3 + g_input_sizes_idx_0) + c_input_sizes_idx_0)
                               + input_sizes_idx_0) + b_input_sizes_idx_0) +
                c_BasisVal->size[0] * i2] = varargin_5->data[i3 +
                e_input_sizes_idx_0 * i2];
        }
    }

    for (i2 = 0; i2 < CurvStructs_idx_0; i2++) {
        for (i3 = 0; i3 < f_input_sizes_idx_0; i3++) {
            c_BasisVal->data[((((i3 + i) + input_sizes_idx_0) +
                               b_input_sizes_idx_0) + e_input_sizes_idx_0) +
                c_BasisVal->size[0] * i2] = R3->data[i3 + f_input_sizes_idx_0 *
                i2];
        }
    }

    for (i = 0; i < CurvStructs_idx_0; i++) {
        for (i2 = 0; i2 < sizes_idx_0; i2++) {
            c_BasisVal->data[((((i2 + i1) + b_input_sizes_idx_0) +
                               e_input_sizes_idx_0) + f_input_sizes_idx_0) +
                c_BasisVal->size[0] * i] = varargin_7->data[i2 + sizes_idx_0 * i];
        }
    }

    sparse_parenAssign(A, c_BasisVal, y->data, y->size, b_y);

    /*  */
    x = pow(CurvStructs_data[0].FeedRate, 2.0);
    bC2_size_idx_0 = b_BasisVal->size[0];
    loop_ub = b_BasisVal->size[0];
    for (i = 0; i < loop_ub; i++) {
        bC2_data[i] = amax[0];
    }

    bC3_size_idx_0 = b_BasisVal->size[0];
    loop_ub = b_BasisVal->size[0];
    for (i = 0; i < loop_ub; i++) {
        bC3_data[i] = amax[1];
    }

    bC4_size_idx_0 = b_BasisVal->size[0];
    loop_ub = b_BasisVal->size[0];
    for (i = 0; i < loop_ub; i++) {
        bC4_data[i] = amax[2];
    }

    /*  */
    loop_ub = unusedU0_size[1];
    if (0 <= loop_ub - 1) {
        memcpy(&b_r2D_data[0], &r1D_sqnorm_data[0], loop_ub * sizeof(double));
    }

    b_input_sizes_idx_0 = unusedU0_size[1];
    loop_ub = unusedU0_size[1];
    for (i = 0; i < loop_ub; i++) {
        b_data[i] = x / b_r2D_data[i];
    }

    for (i = 0; i < bC2_size_idx_0; i++) {
        b_data[i + b_input_sizes_idx_0] = bC2_data[i];
    }

    for (i = 0; i < bC2_size_idx_0; i++) {
        b_data[(i + b_input_sizes_idx_0) + bC2_size_idx_0] = bC2_data[i];
    }

    for (i = 0; i < bC3_size_idx_0; i++) {
        b_data[((i + b_input_sizes_idx_0) + bC2_size_idx_0) + bC2_size_idx_0] =
            bC3_data[i];
    }

    for (i = 0; i < bC3_size_idx_0; i++) {
        b_data[(((i + b_input_sizes_idx_0) + bC2_size_idx_0) + bC2_size_idx_0) +
            bC3_size_idx_0] = bC3_data[i];
    }

    for (i = 0; i < bC4_size_idx_0; i++) {
        b_data[((((i + b_input_sizes_idx_0) + bC2_size_idx_0) + bC2_size_idx_0)
                + bC3_size_idx_0) + bC3_size_idx_0] = bC4_data[i];
    }

    for (i = 0; i < bC4_size_idx_0; i++) {
        b_data[(((((i + b_input_sizes_idx_0) + bC2_size_idx_0) + bC2_size_idx_0)
                 + bC3_size_idx_0) + bC3_size_idx_0) + bC4_size_idx_0] =
            bC4_data[i];
    }

    emxInit_real_T(&a, 2);

    /*  */
    b_a = 0.5 * r1D_data[0];
    c_a = 0.5 * r1D_data[1];
    d_a = 0.5 * r1D_data[2];
    d = b_norm(*(double (*)[3])&r1D_data[0]);
    t_1[0] = r1D_data[0] / d;
    t_1[1] = r1D_data[1] / d;
    t_1[2] = r1D_data[2] / d;
    loop_ub = b_BasisVal->size[1];
    input_sizes_idx_0 = b_BasisVal->size[1];
    CurvStructs_idx_0 = b_BasisVal->size[1];
    i = a->size[0] * a->size[1];
    a->size[0] = 3;
    a->size[1] = b_BasisVal->size[1];
    emxEnsureCapacity_real_T(a, i);
    for (i = 0; i < loop_ub; i++) {
        a->data[3 * i] = r2D_data[0] * b_BasisVal->data[b_BasisVal->size[0] * i]
            + b_a * b_BasisValD->data[b_BasisValD->size[0] * i];
    }

    for (i = 0; i < input_sizes_idx_0; i++) {
        a->data[3 * i + 1] = r2D_data[1] * b_BasisVal->data[b_BasisVal->size[0] *
            i] + c_a * b_BasisValD->data[b_BasisValD->size[0] * i];
    }

    for (i = 0; i < CurvStructs_idx_0; i++) {
        a->data[3 * i + 2] = r2D_data[2] * b_BasisVal->data[b_BasisVal->size[0] *
            i] + d_a * b_BasisValD->data[b_BasisValD->size[0] * i];
    }

    mtimes(t_1, a, y);
    loop_ub = b_BasisVal->size[1];
    for (i = 0; i < loop_ub; i++) {
        Aeq_data[Aeq_size[0] * i] = b_BasisVal->data[b_BasisVal->size[0] * i];
    }

    loop_ub = y->size[1];
    for (i = 0; i < loop_ub; i++) {
        Aeq_data[Aeq_size[0] * i + 1] = y->data[i];
    }

    beq_data[0] = pow(b_v_0, 2.0) / r1D_sqnorm_data[0];
    beq_data[1] = b_at_0;

    /*  */
    unusedU0_size[1] = b_BasisVal->size[0];
    loop_ub = b_BasisVal->size[0];
    if (0 <= loop_ub - 1) {
        memset(&r1D_sqnorm_data[0], 0, loop_ub * sizeof(double));
    }

    i = CurvStructs_size[1];
    if (0 <= CurvStructs_size[1] - 2) {
        b_loop_ub = bC2_size_idx_0;
        c_loop_ub = bC3_size_idx_0;
        d_loop_ub = bC4_size_idx_0;
        d_BasisVal = b_BasisVal->size[0];
        e_loop_ub = b_BasisVal->size[1];
        f_loop_ub = b_BasisVal->size[1];
        i4 = b_BasisVal->size[1];
        e_BasisVal = b_BasisVal->size[0];
        c_BasisValD = b_BasisValD->size[0];
        g_loop_ub = b_BasisVal->size[1];
        i5 = b_BasisVal->size[1];
        f_BasisVal = b_BasisVal->size[0];
        d_BasisValD = b_BasisValD->size[0];
        i6 = b_BasisVal->size[1];
        g_BasisVal = b_BasisVal->size[0];
        e_BasisValD = b_BasisValD->size[0];
        i7 = b_BasisVal->size[1];
        h_loop_ub = b_BasisVal->size[1];
    }

    emxInitMatrix_cell_wrap_3(reshapes);
    for (k = 0; k <= i - 2; k++) {
        c_u_vec_size[0] = 1;
        c_u_vec_size[1] = u_vec_size[1];
        loop_ub = u_vec_size[1] - 1;
        if (0 <= loop_ub) {
            memcpy(&r1D_sqnorm_data[0], &u_vec_data[0], (loop_ub + 1) * sizeof
                   (double));
        }

        d_EvalCurvStruct(CurvStructs_data[k + 1].Type, CurvStructs_data[k + 1].
                         P0, CurvStructs_data[k + 1].P1, CurvStructs_data[k + 1]
                         .evec, CurvStructs_data[k + 1].theta,
                         CurvStructs_data[k + 1].pitch, *(double (*)[6][3])&
                         CurvStructs_data[k + 1].CoeffP5[0][0], r1D_sqnorm_data,
                         c_u_vec_size, unusedU0_data, unusedU0_size, r1Dn_data,
                         r1Dn_size, r2Dn_data, r2Dn_size);
        power(r1Dn_data, r1Dn_size, unusedU0_data, b_u_vec_size);
        sum(unusedU0_data, b_u_vec_size, r1D_sqnorm_data, unusedU0_size);

        /*  squared norm */
        x = pow(CurvStructs_data[k + 1].FeedRate, 2.0);
        i1 = 3 * (r1D_size[1] - 1);
        d = b_norm(*(double (*)[3])&r1D_data[i1]);
        t_1[0] = r1D_data[i1] / d;
        t_1_tmp = 3 * (r1D_size[1] - 1) + 1;
        t_1[1] = r1D_data[t_1_tmp] / d;
        b_t_1_tmp = 3 * (r1D_size[1] - 1) + 2;
        t_1[2] = r1D_data[b_t_1_tmp] / d;

        /*  unit tangent vector @ end of previous piece */
        /*  */
        loop_ub = r2Dn_size[1];
        b_r2D_size[0] = r2Dn_size[1];
        for (i2 = 0; i2 < loop_ub; i2++) {
            b_r2D_data[i2] = r2Dn_data[3 * i2];
        }

        bsxfun(b_r2D_data, b_r2D_size, b_BasisVal, R1);
        loop_ub = r1Dn_size[1];
        b_r2D_size[0] = r1Dn_size[1];
        for (i2 = 0; i2 < loop_ub; i2++) {
            b_r2D_data[i2] = r1Dn_data[3 * i2];
        }

        bsxfun(b_r2D_data, b_r2D_size, b_BasisValD, varargin_3);
        loop_ub = R1->size[1];
        for (i2 = 0; i2 < loop_ub; i2++) {
            input_sizes_idx_0 = R1->size[0];
            for (i3 = 0; i3 < input_sizes_idx_0; i3++) {
                R1->data[i3 + R1->size[0] * i2] += 0.5 * varargin_3->data[i3 +
                    varargin_3->size[0] * i2];
            }
        }

        loop_ub = r2Dn_size[1];
        b_r2D_size[0] = r2Dn_size[1];
        for (i2 = 0; i2 < loop_ub; i2++) {
            b_r2D_data[i2] = r2Dn_data[3 * i2 + 1];
        }

        bsxfun(b_r2D_data, b_r2D_size, b_BasisVal, R2);
        loop_ub = r1Dn_size[1];
        b_r2D_size[0] = r1Dn_size[1];
        for (i2 = 0; i2 < loop_ub; i2++) {
            b_r2D_data[i2] = r1Dn_data[3 * i2 + 1];
        }

        bsxfun(b_r2D_data, b_r2D_size, b_BasisValD, varargin_3);
        loop_ub = R2->size[1];
        for (i2 = 0; i2 < loop_ub; i2++) {
            input_sizes_idx_0 = R2->size[0];
            for (i3 = 0; i3 < input_sizes_idx_0; i3++) {
                R2->data[i3 + R2->size[0] * i2] += 0.5 * varargin_3->data[i3 +
                    varargin_3->size[0] * i2];
            }
        }

        loop_ub = r2Dn_size[1];
        b_r2D_size[0] = r2Dn_size[1];
        for (i2 = 0; i2 < loop_ub; i2++) {
            b_r2D_data[i2] = r2Dn_data[3 * i2 + 2];
        }

        bsxfun(b_r2D_data, b_r2D_size, b_BasisVal, R3);
        loop_ub = r1Dn_size[1];
        b_r2D_size[0] = r1Dn_size[1];
        for (i2 = 0; i2 < loop_ub; i2++) {
            b_r2D_data[i2] = r1Dn_data[3 * i2 + 2];
        }

        bsxfun(b_r2D_data, b_r2D_size, b_BasisValD, varargin_3);
        loop_ub = R3->size[1];
        for (i2 = 0; i2 < loop_ub; i2++) {
            input_sizes_idx_0 = R3->size[0];
            for (i3 = 0; i3 < input_sizes_idx_0; i3++) {
                R3->data[i3 + R3->size[0] * i2] += 0.5 * varargin_3->data[i3 +
                    varargin_3->size[0] * i2];
            }
        }

        /*  */
        i2 = varargin_3->size[0] * varargin_3->size[1];
        varargin_3->size[0] = R1->size[0];
        varargin_3->size[1] = R1->size[1];
        emxEnsureCapacity_real_T(varargin_3, i2);
        loop_ub = R1->size[1];
        for (i2 = 0; i2 < loop_ub; i2++) {
            input_sizes_idx_0 = R1->size[0];
            for (i3 = 0; i3 < input_sizes_idx_0; i3++) {
                varargin_3->data[i3 + varargin_3->size[0] * i2] = -R1->data[i3 +
                    R1->size[0] * i2];
            }
        }

        i2 = varargin_5->size[0] * varargin_5->size[1];
        varargin_5->size[0] = R2->size[0];
        varargin_5->size[1] = R2->size[1];
        emxEnsureCapacity_real_T(varargin_5, i2);
        loop_ub = R2->size[1];
        for (i2 = 0; i2 < loop_ub; i2++) {
            input_sizes_idx_0 = R2->size[0];
            for (i3 = 0; i3 < input_sizes_idx_0; i3++) {
                varargin_5->data[i3 + varargin_5->size[0] * i2] = -R2->data[i3 +
                    R2->size[0] * i2];
            }
        }

        i2 = varargin_7->size[0] * varargin_7->size[1];
        varargin_7->size[0] = R3->size[0];
        varargin_7->size[1] = R3->size[1];
        emxEnsureCapacity_real_T(varargin_7, i2);
        loop_ub = R3->size[1];
        for (i2 = 0; i2 < loop_ub; i2++) {
            input_sizes_idx_0 = R3->size[0];
            for (i3 = 0; i3 < input_sizes_idx_0; i3++) {
                varargin_7->data[i3 + varargin_7->size[0] * i2] = -R3->data[i3 +
                    R3->size[0] * i2];
            }
        }

        if ((b_BasisVal->size[0] != 0) && (b_BasisVal->size[1] != 0)) {
            CurvStructs_idx_0 = b_BasisVal->size[1];
        } else if ((R1->size[0] != 0) && (R1->size[1] != 0)) {
            CurvStructs_idx_0 = R1->size[1];
        } else if ((varargin_3->size[0] != 0) && (varargin_3->size[1] != 0)) {
            CurvStructs_idx_0 = varargin_3->size[1];
        } else if ((R2->size[0] != 0) && (R2->size[1] != 0)) {
            CurvStructs_idx_0 = R2->size[1];
        } else if ((varargin_5->size[0] != 0) && (varargin_5->size[1] != 0)) {
            CurvStructs_idx_0 = varargin_5->size[1];
        } else if ((R3->size[0] != 0) && (R3->size[1] != 0)) {
            CurvStructs_idx_0 = R3->size[1];
        } else if ((varargin_7->size[0] != 0) && (varargin_7->size[1] != 0)) {
            CurvStructs_idx_0 = varargin_7->size[1];
        } else {
            CurvStructs_idx_0 = b_BasisVal->size[1];
            if (CurvStructs_idx_0 <= 0) {
                CurvStructs_idx_0 = 0;
            }

            if (R1->size[1] > CurvStructs_idx_0) {
                CurvStructs_idx_0 = R1->size[1];
            }

            if (varargin_3->size[1] > CurvStructs_idx_0) {
                CurvStructs_idx_0 = varargin_3->size[1];
            }

            if (R2->size[1] > CurvStructs_idx_0) {
                CurvStructs_idx_0 = R2->size[1];
            }

            if (varargin_5->size[1] > CurvStructs_idx_0) {
                CurvStructs_idx_0 = varargin_5->size[1];
            }

            if (R3->size[1] > CurvStructs_idx_0) {
                CurvStructs_idx_0 = R3->size[1];
            }

            if (varargin_7->size[1] > CurvStructs_idx_0) {
                CurvStructs_idx_0 = varargin_7->size[1];
            }
        }

        empty_non_axis_sizes = (CurvStructs_idx_0 == 0);
        if (empty_non_axis_sizes || ((b_BasisVal->size[0] != 0) &&
                (b_BasisVal->size[1] != 0))) {
            c_input_sizes_idx_0 = b_BasisVal->size[0];
        } else {
            c_input_sizes_idx_0 = 0;
        }

        if (empty_non_axis_sizes || ((R1->size[0] != 0) && (R1->size[1] != 0)))
        {
            input_sizes_idx_0 = R1->size[0];
        } else {
            input_sizes_idx_0 = 0;
        }

        if (empty_non_axis_sizes || ((varargin_3->size[0] != 0) &&
                (varargin_3->size[1] != 0))) {
            b_input_sizes_idx_0 = varargin_3->size[0];
        } else {
            b_input_sizes_idx_0 = 0;
        }

        if (empty_non_axis_sizes || ((R2->size[0] != 0) && (R2->size[1] != 0)))
        {
            d_input_sizes_idx_0 = R2->size[0];
        } else {
            d_input_sizes_idx_0 = 0;
        }

        if (empty_non_axis_sizes || ((varargin_5->size[0] != 0) &&
                (varargin_5->size[1] != 0))) {
            e_input_sizes_idx_0 = varargin_5->size[0];
        } else {
            e_input_sizes_idx_0 = 0;
        }

        if (empty_non_axis_sizes || ((R3->size[0] != 0) && (R3->size[1] != 0)))
        {
            f_input_sizes_idx_0 = R3->size[0];
        } else {
            f_input_sizes_idx_0 = 0;
        }

        if (empty_non_axis_sizes || ((varargin_7->size[0] != 0) &&
                (varargin_7->size[1] != 0))) {
            g_input_sizes_idx_0 = varargin_7->size[0];
        } else {
            g_input_sizes_idx_0 = 0;
        }

        b = ((double)k + 1.0) * 7.0 * (double)M + 1.0;
        b_tmp = (((double)k + 1.0) + 1.0) * 7.0 * (double)M;
        if (b_tmp < b) {
            y->size[0] = 1;
            y->size[1] = 0;
        } else {
            i2 = y->size[0] * y->size[1];
            y->size[0] = 1;
            loop_ub = (int)(b_tmp - b);
            y->size[1] = loop_ub + 1;
            emxEnsureCapacity_real_T(y, i2);
            for (i2 = 0; i2 <= loop_ub; i2++) {
                y->data[i2] = b + (double)i2;
            }
        }

        a_tmp = ((double)k + 1.0) * (double)N;
        b_b_tmp = (((double)k + 1.0) + 1.0) * (double)N;
        if (b_b_tmp < a_tmp + 1.0) {
            b_y->size[0] = 1;
            b_y->size[1] = 0;
        } else {
            i2 = b_y->size[0] * b_y->size[1];
            b_y->size[0] = 1;
            loop_ub = (int)(b_b_tmp - (a_tmp + 1.0));
            b_y->size[1] = loop_ub + 1;
            emxEnsureCapacity_real_T(b_y, i2);
            for (i2 = 0; i2 <= loop_ub; i2++) {
                b_y->data[i2] = (a_tmp + 1.0) + (double)i2;
            }
        }

        i2 = c_BasisVal->size[0] * c_BasisVal->size[1];
        c_BasisVal->size[0] = (((((c_input_sizes_idx_0 + input_sizes_idx_0) +
            b_input_sizes_idx_0) + d_input_sizes_idx_0) + e_input_sizes_idx_0) +
                               f_input_sizes_idx_0) + g_input_sizes_idx_0;
        c_BasisVal->size[1] = CurvStructs_idx_0;
        emxEnsureCapacity_real_T(c_BasisVal, i2);
        for (i2 = 0; i2 < CurvStructs_idx_0; i2++) {
            for (i3 = 0; i3 < c_input_sizes_idx_0; i3++) {
                c_BasisVal->data[i3 + c_BasisVal->size[0] * i2] =
                    b_BasisVal->data[i3 + c_input_sizes_idx_0 * i2];
            }
        }

        for (i2 = 0; i2 < CurvStructs_idx_0; i2++) {
            for (i3 = 0; i3 < input_sizes_idx_0; i3++) {
                c_BasisVal->data[(i3 + c_input_sizes_idx_0) + c_BasisVal->size[0]
                    * i2] = R1->data[i3 + input_sizes_idx_0 * i2];
            }
        }

        for (i2 = 0; i2 < CurvStructs_idx_0; i2++) {
            for (i3 = 0; i3 < b_input_sizes_idx_0; i3++) {
                c_BasisVal->data[((i3 + c_input_sizes_idx_0) + input_sizes_idx_0)
                    + c_BasisVal->size[0] * i2] = varargin_3->data[i3 +
                    b_input_sizes_idx_0 * i2];
            }
        }

        for (i2 = 0; i2 < CurvStructs_idx_0; i2++) {
            for (i3 = 0; i3 < d_input_sizes_idx_0; i3++) {
                c_BasisVal->data[(((i3 + c_input_sizes_idx_0) +
                                   input_sizes_idx_0) + b_input_sizes_idx_0) +
                    c_BasisVal->size[0] * i2] = R2->data[i3 +
                    d_input_sizes_idx_0 * i2];
            }
        }

        for (i2 = 0; i2 < CurvStructs_idx_0; i2++) {
            for (i3 = 0; i3 < e_input_sizes_idx_0; i3++) {
                c_BasisVal->data[((((i3 + c_input_sizes_idx_0) +
                                    input_sizes_idx_0) + b_input_sizes_idx_0) +
                                  d_input_sizes_idx_0) + c_BasisVal->size[0] *
                    i2] = varargin_5->data[i3 + e_input_sizes_idx_0 * i2];
            }
        }

        for (i2 = 0; i2 < CurvStructs_idx_0; i2++) {
            for (i3 = 0; i3 < f_input_sizes_idx_0; i3++) {
                c_BasisVal->data[(((((i3 + c_input_sizes_idx_0) +
                                     input_sizes_idx_0) + b_input_sizes_idx_0) +
                                   d_input_sizes_idx_0) + e_input_sizes_idx_0) +
                    c_BasisVal->size[0] * i2] = R3->data[i3 +
                    f_input_sizes_idx_0 * i2];
            }
        }

        for (i2 = 0; i2 < CurvStructs_idx_0; i2++) {
            for (i3 = 0; i3 < g_input_sizes_idx_0; i3++) {
                c_BasisVal->data[((((((i3 + c_input_sizes_idx_0) +
                                      input_sizes_idx_0) + b_input_sizes_idx_0)
                                    + d_input_sizes_idx_0) + e_input_sizes_idx_0)
                                  + f_input_sizes_idx_0) + c_BasisVal->size[0] *
                    i2] = varargin_7->data[i3 + g_input_sizes_idx_0 * i2];
            }
        }

        sparse_parenAssign(A, c_BasisVal, y->data, y->size, b_y);

        /*  */
        if (b > b_tmp) {
            i2 = -1;
            i3 = 0;
        } else {
            i2 = (int)b - 2;
            i3 = (int)b_tmp;
        }

        loop_ub = unusedU0_size[1];
        for (sizes_idx_0 = 0; sizes_idx_0 < loop_ub; sizes_idx_0++) {
            x_data[sizes_idx_0] = x / r1D_sqnorm_data[sizes_idx_0];
        }

        for (sizes_idx_0 = 0; sizes_idx_0 < b_loop_ub; sizes_idx_0++) {
            x_data[sizes_idx_0 + unusedU0_size[1]] = bC2_data[sizes_idx_0];
        }

        for (sizes_idx_0 = 0; sizes_idx_0 < b_loop_ub; sizes_idx_0++) {
            x_data[(sizes_idx_0 + unusedU0_size[1]) + bC2_size_idx_0] =
                bC2_data[sizes_idx_0];
        }

        for (sizes_idx_0 = 0; sizes_idx_0 < c_loop_ub; sizes_idx_0++) {
            x_data[((sizes_idx_0 + unusedU0_size[1]) + bC2_size_idx_0) +
                bC2_size_idx_0] = bC3_data[sizes_idx_0];
        }

        for (sizes_idx_0 = 0; sizes_idx_0 < c_loop_ub; sizes_idx_0++) {
            x_data[(((sizes_idx_0 + unusedU0_size[1]) + bC2_size_idx_0) +
                    bC2_size_idx_0) + bC3_size_idx_0] = bC3_data[sizes_idx_0];
        }

        for (sizes_idx_0 = 0; sizes_idx_0 < d_loop_ub; sizes_idx_0++) {
            x_data[((((sizes_idx_0 + unusedU0_size[1]) + bC2_size_idx_0) +
                     bC2_size_idx_0) + bC3_size_idx_0) + bC3_size_idx_0] =
                bC4_data[sizes_idx_0];
        }

        for (sizes_idx_0 = 0; sizes_idx_0 < d_loop_ub; sizes_idx_0++) {
            x_data[(((((sizes_idx_0 + unusedU0_size[1]) + bC2_size_idx_0) +
                      bC2_size_idx_0) + bC3_size_idx_0) + bC3_size_idx_0) +
                bC4_size_idx_0] = bC4_data[sizes_idx_0];
        }

        loop_ub = (i3 - i2) - 1;
        for (i3 = 0; i3 < loop_ub; i3++) {
            b_data[(i2 + i3) + 1] = x_data[i3];
        }

        /*  */
        d = (((double)k + 1.0) - 1.0) * (double)N + 1.0;
        if (d > a_tmp) {
            i2 = 1;
        } else {
            i2 = (int)d;
        }

        i3 = (k + 1) << 1;
        b_a = pow((t_1[0] * r1D_data[i1] + t_1[1] * r1D_data[t_1_tmp]) + t_1[2] *
                  r1D_data[b_t_1_tmp], 2.0);
        for (sizes_idx_0 = 0; sizes_idx_0 < e_loop_ub; sizes_idx_0++) {
            Aeq_data[i3 + Aeq_size[0] * ((i2 + sizes_idx_0) - 1)] = b_a *
                b_BasisVal->data[(d_BasisVal + b_BasisVal->size[0] * sizes_idx_0)
                - 1];
        }

        b = ((double)k + 1.0) * (double)N + 1.0;
        if (b > b_b_tmp) {
            i2 = 1;
        } else {
            i2 = (int)b;
        }

        b_a = -pow((t_1[0] * r1Dn_data[0] + t_1[1] * r1Dn_data[1]) + t_1[2] *
                   r1Dn_data[2], 2.0);
        for (sizes_idx_0 = 0; sizes_idx_0 < f_loop_ub; sizes_idx_0++) {
            Aeq_data[i3 + Aeq_size[0] * ((i2 + sizes_idx_0) - 1)] = b_a *
                b_BasisVal->data[b_BasisVal->size[0] * sizes_idx_0];
        }

        b_a = r2D_data[3 * (r2D_size[1] - 1)];
        c_a = 0.5 * r1D_data[i1];
        d_a = r2D_data[3 * (r2D_size[1] - 1) + 1];
        b = 0.5 * r1D_data[t_1_tmp];
        b_tmp = r2D_data[3 * (r2D_size[1] - 1) + 2];
        x = 0.5 * r1D_data[b_t_1_tmp];
        if (d > a_tmp) {
            i1 = 1;
        } else {
            i1 = (int)d;
        }

        i2 = i3 + 1;
        i3 = reshapes[0].f1->size[0] * reshapes[0].f1->size[1];
        reshapes[0].f1->size[0] = 1;
        reshapes[0].f1->size[1] = i4;
        emxEnsureCapacity_real_T(reshapes[0].f1, i3);
        i3 = reshapes[1].f1->size[0] * reshapes[1].f1->size[1];
        reshapes[1].f1->size[0] = 1;
        reshapes[1].f1->size[1] = i5;
        emxEnsureCapacity_real_T(reshapes[1].f1, i3);
        i3 = reshapes[2].f1->size[0] * reshapes[2].f1->size[1];
        reshapes[2].f1->size[0] = 1;
        reshapes[2].f1->size[1] = i6;
        emxEnsureCapacity_real_T(reshapes[2].f1, i3);
        for (i3 = 0; i3 < g_loop_ub; i3++) {
            reshapes[0].f1->data[i3] = b_a * b_BasisVal->data[(e_BasisVal +
                b_BasisVal->size[0] * i3) - 1] + c_a * b_BasisValD->data
                [(c_BasisValD + b_BasisValD->size[0] * i3) - 1];
            reshapes[1].f1->data[i3] = d_a * b_BasisVal->data[(f_BasisVal +
                b_BasisVal->size[0] * i3) - 1] + b * b_BasisValD->data
                [(d_BasisValD + b_BasisValD->size[0] * i3) - 1];
            reshapes[2].f1->data[i3] = b_tmp * b_BasisVal->data[(g_BasisVal +
                b_BasisVal->size[0] * i3) - 1] + x * b_BasisValD->data
                [(e_BasisValD + b_BasisValD->size[0] * i3) - 1];
        }

        i3 = a->size[0] * a->size[1];
        a->size[0] = 3;
        loop_ub = reshapes[0].f1->size[1];
        a->size[1] = reshapes[0].f1->size[1];
        emxEnsureCapacity_real_T(a, i3);
        for (i3 = 0; i3 < loop_ub; i3++) {
            a->data[3 * i3] = reshapes[0].f1->data[i3];
        }

        loop_ub = reshapes[1].f1->size[1];
        for (i3 = 0; i3 < loop_ub; i3++) {
            a->data[3 * i3 + 1] = reshapes[1].f1->data[i3];
        }

        loop_ub = reshapes[2].f1->size[1];
        for (i3 = 0; i3 < loop_ub; i3++) {
            a->data[3 * i3 + 2] = reshapes[2].f1->data[i3];
        }

        mtimes(t_1, a, y);
        loop_ub = y->size[1];
        for (i3 = 0; i3 < loop_ub; i3++) {
            Aeq_data[i2 + Aeq_size[0] * ((i1 + i3) - 1)] = y->data[i3];
        }

        /*  */
        b_a = 0.5 * r1Dn_data[0];
        c_a = 0.5 * r1Dn_data[1];
        d_a = 0.5 * r1Dn_data[2];
        d = ((double)k + 1.0) * (double)N + 1.0;
        if (d > b_b_tmp) {
            i1 = 1;
        } else {
            i1 = (int)d;
        }

        i3 = a->size[0] * a->size[1];
        a->size[0] = 3;
        a->size[1] = i7;
        emxEnsureCapacity_real_T(a, i3);
        for (i3 = 0; i3 < h_loop_ub; i3++) {
            d = b_BasisVal->data[b_BasisVal->size[0] * i3];
            b = b_BasisValD->data[b_BasisValD->size[0] * i3];
            a->data[3 * i3] = r2Dn_data[0] * d + b_a * b;
            a->data[3 * i3 + 1] = r2Dn_data[1] * d + c_a * b;
            a->data[3 * i3 + 2] = r2Dn_data[2] * d + d_a * b;
        }

        mtimes(t_1, a, y);
        loop_ub = y->size[1];
        for (i3 = 0; i3 < loop_ub; i3++) {
            Aeq_data[i2 + Aeq_size[0] * ((i1 + i3) - 1)] = -y->data[i3];
        }

        /*  */
        r1D_size[1] = r1Dn_size[1];
        loop_ub = r1Dn_size[1];
        for (i1 = 0; i1 < loop_ub; i1++) {
            r1D_data[3 * i1] = r1Dn_data[3 * i1];
            d_input_sizes_idx_0 = 3 * i1 + 1;
            r1D_data[d_input_sizes_idx_0] = r1Dn_data[d_input_sizes_idx_0];
            d_input_sizes_idx_0 = 3 * i1 + 2;
            r1D_data[d_input_sizes_idx_0] = r1Dn_data[d_input_sizes_idx_0];
        }

        loop_ub = r2Dn_size[1];
        r2D_size[1] = r2Dn_size[1];
        for (i1 = 0; i1 < loop_ub; i1++) {
            r2D_data[3 * i1] = r2Dn_data[3 * i1];
            sizes_idx_0 = 3 * i1 + 1;
            r2D_data[sizes_idx_0] = r2Dn_data[sizes_idx_0];
            sizes_idx_0 = 3 * i1 + 2;
            r2D_data[sizes_idx_0] = r2Dn_data[sizes_idx_0];
        }
    }

    emxFree_real_T(&c_BasisVal);
    emxFreeMatrix_cell_wrap_3(reshapes);
    emxFree_real_T(&b_y);
    emxFree_real_T(&varargin_7);
    emxFree_real_T(&varargin_5);
    emxFree_real_T(&varargin_3);
    emxFree_real_T(&R3);
    emxFree_real_T(&R2);
    emxFree_real_T(&R1);

    /*  */
    /*  unit tangent vector @ end of previous piece */
    /*  */
    d_input_sizes_idx_0 = 3 * (r2D_size[1] - 1);
    CurvStructs_idx_0 = 3 * (r1D_size[1] - 1);
    b_a = 0.5 * r1D_data[CurvStructs_idx_0];
    c_a = r2D_data[d_input_sizes_idx_0 + 1];
    input_sizes_idx_0 = CurvStructs_idx_0 + 1;
    d_a = 0.5 * r1D_data[input_sizes_idx_0];
    b = r2D_data[d_input_sizes_idx_0 + 2];
    b_input_sizes_idx_0 = CurvStructs_idx_0 + 2;
    b_tmp = 0.5 * r1D_data[b_input_sizes_idx_0];
    i = Aeq_size[1] - b_BasisVal->size[1];
    if (i + 1 > Aeq_size[1]) {
        i = 0;
    }

    c_input_sizes_idx_0 = Aeq_size[0] - 2;
    sizes_idx_0 = Aeq_size[0] - 1;
    d = b_norm(*(double (*)[3])&r1D_data[CurvStructs_idx_0]);
    t_1[0] = r1D_data[CurvStructs_idx_0] / d;
    t_1[1] = r1D_data[input_sizes_idx_0] / d;
    t_1[2] = r1D_data[b_input_sizes_idx_0] / d;
    loop_ub = b_BasisVal->size[1];
    input_sizes_idx_0 = b_BasisVal->size[1];
    CurvStructs_idx_0 = b_BasisVal->size[1];
    i1 = a->size[0] * a->size[1];
    a->size[0] = 3;
    a->size[1] = b_BasisVal->size[1];
    emxEnsureCapacity_real_T(a, i1);
    for (i1 = 0; i1 < loop_ub; i1++) {
        a->data[3 * i1] = r2D_data[d_input_sizes_idx_0] * b_BasisVal->data
            [(b_BasisVal->size[0] + b_BasisVal->size[0] * i1) - 1] + b_a *
            b_BasisValD->data[(b_BasisValD->size[0] + b_BasisValD->size[0] * i1)
            - 1];
    }

    for (i1 = 0; i1 < input_sizes_idx_0; i1++) {
        a->data[3 * i1 + 1] = c_a * b_BasisVal->data[(b_BasisVal->size[0] +
            b_BasisVal->size[0] * i1) - 1] + d_a * b_BasisValD->data
            [(b_BasisValD->size[0] + b_BasisValD->size[0] * i1) - 1];
    }

    for (i1 = 0; i1 < CurvStructs_idx_0; i1++) {
        a->data[3 * i1 + 2] = b * b_BasisVal->data[(b_BasisVal->size[0] +
            b_BasisVal->size[0] * i1) - 1] + b_tmp * b_BasisValD->data
            [(b_BasisValD->size[0] + b_BasisValD->size[0] * i1) - 1];
    }

    mtimes(t_1, a, y);
    loop_ub = b_BasisVal->size[1];
    emxFree_real_T(&a);
    for (i1 = 0; i1 < loop_ub; i1++) {
        Aeq_data[c_input_sizes_idx_0 + Aeq_size[0] * (i + i1)] =
            b_BasisVal->data[(b_BasisVal->size[0] + b_BasisVal->size[0] * i1) -
            1];
    }

    loop_ub = y->size[1];
    for (i1 = 0; i1 < loop_ub; i1++) {
        Aeq_data[sizes_idx_0 + Aeq_size[0] * (i + i1)] = y->data[i1];
    }

    emxFree_real_T(&y);

    /*  */
    beq_data[beq_size[0] - 2] = pow(b_v_1, 2.0) / r1D_sqnorm_data[unusedU0_size
        [1] - 1];
    beq_data[beq_size[0] - 1] = b_at_1;
    PROF_OUT(BuildConstr_v4);
}

/*
 * File trailer for BuildConstr_v4.c
 *
 * [EOF]
 */
