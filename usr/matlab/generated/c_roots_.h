/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: c_roots_.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

#ifndef C_ROOTS__H
#define C_ROOTS__H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Function Declarations */
extern void c_roots_(const double coeffs_data[], const int coeffs_size[2],
                     creal_T Y_data[], int Y_size[1]);

#endif

/*
 * File trailer for c_roots_.h
 *
 * [EOF]
 */
