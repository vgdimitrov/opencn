/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: EvalCurvStruct.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "EvalCurvStruct.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalHelix.h"
#include "EvalLine.h"
#include "EvalTransP5.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_data.h"
#include "FeedoptTypes_initialize.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"
#include <stdio.h>

/* Function Definitions */

/*
 * Arguments    : const CurvStruct *b_CurvStruct
 *                double u_vec_data[]
 *                const int u_vec_size[2]
 *                double r0D_data[]
 *                int r0D_size[2]
 *                double r1D_data[]
 *                int r1D_size[2]
 *                double r2D_data[]
 *                int r2D_size[2]
 *                double r3D_data[]
 *                int r3D_size[2]
 * Return Type  : void
 */
void EvalCurvStruct(const CurvStruct *b_CurvStruct, double u_vec_data[], const
                    int u_vec_size[2], double r0D_data[], int r0D_size[2],
                    double r1D_data[], int r1D_size[2], double r2D_data[], int
                    r2D_size[2], double r3D_data[], int r3D_size[2])
{
    int loop_ub;
    int i;
    boolean_T y;
    boolean_T x_data[200];
    boolean_T exitg1;
    char message[30];
    double a_idx_0;
    double a_idx_1;
    double a_idx_2;
    if (isInitialized_FeedoptTypes == false) {
        FeedoptTypes_initialize();
    }

    loop_ub = u_vec_size[1];
    for (i = 0; i < loop_ub; i++) {
        x_data[i] = (u_vec_data[i] > 1.0);
    }

    y = false;
    loop_ub = 0;
    exitg1 = false;
    while ((!exitg1) && (loop_ub <= u_vec_size[1] - 1)) {
        if (!x_data[loop_ub]) {
            loop_ub++;
        } else {
            y = true;
            exitg1 = true;
        }
    }

    if (y) {
        printf("EvalCurvStruct: u_vec > 1\n");
        fflush(stdout);
        loop_ub = u_vec_size[1];
        for (i = 0; i < loop_ub; i++) {
            if (u_vec_data[i] > 1.0) {
                u_vec_data[i] = 1.0;
            }
        }
    }

    loop_ub = u_vec_size[1];
    for (i = 0; i < loop_ub; i++) {
        x_data[i] = (u_vec_data[i] < 0.0);
    }

    y = false;
    loop_ub = 0;
    exitg1 = false;
    while ((!exitg1) && (loop_ub <= u_vec_size[1] - 1)) {
        if (!x_data[loop_ub]) {
            loop_ub++;
        } else {
            y = true;
            exitg1 = true;
        }
    }

    if (y) {
        printf("EvalCurvStruct: u_vec < 0\n");
        fflush(stdout);
        loop_ub = u_vec_size[1];
        for (i = 0; i < loop_ub; i++) {
            if (u_vec_data[i] < 0.0) {
                u_vec_data[i] = 0.0;
            }
        }
    }

    /*  */
    /*  */
    r0D_size[0] = 3;
    r0D_size[1] = u_vec_size[1];
    loop_ub = u_vec_size[1];
    for (i = 0; i < loop_ub; i++) {
        r0D_data[3 * i] = 0.0;
        r0D_data[3 * i + 1] = 0.0;
        r0D_data[3 * i + 2] = 0.0;
    }

    r1D_size[0] = 3;
    r1D_size[1] = u_vec_size[1];
    loop_ub = u_vec_size[1];
    for (i = 0; i < loop_ub; i++) {
        r1D_data[3 * i] = 0.0;
        r1D_data[3 * i + 1] = 0.0;
        r1D_data[3 * i + 2] = 0.0;
    }

    r2D_size[0] = 3;
    r2D_size[1] = u_vec_size[1];
    loop_ub = u_vec_size[1];
    for (i = 0; i < loop_ub; i++) {
        r2D_data[3 * i] = 0.0;
        r2D_data[3 * i + 1] = 0.0;
        r2D_data[3 * i + 2] = 0.0;
    }

    r3D_size[0] = 3;
    r3D_size[1] = u_vec_size[1];
    loop_ub = u_vec_size[1];
    for (i = 0; i < loop_ub; i++) {
        r3D_data[3 * i] = 0.0;
        r3D_data[3 * i + 1] = 0.0;
        r3D_data[3 * i + 2] = 0.0;
    }

    switch (b_CurvStruct->Type) {
      case CurveType_Line:
        /*  line (G01) */
        /*  */
        /*  parametrization of a straight line between P0 and P1 */
        /*  */
        r0D_size[0] = 3;
        r0D_size[1] = u_vec_size[1];
        loop_ub = u_vec_size[1];
        for (i = 0; i < loop_ub; i++) {
            r0D_data[3 * i] = b_CurvStruct->P1[0] * u_vec_data[i] +
                b_CurvStruct->P0[0] * (1.0 - u_vec_data[i]);
        }

        loop_ub = u_vec_size[1];
        for (i = 0; i < loop_ub; i++) {
            r0D_data[3 * i + 1] = b_CurvStruct->P1[1] * u_vec_data[i] +
                b_CurvStruct->P0[1] * (1.0 - u_vec_data[i]);
        }

        loop_ub = u_vec_size[1];
        for (i = 0; i < loop_ub; i++) {
            r0D_data[3 * i + 2] = b_CurvStruct->P1[2] * u_vec_data[i] +
                b_CurvStruct->P0[2] * (1.0 - u_vec_data[i]);
        }

        /*  */
        a_idx_0 = b_CurvStruct->P1[0] - b_CurvStruct->P0[0];
        a_idx_1 = b_CurvStruct->P1[1] - b_CurvStruct->P0[1];
        a_idx_2 = b_CurvStruct->P1[2] - b_CurvStruct->P0[2];
        r1D_size[0] = 3;
        r1D_size[1] = (unsigned char)u_vec_size[1];
        if ((unsigned char)u_vec_size[1] != 0) {
            i = u_vec_size[1] - 1;
            for (loop_ub = 0; loop_ub <= i; loop_ub++) {
                r1D_data[3 * loop_ub] = a_idx_0;
                r1D_data[3 * loop_ub + 1] = a_idx_1;
                r1D_data[3 * loop_ub + 2] = a_idx_2;
            }
        }

        /*  */
        break;

      case CurveType_Helix:
        /*  arc of circle / helix (G02, G03) */
        EvalHelix(b_CurvStruct->P0, b_CurvStruct->P1, b_CurvStruct->evec,
                  b_CurvStruct->theta, b_CurvStruct->pitch, u_vec_data,
                  u_vec_size, r0D_data, r0D_size, r1D_data, r1D_size, r2D_data,
                  r2D_size, r3D_data, r3D_size);
        break;

      case CurveType_TransP5:
        /*  polynomial transition */
        EvalTransP5(*(double (*)[6][3])&b_CurvStruct->CoeffP5[0][0], u_vec_data,
                    u_vec_size, r0D_data, r0D_size, r1D_data, r1D_size, r2D_data,
                    r2D_size, r3D_data, r3D_size);
        break;

      default:
        for (i = 0; i < 30; i++) {
            message[i] = cv[i];
        }

        c_assert_(&message[0]);
        break;
    }
}

/*
 * Arguments    : CurveType CurvStruct_Type
 *                const double CurvStruct_P0[3]
 *                const double CurvStruct_P1[3]
 *                const double CurvStruct_evec[3]
 *                double CurvStruct_theta
 *                double CurvStruct_pitch
 *                double CurvStruct_CoeffP5[6][3]
 *                double r0D[3]
 *                double r1D[3]
 * Return Type  : void
 */
void b_EvalCurvStruct(CurveType CurvStruct_Type, const double CurvStruct_P0[3],
                      const double CurvStruct_P1[3], const double
                      CurvStruct_evec[3], double CurvStruct_theta, double
                      CurvStruct_pitch, double CurvStruct_CoeffP5[6][3], double
                      r0D[3], double r1D[3])
{
    int i;
    double a[3];
    double r3D[3];
    char message[30];

    /*  */
    /*  */
    r0D[0] = 0.0;
    r1D[0] = 0.0;
    r0D[1] = 0.0;
    r1D[1] = 0.0;
    r0D[2] = 0.0;
    r1D[2] = 0.0;
    switch (CurvStruct_Type) {
      case CurveType_Line:
        /*  line (G01) */
        /*  */
        /*  parametrization of a straight line between P0 and P1 */
        /*  */
        r0D[0] = CurvStruct_P1[0];
        r0D[1] = CurvStruct_P1[1];
        r0D[2] = CurvStruct_P1[2];

        /*  */
        r1D[0] = CurvStruct_P1[0] - CurvStruct_P0[0];
        r1D[1] = CurvStruct_P1[1] - CurvStruct_P0[1];
        r1D[2] = CurvStruct_P1[2] - CurvStruct_P0[2];

        /*  */
        break;

      case CurveType_Helix:
        /*  arc of circle / helix (G02, G03) */
        b_EvalHelix(CurvStruct_P0, CurvStruct_P1, CurvStruct_evec,
                    CurvStruct_theta, CurvStruct_pitch, 1.0, r0D, r1D, a, r3D);
        break;

      case CurveType_TransP5:
        /*  polynomial transition */
        b_EvalTransP5(CurvStruct_CoeffP5, 1.0, r0D, r1D, a, r3D);
        break;

      default:
        for (i = 0; i < 30; i++) {
            message[i] = cv[i];
        }

        c_assert_(&message[0]);
        break;
    }
}

/*
 * Arguments    : CurveType CurvStruct_Type
 *                const double CurvStruct_P0[3]
 *                const double CurvStruct_P1[3]
 *                const double CurvStruct_evec[3]
 *                double CurvStruct_theta
 *                double CurvStruct_pitch
 *                double CurvStruct_CoeffP5[6][3]
 *                double r0D[3]
 *                double r1D[3]
 * Return Type  : void
 */
void c_EvalCurvStruct(CurveType CurvStruct_Type, const double CurvStruct_P0[3],
                      const double CurvStruct_P1[3], const double
                      CurvStruct_evec[3], double CurvStruct_theta, double
                      CurvStruct_pitch, double CurvStruct_CoeffP5[6][3], double
                      r0D[3], double r1D[3])
{
    int i;
    double a[3];
    double r3D[3];
    char message[30];

    /*  */
    /*  */
    r0D[0] = 0.0;
    r1D[0] = 0.0;
    r0D[1] = 0.0;
    r1D[1] = 0.0;
    r0D[2] = 0.0;
    r1D[2] = 0.0;
    switch (CurvStruct_Type) {
      case CurveType_Line:
        /*  line (G01) */
        /*  */
        /*  parametrization of a straight line between P0 and P1 */
        /*  */
        r0D[0] = CurvStruct_P0[0];
        r0D[1] = CurvStruct_P0[1];
        r0D[2] = CurvStruct_P0[2];

        /*  */
        r1D[0] = CurvStruct_P1[0] - CurvStruct_P0[0];
        r1D[1] = CurvStruct_P1[1] - CurvStruct_P0[1];
        r1D[2] = CurvStruct_P1[2] - CurvStruct_P0[2];

        /*  */
        break;

      case CurveType_Helix:
        /*  arc of circle / helix (G02, G03) */
        b_EvalHelix(CurvStruct_P0, CurvStruct_P1, CurvStruct_evec,
                    CurvStruct_theta, CurvStruct_pitch, 0.0, r0D, r1D, a, r3D);
        break;

      case CurveType_TransP5:
        /*  polynomial transition */
        b_EvalTransP5(CurvStruct_CoeffP5, 0.0, r0D, r1D, a, r3D);
        break;

      default:
        for (i = 0; i < 30; i++) {
            message[i] = cv[i];
        }

        c_assert_(&message[0]);
        break;
    }
}

/*
 * Arguments    : CurveType CurvStruct_Type
 *                const double CurvStruct_P0[3]
 *                const double CurvStruct_P1[3]
 *                const double CurvStruct_evec[3]
 *                double CurvStruct_theta
 *                double CurvStruct_pitch
 *                double CurvStruct_CoeffP5[6][3]
 *                double u_vec_data[]
 *                const int u_vec_size[2]
 *                double r0D_data[]
 *                int r0D_size[2]
 *                double r1D_data[]
 *                int r1D_size[2]
 *                double r2D_data[]
 *                int r2D_size[2]
 * Return Type  : void
 */
void d_EvalCurvStruct(CurveType CurvStruct_Type, const double CurvStruct_P0[3],
                      const double CurvStruct_P1[3], const double
                      CurvStruct_evec[3], double CurvStruct_theta, double
                      CurvStruct_pitch, double CurvStruct_CoeffP5[6][3], double
                      u_vec_data[], const int u_vec_size[2], double r0D_data[],
                      int r0D_size[2], double r1D_data[], int r1D_size[2],
                      double r2D_data[], int r2D_size[2])
{
    int loop_ub;
    int i;
    boolean_T y;
    boolean_T x_data[200];
    boolean_T exitg1;
    double r3D_data[600];
    int r3D_size[2];
    char message[30];
    loop_ub = u_vec_size[1];
    for (i = 0; i < loop_ub; i++) {
        x_data[i] = (u_vec_data[i] > 1.0);
    }

    y = false;
    loop_ub = 0;
    exitg1 = false;
    while ((!exitg1) && (loop_ub <= u_vec_size[1] - 1)) {
        if (!x_data[loop_ub]) {
            loop_ub++;
        } else {
            y = true;
            exitg1 = true;
        }
    }

    if (y) {
        printf("EvalCurvStruct: u_vec > 1\n");
        fflush(stdout);
        loop_ub = u_vec_size[1];
        for (i = 0; i < loop_ub; i++) {
            if (u_vec_data[i] > 1.0) {
                u_vec_data[i] = 1.0;
            }
        }
    }

    loop_ub = u_vec_size[1];
    for (i = 0; i < loop_ub; i++) {
        x_data[i] = (u_vec_data[i] < 0.0);
    }

    y = false;
    loop_ub = 0;
    exitg1 = false;
    while ((!exitg1) && (loop_ub <= u_vec_size[1] - 1)) {
        if (!x_data[loop_ub]) {
            loop_ub++;
        } else {
            y = true;
            exitg1 = true;
        }
    }

    if (y) {
        printf("EvalCurvStruct: u_vec < 0\n");
        fflush(stdout);
        loop_ub = u_vec_size[1];
        for (i = 0; i < loop_ub; i++) {
            if (u_vec_data[i] < 0.0) {
                u_vec_data[i] = 0.0;
            }
        }
    }

    /*  */
    /*  */
    r0D_size[0] = 3;
    r0D_size[1] = u_vec_size[1];
    loop_ub = u_vec_size[1];
    for (i = 0; i < loop_ub; i++) {
        r0D_data[3 * i] = 0.0;
        r0D_data[3 * i + 1] = 0.0;
        r0D_data[3 * i + 2] = 0.0;
    }

    r1D_size[0] = 3;
    r1D_size[1] = u_vec_size[1];
    loop_ub = u_vec_size[1];
    for (i = 0; i < loop_ub; i++) {
        r1D_data[3 * i] = 0.0;
        r1D_data[3 * i + 1] = 0.0;
        r1D_data[3 * i + 2] = 0.0;
    }

    r2D_size[0] = 3;
    r2D_size[1] = u_vec_size[1];
    loop_ub = u_vec_size[1];
    for (i = 0; i < loop_ub; i++) {
        r2D_data[3 * i] = 0.0;
        r2D_data[3 * i + 1] = 0.0;
        r2D_data[3 * i + 2] = 0.0;
    }

    switch (CurvStruct_Type) {
      case CurveType_Line:
        /*  line (G01) */
        EvalLine(CurvStruct_P0, CurvStruct_P1, u_vec_data, u_vec_size, r0D_data,
                 r0D_size, r1D_data, r1D_size, r2D_data, r2D_size, r3D_data,
                 r3D_size);
        break;

      case CurveType_Helix:
        /*  arc of circle / helix (G02, G03) */
        EvalHelix(CurvStruct_P0, CurvStruct_P1, CurvStruct_evec,
                  CurvStruct_theta, CurvStruct_pitch, u_vec_data, u_vec_size,
                  r0D_data, r0D_size, r1D_data, r1D_size, r2D_data, r2D_size,
                  r3D_data, r3D_size);
        break;

      case CurveType_TransP5:
        /*  polynomial transition */
        EvalTransP5(CurvStruct_CoeffP5, u_vec_data, u_vec_size, r0D_data,
                    r0D_size, r1D_data, r1D_size, r2D_data, r2D_size, r3D_data,
                    r3D_size);
        break;

      default:
        for (i = 0; i < 30; i++) {
            message[i] = cv[i];
        }

        c_assert_(&message[0]);
        break;
    }
}

/*
 * Arguments    : CurveType CurvStruct_Type
 *                const double CurvStruct_P0[3]
 *                const double CurvStruct_P1[3]
 *                const double CurvStruct_evec[3]
 *                double CurvStruct_theta
 *                double CurvStruct_pitch
 *                double CurvStruct_CoeffP5[6][3]
 *                double u_vec_data[]
 *                const int u_vec_size[2]
 *                double r0D_data[]
 *                int r0D_size[2]
 *                double r1D_data[]
 *                int r1D_size[2]
 *                double r2D_data[]
 *                int r2D_size[2]
 *                double r3D_data[]
 *                int r3D_size[2]
 * Return Type  : void
 */
void e_EvalCurvStruct(CurveType CurvStruct_Type, const double CurvStruct_P0[3],
                      const double CurvStruct_P1[3], const double
                      CurvStruct_evec[3], double CurvStruct_theta, double
                      CurvStruct_pitch, double CurvStruct_CoeffP5[6][3], double
                      u_vec_data[], const int u_vec_size[2], double r0D_data[],
                      int r0D_size[2], double r1D_data[], int r1D_size[2],
                      double r2D_data[], int r2D_size[2], double r3D_data[], int
                      r3D_size[2])
{
    int loop_ub;
    int i;
    boolean_T y;
    boolean_T x_data[200];
    boolean_T exitg1;
    char message[30];
    double a_idx_0;
    double a_idx_1;
    double a_idx_2;
    loop_ub = u_vec_size[1];
    for (i = 0; i < loop_ub; i++) {
        x_data[i] = (u_vec_data[i] > 1.0);
    }

    y = false;
    loop_ub = 0;
    exitg1 = false;
    while ((!exitg1) && (loop_ub <= u_vec_size[1] - 1)) {
        if (!x_data[loop_ub]) {
            loop_ub++;
        } else {
            y = true;
            exitg1 = true;
        }
    }

    if (y) {
        printf("EvalCurvStruct: u_vec > 1\n");
        fflush(stdout);
        loop_ub = u_vec_size[1];
        for (i = 0; i < loop_ub; i++) {
            if (u_vec_data[i] > 1.0) {
                u_vec_data[i] = 1.0;
            }
        }
    }

    loop_ub = u_vec_size[1];
    for (i = 0; i < loop_ub; i++) {
        x_data[i] = (u_vec_data[i] < 0.0);
    }

    y = false;
    loop_ub = 0;
    exitg1 = false;
    while ((!exitg1) && (loop_ub <= u_vec_size[1] - 1)) {
        if (!x_data[loop_ub]) {
            loop_ub++;
        } else {
            y = true;
            exitg1 = true;
        }
    }

    if (y) {
        printf("EvalCurvStruct: u_vec < 0\n");
        fflush(stdout);
        loop_ub = u_vec_size[1];
        for (i = 0; i < loop_ub; i++) {
            if (u_vec_data[i] < 0.0) {
                u_vec_data[i] = 0.0;
            }
        }
    }

    /*  */
    /*  */
    r0D_size[0] = 3;
    r0D_size[1] = u_vec_size[1];
    loop_ub = u_vec_size[1];
    for (i = 0; i < loop_ub; i++) {
        r0D_data[3 * i] = 0.0;
        r0D_data[3 * i + 1] = 0.0;
        r0D_data[3 * i + 2] = 0.0;
    }

    r1D_size[0] = 3;
    r1D_size[1] = u_vec_size[1];
    loop_ub = u_vec_size[1];
    for (i = 0; i < loop_ub; i++) {
        r1D_data[3 * i] = 0.0;
        r1D_data[3 * i + 1] = 0.0;
        r1D_data[3 * i + 2] = 0.0;
    }

    r2D_size[0] = 3;
    r2D_size[1] = u_vec_size[1];
    loop_ub = u_vec_size[1];
    for (i = 0; i < loop_ub; i++) {
        r2D_data[3 * i] = 0.0;
        r2D_data[3 * i + 1] = 0.0;
        r2D_data[3 * i + 2] = 0.0;
    }

    r3D_size[0] = 3;
    r3D_size[1] = u_vec_size[1];
    loop_ub = u_vec_size[1];
    for (i = 0; i < loop_ub; i++) {
        r3D_data[3 * i] = 0.0;
        r3D_data[3 * i + 1] = 0.0;
        r3D_data[3 * i + 2] = 0.0;
    }

    switch (CurvStruct_Type) {
      case CurveType_Line:
        /*  line (G01) */
        /*  */
        /*  parametrization of a straight line between P0 and P1 */
        /*  */
        r0D_size[0] = 3;
        r0D_size[1] = u_vec_size[1];
        loop_ub = u_vec_size[1];
        for (i = 0; i < loop_ub; i++) {
            r0D_data[3 * i] = CurvStruct_P1[0] * u_vec_data[i] + CurvStruct_P0[0]
                * (1.0 - u_vec_data[i]);
        }

        loop_ub = u_vec_size[1];
        for (i = 0; i < loop_ub; i++) {
            r0D_data[3 * i + 1] = CurvStruct_P1[1] * u_vec_data[i] +
                CurvStruct_P0[1] * (1.0 - u_vec_data[i]);
        }

        loop_ub = u_vec_size[1];
        for (i = 0; i < loop_ub; i++) {
            r0D_data[3 * i + 2] = CurvStruct_P1[2] * u_vec_data[i] +
                CurvStruct_P0[2] * (1.0 - u_vec_data[i]);
        }

        /*  */
        a_idx_0 = CurvStruct_P1[0] - CurvStruct_P0[0];
        a_idx_1 = CurvStruct_P1[1] - CurvStruct_P0[1];
        a_idx_2 = CurvStruct_P1[2] - CurvStruct_P0[2];
        r1D_size[0] = 3;
        r1D_size[1] = (unsigned char)u_vec_size[1];
        if ((unsigned char)u_vec_size[1] != 0) {
            i = u_vec_size[1] - 1;
            for (loop_ub = 0; loop_ub <= i; loop_ub++) {
                r1D_data[3 * loop_ub] = a_idx_0;
                r1D_data[3 * loop_ub + 1] = a_idx_1;
                r1D_data[3 * loop_ub + 2] = a_idx_2;
            }
        }

        /*  */
        break;

      case CurveType_Helix:
        /*  arc of circle / helix (G02, G03) */
        EvalHelix(CurvStruct_P0, CurvStruct_P1, CurvStruct_evec,
                  CurvStruct_theta, CurvStruct_pitch, u_vec_data, u_vec_size,
                  r0D_data, r0D_size, r1D_data, r1D_size, r2D_data, r2D_size,
                  r3D_data, r3D_size);
        break;

      case CurveType_TransP5:
        /*  polynomial transition */
        EvalTransP5(CurvStruct_CoeffP5, u_vec_data, u_vec_size, r0D_data,
                    r0D_size, r1D_data, r1D_size, r2D_data, r2D_size, r3D_data,
                    r3D_size);
        break;

      default:
        for (i = 0; i < 30; i++) {
            message[i] = cv[i];
        }

        c_assert_(&message[0]);
        break;
    }
}

/*
 * File trailer for EvalCurvStruct.c
 *
 * [EOF]
 */
