/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: vertcat.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

#ifndef VERTCAT_H
#define VERTCAT_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Function Declarations */
extern void b_sparse_vertcat(const coder_internal_sparse varargin_1, const
    coder_internal_sparse varargin_2, emxArray_real_T *c_d, emxArray_int32_T
    *c_colidx, emxArray_int32_T *c_rowidx, int *c_m, int *c_n);
extern void sparse_vertcat(const emxArray_real_T *varargin_1_d, const
    emxArray_int32_T *varargin_1_colidx, const emxArray_int32_T
    *varargin_1_rowidx, int varargin_1_m, int varargin_1_n, const double
    varargin_2_data[], const int varargin_2_size[2], emxArray_real_T *c_d,
    emxArray_int32_T *c_colidx, emxArray_int32_T *c_rowidx, int *c_m, int *c_n);

#endif

/*
 * File trailer for vertcat.h
 *
 * [EOF]
 */
