/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: bsxfun.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

/* Include Files */
#include "bsxfun.h"
#include "BenchmarkFeedratePlanning.h"
#include "CalcTransition.h"
#include "Calc_u_v4.h"
#include "ConstrHelixStruct.h"
#include "ConstrLineStruct.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptPlan.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_emxutil.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_alloc_matrix.h"
#include "c_linspace.h"
#include "c_roots_.h"
#include "sinspace.h"

/* Function Definitions */

/*
 * Arguments    : const emxArray_real_T *a
 *                const double b_data[]
 *                const int b_size[1]
 *                emxArray_real_T *c
 * Return Type  : void
 */
void b_bsxfun(const emxArray_real_T *a, const double b_data[], const int b_size
              [1], emxArray_real_T *c)
{
    int i;
    int acoef;
    int u1;
    int varargin_2;
    int b_acoef;
    int bcoef;
    int i1;
    int k;
    i = c->size[0] * c->size[1];
    acoef = b_size[0];
    u1 = a->size[0];
    if (acoef < u1) {
        u1 = acoef;
    }

    if (b_size[0] == 1) {
        c->size[0] = a->size[0];
    } else if (a->size[0] == 1) {
        c->size[0] = b_size[0];
    } else if (a->size[0] == b_size[0]) {
        c->size[0] = a->size[0];
    } else {
        c->size[0] = u1;
    }

    c->size[1] = a->size[1];
    emxEnsureCapacity_real_T(c, i);
    acoef = b_size[0];
    u1 = a->size[0];
    if (acoef < u1) {
        u1 = acoef;
    }

    if (b_size[0] == 1) {
        u1 = a->size[0];
    } else if (a->size[0] == 1) {
        u1 = b_size[0];
    } else {
        if (a->size[0] == b_size[0]) {
            u1 = a->size[0];
        }
    }

    if ((u1 != 0) && (a->size[1] != 0)) {
        acoef = (a->size[1] != 1);
        i = a->size[1] - 1;
        for (u1 = 0; u1 <= i; u1++) {
            varargin_2 = acoef * u1;
            b_acoef = (a->size[0] != 1);
            bcoef = (b_size[0] != 1);
            i1 = c->size[0] - 1;
            for (k = 0; k <= i1; k++) {
                c->data[k + c->size[0] * u1] = a->data[b_acoef * k + a->size[0] *
                    varargin_2] * b_data[bcoef * k];
            }
        }
    }
}

/*
 * Arguments    : const double a_data[]
 *                const int a_size[1]
 *                const emxArray_real_T *b
 *                emxArray_real_T *c
 * Return Type  : void
 */
void bsxfun(const double a_data[], const int a_size[1], const emxArray_real_T *b,
            emxArray_real_T *c)
{
    int i;
    int bcoef;
    int u1;
    int varargin_3;
    int acoef;
    int b_bcoef;
    int i1;
    int k;
    i = c->size[0] * c->size[1];
    bcoef = b->size[0];
    u1 = a_size[0];
    if (bcoef < u1) {
        u1 = bcoef;
    }

    if (b->size[0] == 1) {
        c->size[0] = a_size[0];
    } else if (a_size[0] == 1) {
        c->size[0] = b->size[0];
    } else if (a_size[0] == b->size[0]) {
        c->size[0] = a_size[0];
    } else {
        c->size[0] = u1;
    }

    c->size[1] = b->size[1];
    emxEnsureCapacity_real_T(c, i);
    bcoef = b->size[0];
    u1 = a_size[0];
    if (bcoef < u1) {
        u1 = bcoef;
    }

    if (b->size[0] == 1) {
        u1 = a_size[0];
    } else if (a_size[0] == 1) {
        u1 = b->size[0];
    } else {
        if (a_size[0] == b->size[0]) {
            u1 = a_size[0];
        }
    }

    if ((u1 != 0) && (b->size[1] != 0)) {
        bcoef = (b->size[1] != 1);
        i = b->size[1] - 1;
        for (u1 = 0; u1 <= i; u1++) {
            varargin_3 = bcoef * u1;
            acoef = (a_size[0] != 1);
            b_bcoef = (b->size[0] != 1);
            i1 = c->size[0] - 1;
            for (k = 0; k <= i1; k++) {
                c->data[k + c->size[0] * u1] = a_data[acoef * k] * b->
                    data[b_bcoef * k + b->size[0] * varargin_3];
            }
        }
    }
}

/*
 * File trailer for bsxfun.c
 *
 * [EOF]
 */
