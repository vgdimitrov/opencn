/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: CalcVAJ_v5.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

#ifndef CALCVAJ_V5_H
#define CALCVAJ_V5_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Function Declarations */
extern void CalcVAJ_v5(CurveType CurvStructs_Type, const double CurvStructs_P0[3],
                       const double CurvStructs_P1[3], const double
                       CurvStructs_evec[3], double CurvStructs_theta, double
                       CurvStructs_pitch, double CurvStructs_CoeffP5[6][3],
                       unsigned long Bl_handle, const double Coeff_data[], const
                       int Coeff_size[1], double *v_norm, double a[3]);

#endif

/*
 * File trailer for CalcVAJ_v5.h
 *
 * [EOF]
 */
