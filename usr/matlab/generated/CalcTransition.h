/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: CalcTransition.h
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:09
 */

#ifndef CALCTRANSITION_H
#define CALCTRANSITION_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "FeedoptTypes_types.h"

/* Custom Header Code */
#include "c_simplex.hpp"

/* Function Declarations */
extern void CalcTransition(const CurvStruct *CurvStruct1, const CurvStruct
    *CurvStruct2, double CutOff, CurvStruct *CurvStruct1_C, CurvStruct
    *CurvStruct_T, CurvStruct *CurvStruct2_C);

#endif

/*
 * File trailer for CalcTransition.h
 *
 * [EOF]
 */
