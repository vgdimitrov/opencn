file(GLOB MATLAB_GEN_SRC ${opencn-usr_SOURCE_DIR}/matlab/generated/*.c)
file(GLOB RS274_SRC rs274ngc/*.cc)

add_executable(feedopt feedopt.cpp
${MATLAB_GEN_SRC}
${opencn-usr_SOURCE_DIR}/matlab/common/src/c_spline.c
${opencn-usr_SOURCE_DIR}/matlab/common/src/functions.c
${opencn-usr_SOURCE_DIR}/matlab/common/src/cpp_simplex.cpp
tools.c
${RS274_SRC}
)

find_package(GSL REQUIRED)

set(BLA_VENDOR OpenBLAS)
find_package(BLAS REQUIRED)

target_link_libraries(feedopt opencn ${BLAS_LIBRARIES} GSL::gsl ClpSolver Clp CoinUtils dl rt pthread)
set_target_properties(feedopt PROPERTIES COMPILE_FLAGS "${_components_cflags} -DUSE_FULL_MATLAB_GEN -D_POSIX_C_SOURCE=199309L")
set_target_properties(feedopt PROPERTIES LINK_FLAGS "${_components_ldflags}")
# -fsanitize=address -static-libasan"
target_include_directories(feedopt PRIVATE ${_components_include_directories} 
${opencn-usr_SOURCE_DIR}/matlab/common/src 
${opencn-usr_SOURCE_DIR}
)

set_property(SOURCE ${MATLAB_GEN_SRC} PROPERTY COMPILE_FLAGS -Wno-maybe-uninitialized)
