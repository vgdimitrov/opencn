add_executable(lcec lcec.c lcec_xml_parser.c lcec_xml_icmds.c)

target_link_libraries(lcec opencn)
set_target_properties(lcec PROPERTIES COMPILE_FLAGS ${_components_cflags})
set_target_properties(lcec PROPERTIES LINK_FLAGS ${_components_ldflags})
target_include_directories(lcec PRIVATE ${_components_include_directories})
