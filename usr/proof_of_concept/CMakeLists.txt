add_executable(proof-of-concept test.cpp)
add_executable(cpu23_try cpu23_try.c)
add_executable(trysample trysample.c)
add_executable(trylog trylog.c)

target_link_libraries(proof-of-concept ClpSolver Clp CoinUtils)
target_link_libraries(cpu23_try pthread)
target_link_libraries(trysample opencn)
target_link_libraries(trylog opencn)