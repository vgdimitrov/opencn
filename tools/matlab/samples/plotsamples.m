clc; close all;

Configs = load_configs();

filename = '2019-9-4_11-54-45_sampler.log';
if exist([filename, '.mat'], 'file')
    content = load([filename, '.mat']);
    data = content.data;
else
    data = load(filename);
    save([filename, '.mat'], 'data');
end
cfg = Configs(filename);

%data = data(1:18789, :);

t = 0:cfg.dt:(cfg.dt*(size(data,1)-1));

Position = data(:, 1:3);
Velocity = diff(data(:, 1:3), 1)/cfg.dt/cfg.vmax;
Acceleration = diff(data(:, 1:3), 2)/cfg.dt^2/cfg.amax;
Jerk = diff(data(:, 1:3), 3)/cfg.dt^3/cfg.jmax;


figure
xlim([min(Position(:,1))-10, max(Position(:, 1))+10]);
ylim([min(Position(:,2))-10, max(Position(:, 2))+10]);
hold all;
Idx = 1:5:size(data,1);
scatter(Position(Idx,1), Position(Idx, 2), '.')
quiver(Position(Idx,1), Position(Idx, 2),Acceleration(Idx,1), Acceleration(Idx, 2))

figure
subplot(2,2,1)
plot(Position)
title('Position')

subplot(2,2,2)
plot(Velocity)
title('Velocity')

subplot(2,2,3)
plot(Acceleration)
title('Acceleration')

subplot(2,2,4)
plot(Jerk)
title('Jerk')