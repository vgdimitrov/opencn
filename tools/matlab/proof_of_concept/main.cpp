#include <algorithm>
#include <cassert>
#include <cstdio>
#include <cstring>
#include <gsl/gsl_bspline.h>
#include <gsl/gsl_multifit.h>
#include <iomanip>
#include <iostream>

#include "SmoothCurvStructs.h"
#include "SmoothCurvStructs_emxAPI.h"
#include "SmoothCurvStructs_emxutil.h"
#include "functions.h"

//#include "/usr/local/include/cblas.h"
#include "static_test.hpp"
#include "threaded_test.hpp"
#include "tools.hpp"
#include <time_prof.h>

using namespace std::chrono_literals;

int main()
{

//    printf("%s\n", openblas_get_config());
//    printf("threads = %d\n", openblas_get_num_threads());

    //    static_test();

    threaded_test();
    return 0;
}
