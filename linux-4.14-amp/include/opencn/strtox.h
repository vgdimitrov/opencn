

double strtod(char *s, char **endptr);
unsigned long long strtoull(char *s, char **endptr, int base);
long long strtoll(char *s, char **endptr, unsigned int base);
unsigned long strtoul(char *s, char **endptr, unsigned int base);
unsigned int strtouint(char *s, char **endptr, unsigned int base);
int strtol(const char *s, char **endptr, unsigned int base);
