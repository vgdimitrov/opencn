#ifndef __GSL_BLOCK_H__
#define __GSL_BLOCK_H__

#include <opencn/gsl/gsl_block_complex_long_double.h>
#include <opencn/gsl/gsl_block_complex_double.h>
#include <opencn/gsl/gsl_block_complex_float.h>

#include <opencn/gsl/gsl_block_long_double.h>
#include <opencn/gsl/gsl_block_double.h>
#include <opencn/gsl/gsl_block_float.h>

#include <opencn/gsl/gsl_block_ulong.h>
#include <opencn/gsl/gsl_block_long.h>

#include <opencn/gsl/gsl_block_uint.h>
#include <opencn/gsl/gsl_block_int.h>

#include <opencn/gsl/gsl_block_ushort.h>
#include <opencn/gsl/gsl_block_short.h>

#include <opencn/gsl/gsl_block_uchar.h>
#include <opencn/gsl/gsl_block_char.h>

#endif /* __GSL_BLOCK_H__ */
