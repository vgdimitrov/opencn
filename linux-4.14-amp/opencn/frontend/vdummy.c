
/*
 * Copyright (C) 2014-2019 Daniel Rossier <daniel.rossier@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <linux/slab.h>

#include <xenomai/rtdm/driver.h>

#include <soo/avz.h>
#include <soo/dev/vdummy.h>
#include <soo/evtchn.h>

#include <opencn/backend/vdummy.h>

static vdummy_front_ring_t ring;
static rtdm_irq_t irq_handle;

static void process_pending_rsp(void) {
	RING_IDX i, rp;
	vdummy_response_t *ring_rsp;

	DBG("%s\n", __func__);

	rp = ring.sring->rsp_prod;
	mb(); /* Ensure we see queued responses up to 'rp'. Just to make sure ;-) not necessary in all cases... */

	for (i = ring.sring->rsp_cons; i != rp; i++) {
		DBG("%s, cons=%d\n", __func__, i);
		ring_rsp = RING_GET_RESPONSE(&ring, i);

		/* Do something with the response */
		lprintk("## now received the response: %s on CPU %d\n", ring_rsp->buffer, smp_processor_id());
	}

	/* At the end rsp_cons = rsp_prod and refers to a free response index */
	ring.sring->rsp_cons = i;

#if 0
	/* Example of batch of responses processing */

	again:

	rp = ring.sring->rsp_prod;
	mb(); /* Ensure we see queued responses up to 'rp'. Just to make sure ;-) not necessary in all cases... */

	for (i = ring.sring->rsp_cons; i != rp; i++) {
		ring_rsp = RING_GET_RESPONSE(&ring, i);

		/* Do something with the response */
	}

	ring.sring->rsp_cons = i;

	RING_FINAL_CHECK_FOR_RESPONSES(&ring, work_to_do);

	if (work_to_do)
		goto again;
#endif /* 0 */
}

static int vdummy_interrupt(rtdm_irq_t *dummy) {

	process_pending_rsp();

	return RTDM_IRQ_HANDLED;
}

void vdummy_send_data(char *buffer) {
	vdummy_request_t *ring_req;
#if 0 /* only in case of batch of requests */
	int notify;
#endif

	 */
	/*
	 * Try to generate a new request to the backend
	 */
	if (!RING_FULL(&ring)) {
		ring_req = RING_GET_REQUEST(&ring, ring.req_prod_pvt);

		memcpy(ring_req->buffer, buffer, VDUMMY_PACKET_SIZE);

		/* Fill in the ring_req structure */

		/* Make sure the other end "sees" the request when updating the index */
		mb();

		ring.req_prod_pvt++;

		RING_PUSH_REQUESTS(&ring);

		notify_remote_via_irq(irq_handle.irq);
	}

#if 0

	/* Example of batch of request processing */

	if (!RING_FULL(&ring)) {

		/* We generate the first request */

		/* Fill in the ring_req structure */

		ring.req_prod_pvt++;

		/* At this time the request is not visible yet to the other end.
		 * We could proceed with additional requests.
		 */


		/* Now, we are ready to push the first available requests
		 * and we let the macro decide if a notification is required or not.
		 */

		RING_PUSH_REQUESTS_AND_CHECK_NOTIFY(&ring, notify);

		if (notify)
			notify_remote_via_irq(irq_handle.irq);

	}

#endif /* 0 */
}

void vdummy_init(void) {
	int res;
	unsigned int evtchn;
	vdummy_sring_t *sring;

	BUG_ON(smp_processor_id() != OPENCN_RT_CPU);

	/* Allocate an event channel associated to the ring */
	res = vbus_alloc_evtchn(DOMID_CPU0, &evtchn);
	BUG_ON(res);

	res = rtdm_bind_evtchn_to_irq_handler(&irq_handle, evtchn, vdummy_interrupt, 0, "vdummy-frontend", NULL);
	if (res <= 0) {
		lprintk("%s - line %d: Binding event channel failed.\n", __func__, __LINE__);
		BUG();
	}

	/* Allocate a shared page for the ring */
	sring = (vdummy_sring_t *) kmalloc(VDUMMY_RING_SIZE, GFP_ATOMIC);
	if (!sring) {
		lprintk("%s - line %d: Allocating shared ring failed.\n", __func__, __LINE__);
		BUG();
	}

	SHARED_RING_INIT(sring);
	FRONT_RING_INIT(&ring, sring, VDUMMY_RING_SIZE);

	probe_vdummyback(sring, evtchn);
}

/**
 * Free the ring and deallocate the proper data.
 */
void vdummy_free_sring(void) {

	/* Free resources associated with old device channel. */
	kfree(ring.sring);

	rtdm_unbind_from_irqhandler(&irq_handle);

#warning still dc_vdummy_free to be implemented...
}

