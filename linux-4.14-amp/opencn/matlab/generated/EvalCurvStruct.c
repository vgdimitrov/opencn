/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: EvalCurvStruct.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:43
 */

/* Include Files */
#include "EvalCurvStruct.h"
#include "EvalHelix.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptTypes.h"
#include "FeedoptTypes_data.h"
#include "FeedoptTypes_initialize.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_linspace.h"
#include "sinspace.h"
#include <stdio.h>

/* Function Definitions */

/*
 * Arguments    : const CurvStruct *b_CurvStruct
 *                double u_vec_data[]
 *                const int u_vec_size[2]
 *                double r0D_data[]
 *                int r0D_size[2]
 *                double r1D_data[]
 *                int r1D_size[2]
 *                double r2D_data[]
 *                int r2D_size[2]
 *                double r3D_data[]
 *                int r3D_size[2]
 * Return Type  : void
 */
void EvalCurvStruct(const CurvStruct *b_CurvStruct, double u_vec_data[], const
                    int u_vec_size[2], double r0D_data[], int r0D_size[2],
                    double r1D_data[], int r1D_size[2], double r2D_data[], int
                    r2D_size[2], double r3D_data[], int r3D_size[2])
{
    int na;
    int k;
    boolean_T y;
    boolean_T x_data[200];
    boolean_T exitg1;
    int i;
    char message[30];
    static const char b_message[30] = { 'U', 'n', 'k', 'n', 'o', 'w', 'n', ' ',
        'C', 'u', 'r', 'v', 'e', ' ', 'T', 'y', 'p', 'e', ' ', 'f', 'o', 'r',
        ' ', 'E', 'v', 'a', 'l', '.', '\\', 'n' };

    double p5_1D[5][3];
    double p5_2D[4][3];
    double a_idx_0;
    double a_idx_1;
    double a_idx_2;
    double b[3][3];
    unsigned char outsize_idx_1;
    unsigned char b_outsize_idx_1;
    int b_size_idx_1;
    double b_data[600];
    double b_b_data[600];
    if (isInitialized_FeedoptTypes == false) {
        FeedoptTypes_initialize();
    }

    na = u_vec_size[1];
    for (k = 0; k < na; k++) {
        x_data[k] = (u_vec_data[k] > 1.0);
    }

    y = false;
    k = 0;
    exitg1 = false;
    while ((!exitg1) && (k <= u_vec_size[1] - 1)) {
        if (!x_data[k]) {
            k++;
        } else {
            y = true;
            exitg1 = true;
        }
    }

    if (y) {
        printf("EvalCurvStruct: u_vec > 1\n");
        fflush(stdout);
        na = u_vec_size[1];
        for (i = 0; i < na; i++) {
            if (u_vec_data[i] > 1.0) {
                u_vec_data[i] = 1.0;
            }
        }
    }

    na = u_vec_size[1];
    for (k = 0; k < na; k++) {
        x_data[k] = (u_vec_data[k] < 0.0);
    }

    y = false;
    k = 0;
    exitg1 = false;
    while ((!exitg1) && (k <= u_vec_size[1] - 1)) {
        if (!x_data[k]) {
            k++;
        } else {
            y = true;
            exitg1 = true;
        }
    }

    if (y) {
        printf("EvalCurvStruct: u_vec < 0\n");
        fflush(stdout);
        na = u_vec_size[1];
        for (i = 0; i < na; i++) {
            if (u_vec_data[i] < 0.0) {
                u_vec_data[i] = 0.0;
            }
        }
    }

    /*  */
    /*  */
    r0D_size[0] = 3;
    r0D_size[1] = u_vec_size[1];
    na = u_vec_size[1];
    for (k = 0; k < na; k++) {
        r0D_data[3 * k] = 0.0;
        r0D_data[3 * k + 1] = 0.0;
        r0D_data[3 * k + 2] = 0.0;
    }

    r1D_size[0] = 3;
    r1D_size[1] = u_vec_size[1];
    na = u_vec_size[1];
    for (k = 0; k < na; k++) {
        r1D_data[3 * k] = 0.0;
        r1D_data[3 * k + 1] = 0.0;
        r1D_data[3 * k + 2] = 0.0;
    }

    r2D_size[0] = 3;
    r2D_size[1] = u_vec_size[1];
    na = u_vec_size[1];
    for (k = 0; k < na; k++) {
        r2D_data[3 * k] = 0.0;
        r2D_data[3 * k + 1] = 0.0;
        r2D_data[3 * k + 2] = 0.0;
    }

    r3D_size[0] = 3;
    r3D_size[1] = u_vec_size[1];
    na = u_vec_size[1];
    for (k = 0; k < na; k++) {
        r3D_data[3 * k] = 0.0;
        r3D_data[3 * k + 1] = 0.0;
        r3D_data[3 * k + 2] = 0.0;
    }

    switch (b_CurvStruct->Type) {
      case CurveType_Line:
        /*  line (G01) */
        /*  */
        /*  parametrization of a straight line between P0 and P1 */
        /*  */
        r0D_size[0] = 3;
        r0D_size[1] = u_vec_size[1];
        na = u_vec_size[1];
        for (k = 0; k < na; k++) {
            r0D_data[3 * k] = b_CurvStruct->P1[0] * u_vec_data[k] +
                b_CurvStruct->P0[0] * (1.0 - u_vec_data[k]);
        }

        na = u_vec_size[1];
        for (k = 0; k < na; k++) {
            r0D_data[3 * k + 1] = b_CurvStruct->P1[1] * u_vec_data[k] +
                b_CurvStruct->P0[1] * (1.0 - u_vec_data[k]);
        }

        na = u_vec_size[1];
        for (k = 0; k < na; k++) {
            r0D_data[3 * k + 2] = b_CurvStruct->P1[2] * u_vec_data[k] +
                b_CurvStruct->P0[2] * (1.0 - u_vec_data[k]);
        }

        /*  */
        a_idx_0 = b_CurvStruct->P1[0] - b_CurvStruct->P0[0];
        a_idx_1 = b_CurvStruct->P1[1] - b_CurvStruct->P0[1];
        a_idx_2 = b_CurvStruct->P1[2] - b_CurvStruct->P0[2];
        r1D_size[0] = 3;
        r1D_size[1] = (unsigned char)u_vec_size[1];
        if ((unsigned char)u_vec_size[1] != 0) {
            k = u_vec_size[1] - 1;
            for (na = 0; na <= k; na++) {
                r1D_data[3 * na] = a_idx_0;
                r1D_data[3 * na + 1] = a_idx_1;
                r1D_data[3 * na + 2] = a_idx_2;
            }
        }

        /*  */
        break;

      case CurveType_Helix:
        /*  arc of circle / helix (G02, G03) */
        EvalHelix(b_CurvStruct->P0, b_CurvStruct->P1, b_CurvStruct->evec,
                  b_CurvStruct->theta, b_CurvStruct->pitch, u_vec_data,
                  u_vec_size, r0D_data, r0D_size, r1D_data, r1D_size, r2D_data,
                  r2D_size, r3D_data, r3D_size);
        break;

      case CurveType_TransP5:
        /*  polynomial transition */
        /*  */
        /* MYPOLYDER Differentiate polynomial. */
        /*  */
        /* u  = u(:).';  */
        for (k = 0; k < 5; k++) {
            na = 5 - k;
            p5_1D[k][0] = b_CurvStruct->CoeffP5[k][0] * (double)na;
            p5_1D[k][1] = b_CurvStruct->CoeffP5[k][1] * (double)na;
            p5_1D[k][2] = b_CurvStruct->CoeffP5[k][2] * (double)na;
        }

        /* MYPOLYDER Differentiate polynomial. */
        /*  */
        /* u  = u(:).';  */
        for (k = 0; k < 4; k++) {
            na = 4 - k;
            p5_2D[k][0] = p5_1D[k][0] * (double)na;
            p5_2D[k][1] = p5_1D[k][1] * (double)na;
            p5_2D[k][2] = p5_1D[k][2] * (double)na;
        }

        /* MYPOLYDER Differentiate polynomial. */
        /*  */
        /* u  = u(:).';  */
        for (k = 0; k < 3; k++) {
            na = 3 - k;
            b[k][0] = na;
            b[k][1] = na;
            b[k][2] = na;
        }

        /*  */
        /* POLYVAL Evaluate array of polynomials with same degree. */
        /*  */
        /*  */
        /*  Use Horner's method for general case where X is an array. */
        if ((unsigned char)u_vec_size[1] != 0) {
            k = u_vec_size[1] - 1;
            for (na = 0; na <= k; na++) {
                r0D_data[3 * na] = b_CurvStruct->CoeffP5[0][0];
                r0D_data[3 * na + 1] = b_CurvStruct->CoeffP5[0][1];
                r0D_data[3 * na + 2] = b_CurvStruct->CoeffP5[0][2];
            }
        }

        outsize_idx_1 = (unsigned char)u_vec_size[1];
        b_outsize_idx_1 = (unsigned char)u_vec_size[1];
        b_size_idx_1 = outsize_idx_1;
        if (outsize_idx_1 != 0) {
            na = u_vec_size[1];
            for (k = 0; k < na; k++) {
                b_data[3 * k] = u_vec_data[k];
                b_data[3 * k + 1] = u_vec_data[k];
                b_data[3 * k + 2] = u_vec_data[k];
            }
        }

        r0D_size[0] = 3;
        r0D_size[1] = outsize_idx_1;
        for (i = 0; i < 5; i++) {
            if (b_outsize_idx_1 != 0) {
                k = u_vec_size[1] - 1;
                for (na = 0; na <= k; na++) {
                    b_b_data[3 * na] = b_CurvStruct->CoeffP5[i + 1][0];
                    b_b_data[3 * na + 1] = b_CurvStruct->CoeffP5[i + 1][1];
                    b_b_data[3 * na + 2] = b_CurvStruct->CoeffP5[i + 1][2];
                }
            }

            for (k = 0; k < b_size_idx_1; k++) {
                r0D_data[3 * k] = b_data[3 * k] * r0D_data[3 * k] + b_b_data[3 *
                    k];
                na = 3 * k + 1;
                r0D_data[na] = b_data[na] * r0D_data[na] + b_b_data[na];
                na = 3 * k + 2;
                r0D_data[na] = b_data[na] * r0D_data[na] + b_b_data[na];
            }
        }

        /* POLYVAL Evaluate array of polynomials with same degree. */
        /*  */
        /*  */
        /*  Use Horner's method for general case where X is an array. */
        if ((unsigned char)u_vec_size[1] != 0) {
            k = u_vec_size[1] - 1;
            for (na = 0; na <= k; na++) {
                r1D_data[3 * na] = p5_1D[0][0];
                r1D_data[3 * na + 1] = p5_1D[0][1];
                r1D_data[3 * na + 2] = p5_1D[0][2];
            }
        }

        outsize_idx_1 = (unsigned char)u_vec_size[1];
        b_outsize_idx_1 = (unsigned char)u_vec_size[1];
        b_size_idx_1 = outsize_idx_1;
        if (outsize_idx_1 != 0) {
            na = u_vec_size[1];
            for (k = 0; k < na; k++) {
                b_data[3 * k] = u_vec_data[k];
                b_data[3 * k + 1] = u_vec_data[k];
                b_data[3 * k + 2] = u_vec_data[k];
            }
        }

        r1D_size[0] = 3;
        r1D_size[1] = outsize_idx_1;
        for (i = 0; i < 4; i++) {
            if (b_outsize_idx_1 != 0) {
                k = u_vec_size[1] - 1;
                for (na = 0; na <= k; na++) {
                    b_b_data[3 * na] = p5_1D[i + 1][0];
                    b_b_data[3 * na + 1] = p5_1D[i + 1][1];
                    b_b_data[3 * na + 2] = p5_1D[i + 1][2];
                }
            }

            for (k = 0; k < b_size_idx_1; k++) {
                r1D_data[3 * k] = b_data[3 * k] * r1D_data[3 * k] + b_b_data[3 *
                    k];
                na = 3 * k + 1;
                r1D_data[na] = b_data[na] * r1D_data[na] + b_b_data[na];
                na = 3 * k + 2;
                r1D_data[na] = b_data[na] * r1D_data[na] + b_b_data[na];
            }
        }

        /* POLYVAL Evaluate array of polynomials with same degree. */
        /*  */
        /*  */
        /*  Use Horner's method for general case where X is an array. */
        if ((unsigned char)u_vec_size[1] != 0) {
            k = u_vec_size[1] - 1;
            for (na = 0; na <= k; na++) {
                r2D_data[3 * na] = p5_2D[0][0];
                r2D_data[3 * na + 1] = p5_2D[0][1];
                r2D_data[3 * na + 2] = p5_2D[0][2];
            }
        }

        outsize_idx_1 = (unsigned char)u_vec_size[1];
        b_outsize_idx_1 = (unsigned char)u_vec_size[1];
        b_size_idx_1 = outsize_idx_1;
        if (outsize_idx_1 != 0) {
            na = u_vec_size[1];
            for (k = 0; k < na; k++) {
                b_data[3 * k] = u_vec_data[k];
                b_data[3 * k + 1] = u_vec_data[k];
                b_data[3 * k + 2] = u_vec_data[k];
            }
        }

        r2D_size[0] = 3;
        r2D_size[1] = outsize_idx_1;
        for (i = 0; i < 3; i++) {
            if (b_outsize_idx_1 != 0) {
                k = u_vec_size[1] - 1;
                for (na = 0; na <= k; na++) {
                    b_b_data[3 * na] = p5_2D[i + 1][0];
                    b_b_data[3 * na + 1] = p5_2D[i + 1][1];
                    b_b_data[3 * na + 2] = p5_2D[i + 1][2];
                }
            }

            for (k = 0; k < b_size_idx_1; k++) {
                r2D_data[3 * k] = b_data[3 * k] * r2D_data[3 * k] + b_b_data[3 *
                    k];
                na = 3 * k + 1;
                r2D_data[na] = b_data[na] * r2D_data[na] + b_b_data[na];
                na = 3 * k + 2;
                r2D_data[na] = b_data[na] * r2D_data[na] + b_b_data[na];
            }

            b[i][0] *= p5_2D[i][0];
            b[i][1] *= p5_2D[i][1];
            b[i][2] *= p5_2D[i][2];
        }

        /* POLYVAL Evaluate array of polynomials with same degree. */
        /*  */
        /*  */
        /*  Use Horner's method for general case where X is an array. */
        if ((unsigned char)u_vec_size[1] != 0) {
            k = u_vec_size[1] - 1;
            for (na = 0; na <= k; na++) {
                r3D_data[3 * na] = b[0][0];
                r3D_data[3 * na + 1] = b[0][1];
                r3D_data[3 * na + 2] = b[0][2];
            }
        }

        outsize_idx_1 = (unsigned char)u_vec_size[1];
        b_outsize_idx_1 = (unsigned char)u_vec_size[1];
        b_size_idx_1 = outsize_idx_1;
        if (outsize_idx_1 != 0) {
            na = u_vec_size[1];
            for (k = 0; k < na; k++) {
                b_data[3 * k] = u_vec_data[k];
                b_data[3 * k + 1] = u_vec_data[k];
                b_data[3 * k + 2] = u_vec_data[k];
            }
        }

        r3D_size[0] = 3;
        r3D_size[1] = outsize_idx_1;
        for (i = 0; i < 2; i++) {
            if (b_outsize_idx_1 != 0) {
                k = u_vec_size[1] - 1;
                for (na = 0; na <= k; na++) {
                    b_b_data[3 * na] = b[i + 1][0];
                    b_b_data[3 * na + 1] = b[i + 1][1];
                    b_b_data[3 * na + 2] = b[i + 1][2];
                }
            }

            for (k = 0; k < b_size_idx_1; k++) {
                r3D_data[3 * k] = b_data[3 * k] * r3D_data[3 * k] + b_b_data[3 *
                    k];
                na = 3 * k + 1;
                r3D_data[na] = b_data[na] * r3D_data[na] + b_b_data[na];
                na = 3 * k + 2;
                r3D_data[na] = b_data[na] * r3D_data[na] + b_b_data[na];
            }
        }
        break;

      default:
        for (k = 0; k < 30; k++) {
            message[k] = b_message[k];
        }

        c_assert_(&message[0]);
        break;
    }
}

/*
 * File trailer for EvalCurvStruct.c
 *
 * [EOF]
 */
