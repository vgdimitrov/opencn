/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: c_assert.c
 *
 * MATLAB Coder version            : 4.3
 * C/C++ source code generated on  : 08-Nov-2019 11:22:43
 */

/* Include Files */
#include "c_assert.h"
#include "EvalCurvStruct.h"
#include "FeedoptDefaultConfig.h"
#include "FeedoptTypes.h"
#include "InitConfig.h"
#include "PrintCurvStruct.h"
#include "ResampleTick.h"
#include "c_linspace.h"
#include "sinspace.h"

/* Function Definitions */

/*
 * Arguments    : boolean_T condition
 * Return Type  : boolean_T
 */
boolean_T b_c_assert(boolean_T condition)
{
    int i;
    char message[13];
    static const char b_message[13] = { 'e', '\'', ' ', '*', ' ', 'P', '0', 'P',
        '1', ' ', '=', ' ', '0' };

    if (!condition) {
        for (i = 0; i < 13; i++) {
            message[i] = b_message[i];
        }

        c_assert_(&message[0]);
    }

    return condition;
}

/*
 * Arguments    : boolean_T condition
 * Return Type  : boolean_T
 */
boolean_T c_assert(boolean_T condition)
{
    int i;
    char message[16];
    static const char b_message[16] = { 'e', ' ', 'c', 'r', 'o', 's', 's', ' ',
        'P', '0', 'P', '1', ' ', '=', ' ', '0' };

    if (!condition) {
        for (i = 0; i < 16; i++) {
            message[i] = b_message[i];
        }

        c_assert_(&message[0]);
    }

    return condition;
}

/*
 * File trailer for c_assert.c
 *
 * [EOF]
 */
