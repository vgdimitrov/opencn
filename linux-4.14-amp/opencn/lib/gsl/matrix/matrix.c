#include <opencn/gsl/config.h>
#include <opencn/gsl/gsl_errno.h>
#include <opencn/gsl/gsl_vector.h>

/* Compile all the inline matrix functions */

#define COMPILE_INLINE_STATIC
#include <opencn/gsl/build.h>
#include <opencn/gsl/gsl_matrix.h>

